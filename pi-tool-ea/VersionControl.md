

# Fragen
  * Aufruf svn2git wie?
  * Genaue Konfiguration: screenshot?
  

  * ? viele CrossReferences: Repository.ScanXMIAndReconcile()
  

  
# API

## Package Attributes
  * BatchLoad: not used
  * BatchSave: long/boolean
  

  * Flags: String - Extended information about the Package. !!! see Scripting Enterprise Architect

  
  * IsControlled: Boolean R/W - 	Indicates if the Package has been marked as Controlled.
  * IsVersionControlled: Boolean R - 	Indicates whether or not this Package is under Version Control.
  * XMLPath: String Read/Write - 	The path to which the XML is saved when using controlled Packages.
  * LastLoadDate: Date Read/Write - 	The date XML was last loaded for the Package.
  
  

## Package Methods
  * alle nicht in COM implementiert
  

  * VersionControlXXX - Throws an exception if the operation fails; use GetLastError() to retrieve error information.

  
  * VersionControlAdd (string ConfigGuid, string XMLFile, string Comment, boolean KeepCheckedOut): Void
  * VersionControlCheckinEx (string Comment, boolean PreserveCrossPkgRefs)
  * VersionControlCheckout (string Comment)
  * VersionControlGetLatest (boolean ForceImport)
  * VersionControlGetStatus ()
  * VersionControlPutLatest (string CheckInComment)
  * VersionControlRemove ()
  * VersionControlResynchPkgStatus (boolean ClearSettings)
  
  
  * Status:
    ```
    enum EnumCheckOutStatus
     {
          csUncontrolled = 0,
          csCheckedIn,
          csCheckedOutToThisUser,
          csReadOnlyVersion,
          csCheckedOutToAnotherUser,
          csOfflineCheckedIn,
          csCheckedOutOfflineByUser,
          csCheckedOutOfflineByOther,
          csDeleted,
     }; 
    ```
     
    
## Project Methods
 
  * LoadControlledPackage (string PackageGUID): from XMI
  * SaveControlledPackage (string PackageGUID): to XMI


  * ExportPackageXMIEx (string PackageGUID, enumXMIType XMIType, long DiagramXML, long DiagramImage, long FormatXML, long UseDTD, string FileName, ea.ExportPackageXMIFlag Flags)
  * ImportPackageXMI (string PackageGUID, string Filename, long ImportDiagrams, long StripGUID)
     
  
  
# Sonstiges
 * Info: Scripting Enterprise Architect
 * Nested Packages: Stub bis 'CheckAllLatest'
 * finde Parent Packge bis 'under version control'
  
