package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class DiagramObject {

  private Dispatch comDiagramObject;

  public DiagramObject( Dispatch comDiagramObject) {
    this.comDiagramObject = comDiagramObject;
  }

  public int GetElementID() {
    return Dispatch.get( comDiagramObject, "ElementID" ).getInt();
  }

  public void SetElementID( int elementID ) {
    Dispatch.put( comDiagramObject, "ElementID", (long)elementID );
  }

  public void SetDiagramID( int diagramID) {
    Dispatch.put( comDiagramObject, "DiagramID", diagramID );
  }

  public void SetLeft( int left ) {
    Dispatch.put( comDiagramObject, "Left", left );
  }

  public void SetRight( int right ) {
    Dispatch.put( comDiagramObject, "Right", right );
  }

  public void SetStyleEx( String name, String value ) {
    Dispatch.call( comDiagramObject, "SetStyleEx", name, value );
  }

  public void SetTop( int top ) {
    Dispatch.put( comDiagramObject, "Top", top );
  }

  public void SetBottom( int bottom ) {
    Dispatch.put( comDiagramObject, "Bottom", bottom );
  }

  public boolean Update() {
    return Dispatch.get( comDiagramObject, "Update" ).getBoolean();
  }

  public ObjectType GetObjectType() {
    int objTypeInt =  Dispatch.get( comDiagramObject, "ObjectType" ).getInt();
    return ObjectType.values()[objTypeInt];
  }

  public String GetStyle() {
    return Dispatch.get( comDiagramObject, "Style" ).getString();
  }

  public int GetTop() {
    return Dispatch.get( comDiagramObject, "Top" ).getInt();
  }

  public int GetLeft() {
    return Dispatch.get( comDiagramObject, "Left" ).getInt();
  }

  public int GetBottom() {
    return Dispatch.get( comDiagramObject, "Bottom" ).getInt();
  }

  public int GetRight() {
    return Dispatch.get( comDiagramObject, "Right" ).getInt();
  }

  public int GetDiagramID() {
    return Dispatch.get( comDiagramObject, "DiagramID" ).getInt();
  }

  public int GetInstanceID() {
    return Dispatch.get( comDiagramObject, "InstanceID" ).getInt();
  }

  public String GetLastError() {
    return Dispatch.get( comDiagramObject, "LastError" ).getString();
  }

  public int GetSequence() {
    return Dispatch.get( comDiagramObject, "Sequence" ).getInt();
  }

  public void SetSequence( int value ) {
    Dispatch.put( comDiagramObject, "Sequence", value );
  }

  public void SetStyle( String style ) {
    Dispatch.put( comDiagramObject, "Style", style );
  }

  public void ResetFont() {
    Dispatch.call( comDiagramObject, "ResetFont" );
  }
}
