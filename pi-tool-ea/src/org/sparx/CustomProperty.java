package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class CustomProperty {

  private Dispatch comProperty;

  public CustomProperty( Dispatch comProperty ) {
    this.comProperty = comProperty;
  }

  public String GetName() {
    return Dispatch.get( comProperty, "Name" ).getString();
  }

  public String GetValue() {
    return Dispatch.get( comProperty, "Value" ).getString();
  }

  public void SetValue(String value) {
    Dispatch.put( comProperty, "Value", value );
  }
}
