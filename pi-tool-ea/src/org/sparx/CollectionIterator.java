package org.sparx;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by pru on 08.12.2015.
 */
public class CollectionIterator<E> implements Iterator<E> {

  private Collection<E> collection;
  private short position = -1;

  public CollectionIterator( Collection<E> collection ) {
    this.collection = collection;
  }

  /**
   * Returns {@code true} if the iteration has more elements.
   * (In other words, returns {@code true} if {@link #next} would
   * return an element rather than throwing an exception.)
   *
   * @return {@code true} if the iteration has more elements
   */
  @Override
  public boolean hasNext() {
    return (this.collection.GetCount() > (position + 1));
  }

  /**
   * Returns the next element in the iteration.
   *
   * @return the next element in the iteration
   * @throws NoSuchElementException if the iteration has no more elements
   */
  @Override
  public E next() {
    if (!hasNext()) {
      throw new NoSuchElementException();
    }
    else {
      position++;
      return this.collection.GetAt( position );
    }
  }
}
