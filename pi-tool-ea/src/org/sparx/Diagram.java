package org.sparx;

import com.jacob.com.Dispatch;

import java.util.Date;

/**
 * Created by pru on 08.12.2015.
 */
public class Diagram {

  private Dispatch comDiagram;

  public Diagram( Dispatch comDiagram ) {
    this.comDiagram = comDiagram;
  }

  public int GetParentID() {
    return Dispatch.get( comDiagram, "ParentID" ).getInt();
  }
  public void SetParentID( int parentID ) {
    Dispatch.put( comDiagram, "ParentID", parentID );
  }

  public int GetPackageID() {
    return Dispatch.get( comDiagram, "PackageID" ).getInt();
  }

  public String GetType() {
    return Dispatch.get( comDiagram, "Type" ).getString();
  }

  public String GetName() {
    return Dispatch.get( comDiagram, "Name" ).getString();
  }

  public int GetDiagramID() {
    return Dispatch.get( comDiagram, "DiagramID" ).getInt();
  }

  public String GetDiagramGUID() {
    return Dispatch.get( comDiagram, "DiagramGUID" ).getString();
  }

  public String GetVersion() {
    return Dispatch.get( comDiagram, "Version" ).getString();
  }

  public String GetAuthor() {
    return Dispatch.get( comDiagram, "Author" ).getString();
  }

  public Collection<DiagramObject> GetDiagramObjects() {
    Dispatch diagramObjects = Dispatch.get( comDiagram, "DiagramObjects").toDispatch();
    return new Collection<DiagramObject>( diagramObjects, DiagramObject.class );
  }

  public Collection<DiagramObject> SelectedObjects() {
    Dispatch diagramObjects = Dispatch.get( comDiagram, "SelectedObjects").toDispatch();
    return new Collection<DiagramObject>( diagramObjects, DiagramObject.class );
  }

  public Collection<DiagramLink> GetDiagramLinks() {
    Dispatch diagramLinks = Dispatch.get( comDiagram, "DiagramLinks").toDispatch();
    return new Collection<DiagramLink>( diagramLinks, DiagramLink.class );
  }

  public Connector GetSelectedConnector() {
    Dispatch comConnector = Dispatch.get( comDiagram, "SelectedConnector").toDispatch();
    return new Connector( comConnector );
  }

  public void SetSelectedConnector( Connector connector ) {
    Dispatch.put( comDiagram, "SelectedConnector", connector.toDispatch() );
  }

  public void Update() {
    Dispatch.call( comDiagram, "Update" );
  }

  public void SetAuthor( String author ) {
    Dispatch.put( comDiagram, "Author", author );
  }


  public String GetStyleEx() {
    return Dispatch.get( comDiagram, "StyleEx" ).getString();
  }

  public int GetShowDetails() {
    return Dispatch.get( comDiagram, "ShowDetails" ).getInt();
  }

  public void SetShowDetails( int i ) {
    Dispatch.put( comDiagram, "ShowDetails", i );
  }

  public void SetNotes( String text ) {
    Dispatch.put( comDiagram, "Notes", text );
  }

  public String GetNotes() {
    return Dispatch.get( comDiagram, "Notes" ).getString();
  }

  public void SetScale( int faktor ) {
    Dispatch.put( comDiagram, "Scale", faktor );
  }

  public int GetScale() {
    return Dispatch.get( comDiagram, "Scale" ).getInt();
  }

  public SwimlaneDef GetSwimlaneDef() {
    Dispatch comSwimLaneDef = Dispatch.get( comDiagram, "SwimlaneDef" ).toDispatch();
    return new SwimlaneDef(comSwimLaneDef);
  }

  public Dispatch getDispatch() {
    return comDiagram;
  }

  public boolean GetLocked() {
    return Dispatch.get( comDiagram, "IsLocked" ).getBoolean();
  }

  public boolean ApplyUserLock() {
    return Dispatch.call(comDiagram, "ApplyUserLock").getBoolean();
  }

  public boolean ReleaseUserLock() {
    return Dispatch.call(comDiagram, "ReleaseUserLock").getBoolean();
  }

  public String GetLastError() {
    return Dispatch.get( comDiagram, "GetLastError" ).getString();
  }

  public Date GetModifiedDate() {
    return Dispatch.get( comDiagram, "ModifiedDate" ).getJavaDate();
  }

  public Date GetCreatedDate() {
    return Dispatch.get( comDiagram, "CreatedDate" ).getJavaDate();
  }

  public void SetName( String newName ) {
    Dispatch.put( comDiagram, "Name", newName );
  }

  public void SetStyleEx( String s ) {
    Dispatch.put( comDiagram, "StyleEx", s );
  }
}
