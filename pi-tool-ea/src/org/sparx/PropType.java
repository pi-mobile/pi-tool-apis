package org.sparx;

/**
 * Created by pru on 09.12.2015.
 */
public enum PropType {
  ptString,
  ptInteger,
  ptFloatingPoint,
  ptBoolean,
  ptEnum,
  ptArray;
}
