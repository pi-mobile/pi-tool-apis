package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class SwimlaneDef {

  private Dispatch comSwimlaneDef;

  public SwimlaneDef(Dispatch comSwimlaneDef) {
    this.comSwimlaneDef = comSwimlaneDef;
  }

  public Swimlanes GetSwimlanes() {
    Dispatch swimlaneDispatch = Dispatch.get( comSwimlaneDef, "Swimlanes").toDispatch();
    return new Swimlanes( swimlaneDispatch );
  }
}
