package org.sparx;

/**
 * Created by pru on 08.12.2015.
 */
public enum CreateModelType {
  cmEAPFromBase,
  cmEAPFromSQLRepository;
}
