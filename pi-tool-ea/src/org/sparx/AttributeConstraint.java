package org.sparx;

import com.jacob.com.Dispatch;

public class AttributeConstraint extends Releasable {

  public AttributeConstraint( Dispatch comObject ) {
    super(comObject);
  }

  public int GetAttributeID() {
    return Dispatch.get( getComObject(), "AttributeID" ).getInt();
  }

  public void SetName( String name ) {
    Dispatch.put( getComObject(), "Name", name );
  }

  public String GetName() {
    return Dispatch.get( getComObject(), "Name" ).getString();
  }

  public void SetNotes( String notes ) {
    Dispatch.put( getComObject(), "Notes", notes );
  }

  public String GetNotes() {
    return Dispatch.get( getComObject(), "Notes" ).getString();
  }

  /**
   * Returns the kind of an object, such as otAttribute, ...
   * CAUTION: this is not the (UML-)type of the element, see {@link #GetType()}!
   *
   * @return
   */
  public ObjectType GetObjectType() {
    int typeID = Dispatch.get( getComObject(), "ObjectType" ).getInt();
    return ObjectType.values()[typeID];
  }

  /**
   * Returns the Constraint type.
   *
   * @return
   */
  public String GetType() {
    return Dispatch.get( getComObject(), "Type" ).getString();
  }

  public String GetLastError() {
    return Dispatch.call( getComObject(), "GetLastError" ).getString();
  }

  public void Update() {
    Dispatch.call( getComObject(), "Update" );
  }

}
