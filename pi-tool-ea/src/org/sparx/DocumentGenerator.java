package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by hel on 16.11.2016.
 */
public class DocumentGenerator {

    private Dispatch comDocumentGenerator;

    public DocumentGenerator( Dispatch comDocumentGenerator ) {
        this.comDocumentGenerator = comDocumentGenerator;
    }

    public boolean NewDocument (String templateName) {
        return Dispatch.call(comDocumentGenerator, "NewDocument",templateName).getBoolean();
    }

    public boolean SaveDocument (String filename, long nDocType) {
        return Dispatch.call(comDocumentGenerator,"SaveDocument",filename,nDocType).getBoolean();
    }

    public boolean InsertText (String text, String style) {
        return Dispatch.call(comDocumentGenerator,"InsertText",text,style).getBoolean();

    }

}
