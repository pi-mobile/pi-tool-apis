package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Connector {

  private Dispatch comConnector;

  public Connector( Dispatch comConnector ) {
    this.comConnector = comConnector;
  }

  public Dispatch toDispatch() {
    return comConnector;
  }

  public int GetSupplierID() {
    return Dispatch.get( comConnector, "SupplierID" ).getInt();
  }

  public String GetType() {
    return Dispatch.get( comConnector, "Type" ).getString();
  }

  public void SetSupplierID( int supplierID ) {
    Dispatch.put( comConnector, "SupplierID", supplierID );
  }

  public void Update() {
    Dispatch.call( comConnector, "Update" );
  }

  public int GetClientID() {
    return Dispatch.get( comConnector, "ClientID" ).getInt();
  }

  public void SetClientID( int clientID ) {
    Dispatch.put( comConnector, "ClientID", clientID );
  }

  public String GetName() {
    return Dispatch.get( comConnector, "Name" ).getString();
  }

  public String GetStereotype() {
    return Dispatch.get( comConnector, "Stereotype" ).getString();
  }

  public String GetFQStereotype() {
    return Dispatch.get( comConnector, "FQStereotype" ).getString();
  }

  public int GetConnectorID() {
    return Dispatch.get( comConnector, "ConnectorID" ).getInt();
  }

  public String GetConnectorGUID() {
    return Dispatch.get( comConnector, "ConnectorGUID" ).getString();
  }

  public void SetName( String name ) {
    Dispatch.put( comConnector, "Name", name );
  }

  public String GetNotes() {
    return Dispatch.get( comConnector, "Notes" ).getString();
  }

  public void SetNotes( String notes ) {
    Dispatch.put( comConnector, "Notes", notes );
  }

  public int GetStartPointX() {
    return Dispatch.get( comConnector, "StartPointX" ).getInt();
  }

  public int GetStartPointY() {
    return Dispatch.get( comConnector, "StartPointY" ).getInt();
  }

  public int GetEndPointX() {
    return Dispatch.get( comConnector, "EndPointX" ).getInt();
  }

  public int GetEndPointY() {
    return Dispatch.get( comConnector, "EndPointY" ).getInt();
  }

  public ConnectorEnd GetClientEnd() {
    Dispatch comConnEnd = Dispatch.get( comConnector, "ClientEnd").toDispatch();
    return new ConnectorEnd( comConnEnd );
  }

  public ConnectorEnd GetSupplierEnd() {
    Dispatch comConnEnd = Dispatch.get( comConnector, "SupplierEnd").toDispatch();
    return new ConnectorEnd( comConnEnd );
  }

  public String GetDirection() {
    return Dispatch.get( comConnector, "Direction" ).getString();
  }

  public void SetStereotype( String stereotype ) {
    Dispatch.put( comConnector, "Stereotype", stereotype );
  }

  public void SetStereotypeEx( String stereotype ) {
    Dispatch.put( comConnector, "StereotypeEx", stereotype );
  }

  public Collection<ConnectorTag> GetTaggedValues() {
    Dispatch comColl = Dispatch.get( comConnector, "TaggedValues").toDispatch();
    return new Collection<ConnectorTag>( comColl, ConnectorTag.class );
  }

  public String GetStyleEx() {
    return Dispatch.get( comConnector, "StyleEx" ).getString();
  }

  public void SetStyleEx( String styleEx ) {
    Dispatch.put( comConnector, "StyleEx", styleEx );
  }

  public String GetStateFlags() {
    return Dispatch.get( comConnector, "StateFlags" ).getString();
  }

  public void SetStateFlags( String stateFlags ) {
    Dispatch.put( comConnector, "StateFlags", stateFlags );
  }

  public String GetAlias() {
    return Dispatch.get( comConnector, "Alias" ).getString();
  }

  public int GetColor() {
    return Dispatch.get( comConnector, "Color" ).getInt();
  }

  public void SetColor( int color) {
    Dispatch.put( comConnector, "Color", color);
  }


  public int GetDiagramID() {
    return Dispatch.get( comConnector, "DiagramID" ).getInt();
  }

  public String GetEventFlags() {
    return Dispatch.get( comConnector, "EventFlags" ).getString();
  }

  public boolean GetIsLeaf() {
    return Dispatch.get( comConnector, "IsLeaf" ).getBoolean();
  }

  public boolean GetIsRoot() {
    return Dispatch.get( comConnector, "IsRoot" ).getBoolean();
  }

  public boolean GetIsSpec() {
    return Dispatch.get( comConnector, "IsSpec" ).getBoolean();
  }

  public String GetLastError() {
    return Dispatch.call( comConnector, "GetLastError" ).getString();
  }

  public String GetMetaType() {
    return Dispatch.get( comConnector, "MetaType" ).getString();
  }

  public String GetRouteStyle() {
    return Dispatch.get( comConnector, "RouteStyle" ).getString();
  }

  public int GetSequenceNo() {
    return Dispatch.get( comConnector, "SequenceNo" ).getInt();
  }

  public String GetStereotypeEx() {
    return Dispatch.get( comConnector, "StereotypeEx" ).getString();
  }

  public String GetSubtype() {
    return Dispatch.get( comConnector, "Subtype" ).getString();
  }

  public String GetTransitionAction() {
    return Dispatch.get( comConnector, "TransitionAction" ).getString();
  }

  public String GetTransitionEvent() {
    return Dispatch.get( comConnector, "TransitionEvent" ).getString();
  }

  public String GetTransitionGuard() {
    return Dispatch.get( comConnector, "TransitionGuard" ).getString();
  }

  public String GetVirtualInheritance() {
    return Dispatch.get( comConnector, "VirtualInheritance" ).getString();
  }

  public int GetWidth() {
    return Dispatch.get( comConnector, "Width" ).getInt();
  }

  public void SetDirection( String direction ) {
    Dispatch.put( comConnector, "Direction", direction );
  }

  public void SetDiagramID( int diagramID ) {
    Dispatch.put( comConnector, "DiagramID", diagramID );
  }

  public void SetTransitionEvent( String transitionEvent ) {
    Dispatch.put( comConnector, "TransitionEvent", transitionEvent );
  }

  public void SetSequenceNo( int sequenceNo ) {
    Dispatch.put( comConnector, "SequenceNo", sequenceNo );
  }

  public void SetTransitionGuard( String guard ) {
    Dispatch.put( comConnector, "TransitionGuard", guard );
  }

  public Collection<ConnectorConstraint> GetConstraints() {
    Dispatch comColl = Dispatch.get( comConnector, "Constraints").toDispatch();
    return new Collection<ConnectorConstraint>( comColl, ConnectorConstraint.class );
  }
}
