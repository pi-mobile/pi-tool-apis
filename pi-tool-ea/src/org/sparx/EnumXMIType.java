package org.sparx;

/**
 * Created by pru on 08.12.2015.
 */
public enum EnumXMIType {
  xmiEADefault,
  xmiRoseDefault,
  xmiEA10,
  xmiEA11,
  xmiEA12,
  xmiRose10,
  xmiRose11,
  xmiRose12,
  xmiMOF13,
  xmiMOF14,
  xmiEA20,
  xmiEA21;
}
