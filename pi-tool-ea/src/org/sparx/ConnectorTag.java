package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class ConnectorTag {

  private Dispatch comConnectorTag;

  public ConnectorTag( Dispatch comConnectorTag ) {
    this.comConnectorTag = comConnectorTag;
  }

  public String GetName() {
    return Dispatch.get( comConnectorTag, "Name" ).getString();
  }

  public String GetNotes() {
    return Dispatch.get( comConnectorTag, "Notes" ).getString();
  }

  public void SetNotes( String notes ) {
    Dispatch.put( comConnectorTag, "Notes", notes );
  }

  public ObjectType GetObjectType() {
    int typeID = Dispatch.get( comConnectorTag, "ObjectType" ).getInt();
    return ObjectType.values()[typeID];
  }

  public String GetTagGUID() {
    return Dispatch.get( comConnectorTag, "TagGUID" ).getString();
  }

  public int GetTagID() {
    return Dispatch.get( comConnectorTag, "TagID" ).getInt();
  }

  public int GetConnectorID() {
    return Dispatch.get( comConnectorTag, "ConnectorID" ).getInt();
  }

  public String GetValue() {
    return Dispatch.get( comConnectorTag, "Value" ).getString();
  }

  public void SetValue( String value ) {
    Dispatch.put( comConnectorTag, "Value", value );
  }

  public void Update() {
    Dispatch.call( comConnectorTag, "Update" );
  }
}
