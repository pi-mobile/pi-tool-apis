package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Method {

  private Dispatch comMethod;

  public Method( Dispatch comMethod ) {
    this.comMethod = comMethod;
  }

  public int GetMethodID() {
    return Dispatch.get( comMethod, "MethodID" ).getInt();
  }

  public String GetMethodGUID() {
    return Dispatch.get( comMethod, "MethodGUID" ).getString();
  }

  public String GetStereotype() {
    return Dispatch.get( comMethod, "Stereotype" ).getString();
  }

  public void SetStereotype( String stereotype ) {
    Dispatch.put( comMethod, "Stereotype", stereotype );
  }

  public void Update() {
    Dispatch.call( comMethod, "Update" );
  }

  public Collection<Parameter> GetParameters() {
    Dispatch comColl = Dispatch.get( comMethod, "Parameters").toDispatch();
    return new Collection<Parameter>( comColl, Parameter.class );
  }

  public String GetReturnType() {
    return Dispatch.get( comMethod, "ReturnType" ).getString();
  }

  public String GetClassifierID() {
    return Dispatch.get( comMethod, "ClassifierID" ).getString();
  }

  public String GetVisibility() {
    return Dispatch.get( comMethod, "Visibility" ).getString();
  }

  public int GetParentID() {
    return Dispatch.get( comMethod, "ParentID" ).getInt();
  }

  public String GetName() {
    return Dispatch.get( comMethod, "Name" ).getString();
  }

  public void SetNotes( String notes ) {
    Dispatch.put( comMethod, "Notes", notes );
  }

  public String GetNotes() {
    return Dispatch.get( comMethod, "Notes" ).getString();
  }

  public void SetStyle( String style ) {
    Dispatch.put( comMethod, "Style", style );
  }

  public String GetStyle() {
    return Dispatch.get( comMethod, "Style" ).getString();
  }

  public void SetVisibility( String visibility ) {
    Dispatch.put( comMethod, "Visibility", visibility );
  }

  public boolean GetReturnIsArray() {
    return Dispatch.get( comMethod, "ReturnIsArray" ).getBoolean();
  }

  public void SetReturnIsArray( boolean returnIsArray ) {
    Dispatch.put( comMethod, "ReturnIsArray", returnIsArray );
  }

  public Collection<MethodTag> GetTaggedValues() {
    Dispatch comColl = Dispatch.get( comMethod, "TaggedValues").toDispatch();
    return new Collection<MethodTag>( comColl, MethodTag.class );
  }

  public void SetPos( short pos ) {
    Dispatch.put( comMethod, "Pos", pos );
  }

  public void SetReturnType( String returnTypeName ) {
    Dispatch.put( comMethod, "ReturnType", returnTypeName );
  }

  public String GetBehavior() {
    return Dispatch.get( comMethod, "Behavior" ).getString();
  }

  public void SetBehavior( String behaviorGUID ) {
    Dispatch.put( comMethod, "Behavior", behaviorGUID );
  }

  public Dispatch getDispatch() {
    return comMethod;
  }
}
