package org.sparx;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Created by pru on 08.12.2015.
 */
public class Repository {

  private ActiveXComponent ea;
  private Dispatch repository;

  public Repository() {
    ea = new ActiveXComponent( "EA.Repository" );
    repository = ea.getObject();
  }

  public void OpenFile( String canonicalPath ) {
    ea.invoke( "OpenFile", canonicalPath );
  }
  public boolean initialized(){
    return repository != null;
  }

  public void OpenFile2( String canonicalPath, String userName, String password ) {
    Variant paramPath = new Variant( canonicalPath );
    Variant paramUser = new Variant( userName );
    Variant paramPassword = new Variant( password );
    ea.invoke( "OpenFile2", paramPath, paramUser, paramPassword );
  }

  public void ShowInProjectView( Object object ) {
    if(object instanceof Diagram) {
      Dispatch.call( repository, "ShowInProjectView", ((Diagram)object).getDispatch() );
    }
    else if(object instanceof Package) {
      Dispatch.call( repository, "ShowInProjectView", ((Package)object).getDispatch() );
    }
    else if(object instanceof Method) {
      Dispatch.call( repository, "ShowInProjectView", ((Method)object).getDispatch() );
    }
    else if(object instanceof Attribute) {
      Dispatch.call( repository, "ShowInProjectView", ((Attribute)object).getDispatch() );
    }
    else if(object instanceof Element) {
      Dispatch.call( repository, "ShowInProjectView", ((Element)object).getDispatch() );
    }
    else {
      Dispatch.call( repository, "ShowInProjectView", object );
    }
  }

  public Project GetProjectInterface() {
    return new Project( Dispatch.call( repository, "GetProjectInterface").toDispatch() );
  }

  public Element GetElementByID( int elementID ) {
    Variant variant = Dispatch.call( repository, "GetElementByID", elementID );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Element( variant.toDispatch() );
    }
  }

  public Element GetElementByGuid( String elementGuid ) {
    Variant variant = Dispatch.call( repository, "GetElementByGuid", elementGuid );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Element( variant.toDispatch() );
    }
  }

  public boolean CreateModel( CreateModelType cmEAPFromBase, String eapFileName, int i ) {
    return Dispatch.call( repository, "CreateModel", cmEAPFromBase, eapFileName, i ).getBoolean();
  }

  public Collection<Package> GetModels() {
    Dispatch pkgColl = Dispatch.get( repository, "Models").toDispatch();
    return new Collection<Package>( pkgColl, Package.class );
  }

  public Package GetPackageByID( int packageID ) {
    Variant variant = Dispatch.call( repository, "GetPackageByID", packageID );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Package( variant.toDispatch() );
    }
  }

  public Package GetPackageByGuid( String packageGuid ) {
    Variant variant = Dispatch.call( repository, "GetPackageByGuid", packageGuid );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Package( variant.getDispatch() );
    }
  }



  public Method GetOperationByGuid( String operationGUID ) {
    Variant variant = Dispatch.call( repository, "GetMethodByGuid", operationGUID );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Method( variant.getDispatch() );
    }
  }

  public Diagram GetDiagramByID( int diagramID ) {
    Variant variant = Dispatch.call( repository, "GetDiagramByID", diagramID );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Diagram( variant.toDispatch() );
    }
  }

  public Diagram GetDiagramByGuid( String diagramGUID ) {
    Variant variant = Dispatch.call( repository, "GetDiagramByGuid", diagramGUID );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Diagram( variant.toDispatch() );
    }
  }

  public Connector GetConnectorByID( int connectorID ) {
    Variant variant = Dispatch.call( repository, "GetConnectorByID", connectorID );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Connector( variant.toDispatch() );
    }
  }

  public Connector GetConnectorByGuid( String connectorGuid ) {
    Variant variant = Dispatch.call( repository, "GetConnectorByGuid", connectorGuid );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Connector( variant.toDispatch() );
    }
  }

  public Attribute GetAttributeByGuid( String attributeGuid ) {
    Variant variant = Dispatch.call( repository, "GetAttributeByGuid", attributeGuid );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Attribute( variant.toDispatch() );
    }
  }

  public Attribute GetAttributeByID( int attributeID ) {
    Variant variant = Dispatch.call( repository, "GetAttributeByID", attributeID );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Attribute( variant.toDispatch() );
    }
  }

  public Method GetMethodByGuid( String methodGuid ) {
    Variant variant = Dispatch.call( repository, "GetMethodByGuid", methodGuid );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Method( variant.toDispatch() );
    }
  }

  public Method GetMethodByID( int methodID ) {
    Variant variant = Dispatch.call( repository, "GetMethodByID", methodID );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Method( variant.toDispatch() );
    }
  }

  public Collection<Author> GetAuthors() {
    Variant variant = Dispatch.call( repository, "GetAuthors" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Collection<Author>( variant.toDispatch(), Author.class );
    }
  }

  public void ShowWindow( int i ) {
    Dispatch.call( repository, "ShowWindow", i );
    // ea.invoke( "ShowWindow", i );
  }

  public Object GetTreeSelectedObject() {
    Variant variant = Dispatch.call( repository, "GetTreeSelectedObject" );
    if (variant.isNull()) {
      return null;
    }
    else {
      int typeID = Dispatch.get( variant.toDispatch(), "ObjectType" ).getInt();
      switch (ObjectType.values()[typeID]) {
        case otDiagram:
          return new Diagram( variant.toDispatch() );
        case otPackage:
          return new Package( variant.toDispatch() );
        case otMethod:
          return new Method( variant.toDispatch() );
        case otAttribute:
          return new Attribute( variant.toDispatch() );
        default:
          return new Element( variant.toDispatch() );
      }
    }
  }

  public Diagram GetCurrentDiagram() {
    Variant variant = Dispatch.call( repository, "GetCurrentDiagram" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Diagram( variant.toDispatch() );
    }
  }

  public Collection<Element> GetTreeSelectedElements() {
    Variant variant = Dispatch.call( repository, "GetTreeSelectedElements" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Collection<Element>( variant.toDispatch(), Element.class );
    }
  }


  public void Execute( String sql ) {
    Dispatch.call( repository, "Execute", sql );
  }

  public String SQLQuery( String sql ) {
    return Dispatch.call( repository, "SQLQuery", sql ).getString();
  }

  public Collection<String> GetReferenceList( String type ) {
    Variant variant = Dispatch.call( repository, "GetReferenceList", type );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Collection<String>( variant.toDispatch(), String.class );
    }
  }

  /**
   * Reloads a Package or the entire model, updating the user interface.
   * @param packageID the ID of the Package to reload: if 0, the entire model is reloaded; if a valid Package ID, only that Package is reloaded
   */
  public void RefreshModelView( int packageID ) {
    Dispatch.call( repository, "RefreshModelView", packageID );
  }

  public void CloseFile() {
    Dispatch.call( repository, "CloseFile" );
  }

  public void Exit() {
    Dispatch.call( repository, "Exit" );
  }

  /**
   * Saves an open diagram; assumes the diagram is open in the main user interface Tab list.
   * @param diagramID the ID of the diagram to save
   */
  public void SaveDiagram( int diagramID ) {
    Dispatch.call( repository, "SaveDiagram", diagramID );
  }

  public void SaveAllDiagrams() {
    Dispatch.call( repository, "SaveAllDiagrams" );
  }

  /**
   * Reloads a specified diagram. This would commonly be used to refresh a visible diagram after
   * code import/export or other batch process where the diagram requires complete refreshing.
   * <p>
   * Calling this method within a call to EA_OnNotifyContextItemModified is not supported
   * @param diagramID the ID of the diagram to be reloaded
   */
  public void ReloadDiagram( int diagramID ) {
    Dispatch.call( repository, "ReloadDiagram", diagramID );
  }

  public boolean IsSecurityEnabled() {
    return Dispatch.call( repository, "IsSecurityEnabled" ).getBoolean();
  }

  public String GetLastError() {
    return Dispatch.call( repository, "GetLastError" ).getString();
  }

  /**
   * The filename/connection string of the current Repository.
   * For a connection string, the DBMS repository type is identified by "DBType=n;" where n is a number
   * corresponding to the DBMS type, as shown:<UL>
   *  <LI> 0 - MYSQL </LI>
   *  <LI> 1 - SQLSVR </LI>
   *  <LI> 2 - ADOJET </LI>
   *  <LI> 3 - ORACLE </LI>
   *  <LI> 4 - POSTGRES </LI>
   *  <LI> 5 - ASA </LI>
   *  <LI> 8 - ACCESS2007 </LI>
   *  <LI> 9 - FIREBIRD </LI>
   *  </UL>
   * @return The connection string
   */
  public String GetConnectionString() {
    return Dispatch.get( repository, "ConnectionString" ).getString();
  }

  public String GetProjectGUID() {
    return Dispatch.get( repository, "ProjectGUID" ).getString();
  }

  public String GetFormatFromField( String format, String text ) {
    return Dispatch.call( repository, "GetFormatFromField", format, text ).getString();
  }

  public String GetFieldFromFormat( String format, String text ) {
    return Dispatch.call( repository, "GetFieldFromFormat", format, text ).getString();
  }

  public ObjectType GetTreeSelectedItemType() {
    int typeID = Dispatch.get( repository, "GetTreeSelectedItemType" ).getInt();
    return ObjectType.values()[typeID];
  }

  public int InvokeConstructPicker( String filter ) {
    return Dispatch.call( repository, "InvokeConstructPicker", filter ).getInt();
  }

  public Collection<Element> GetElementsByQuery( String helperSearchName, String s ) {
    try {
      Variant variant = Dispatch.call( repository, "GetElementsByQuery", helperSearchName, s );
      if (variant.isNull()) {
        return null;
      }
      else {
        return new Collection<Element>( variant.toDispatch(), Element.class );
      }
    }
    catch (Exception e) {
      return null;
    }
  }

  public void AddDefinedSearches( String searchXml ) {
    Dispatch.call( repository, "AddDefinedSearches", searchXml );
  }

  public void RunModelSearch( String sQueryName, String sSearchTerm, String sSearchOptions, String sSearchData ) {
    Dispatch.call( repository, "RunModelSearch", sQueryName, sSearchTerm, sSearchOptions, sSearchData );
  }

  public void CloseDiagram( int diagramID ) {
    Dispatch.call( repository, "CloseDiagram", diagramID);
  }

  public void OpenDiagram( int diagramID ) {
    Dispatch.call( repository, "OpenDiagram", diagramID);
  }

  public void WriteOutput( String logging_tab, String message, int i ) {
    Dispatch.call( repository, "WriteOutput", logging_tab, message, i);
  }

  public void CreateOutputTab( String logging_tab ) {
    Dispatch.call( repository, "CreateOutputTab", logging_tab);
  }

  public void EnsureOutputVisible( String logging_tab ) {
    Dispatch.call( repository, "EnsureOutputVisible", logging_tab);
  }

  public void ClearOutput( String logging_tab ) {
    Dispatch.call( repository, "ClearOutput", logging_tab);
  }

  /**
   * Set this property to True when your automation client has to rapidly insert many elements, operations,
   * attributes and/or operation parameters.
   * <p>
   * Set to False when work is complete.
   * <p>
   * This can result in 10- to 20-fold improvement in adding new elements in bulk.
   * @param enable
   */
  public void BatchAppend( boolean enable ) {
    Dispatch.put( repository, "BatchAppend", enable);
  }

  /**
   * An optimization for pre-loading Package objects when dealing with large sets of automation objects.
   * @param enable
   */
  public void EnableCache( boolean enable ) {
    Dispatch.put( repository, "EnableCache", enable);
  }

  /**
   * Set this property to False to improve the performance of changes to the model; for example, bulk addition
   * of elements to a Package. To reveal changes to the user, call 'Repository.RefreshModelView()'.
   * @param enable
   */
  public void EnableUIUpdates( boolean enable ) {
    Dispatch.put( repository, "EnableUIUpdates", enable);
  }

  /**
   * Reloads the diagram contents for all open diagrams from the repository.
   * @param fullReload if false only the contents of element compartments are reloaded; if true the full content of each  diagram is reloaded
   */
  public void RefreshOpenDiagrams( boolean fullReload ) {
    Dispatch.call( repository, "RefreshOpenDiagrams", fullReload);
  }

  public DocumentGenerator CreateDocumentGenerator() {

    Variant variant = Dispatch.call( repository, "CreateDocumentGenerator");
    if (variant.isNull()) {
      return null;
    }
    else {
      return new DocumentGenerator( variant.toDispatch() );
    }
  }

  /**
   * If security is not enabled in the repository, an error is generated.
   * If GetGuid is True, a GUID generated by Enterprise Architect representing the user is returned;
   * otherwise the text as entered in System Users/User Details/Login is returned.
   * @param getGUID if true return user GUID otherwise login name
   * @return GUID oder login name
   */
  public String GetCurrentLoginUser( boolean getGUID ) {
    if (IsSecurityEnabled()) {
      return Dispatch.call( repository, "GetCurrentLoginUser", getGUID ).getString();
    }
    return "";
  }

  /**
   * Loads all Add-Ins from a repository when Enterprise Architect is opened from automation.
   */
  public void LoadAddins() {
    Dispatch.call( repository, "LoadAddins" );
  }

  public int LibraryVersion() {
    return Dispatch.call( repository, "LibraryVersion" ).getInt();
  }

  public String GetTechnologyVersion( String profileID ) {
    return Dispatch.call( repository, "GetTechnologyVersion", profileID ).getString();
  }

  public boolean IsTechnologyLoaded( String profileID ) {
    return Dispatch.call( repository, "IsTechnologyLoaded", profileID ).getBoolean();
  }

  public boolean IsTechnologyEnabled( String profileID ) {
    return Dispatch.call( repository, "IsTechnologyEnabled", profileID ).getBoolean();
  }

  public void CustomCommand( String object, String synchProfile, String stereotype ) {
    Dispatch.call( repository, "CustomCommand", object, synchProfile, stereotype);
  }

  public void SynchProfile (String profile, String stereotype) {
    Dispatch.call( repository, profile, stereotype);
  }

  /**
   * Returns the currently open database/repository type.
   * Can return one of these values:<UL>
   *   <LI> JET (.EAP file, MS Access 97 to 2013 format) </LI>
   *   <LI> ACCESS2007 (.accdb file, MS Access 2007+ format) </LI>
   *   <LI> ASA (Sybase SQL Anywhere) </LI>
   *   <LI> SQLSVR (Microsoft SQL Server) </LI>
   *   <LI> MYSQL (MySQL) </LI>
   *   <LI> ORACLE (Oracle) </LI>
   *   <LI> POSTGRES (PostgreSQL) </LI>
   *</UL>
   * @return the repository type
   */
  public String GetRepositoryType() {
    return Dispatch.call( repository, "RepositoryType" ).getString();
  }
}
