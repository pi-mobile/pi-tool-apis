package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class TaggedValue {

  private Dispatch comTaggedValue;

  public TaggedValue(Dispatch comTaggedValue) {
    this.comTaggedValue = comTaggedValue;
  }

  public void Update() {
    Dispatch.call( comTaggedValue, "Update" );
  }

  public int GetPropertyID() {
    return Dispatch.get( comTaggedValue, "PropertyID" ).getInt();
  }

  public String GetPropertyGUID() {
    return Dispatch.get( comTaggedValue, "PropertyGUID" ).getString();
  }

  public int GetElementID() {
    return Dispatch.get( comTaggedValue, "ElementID" ).getInt();
  }

  public String GetName() {
    return Dispatch.get( comTaggedValue, "Name" ).getString();
  }

  public String GetValue() {
    return Dispatch.get( comTaggedValue, "Value" ).getString();
  }

  public String GetNotes() {
    return Dispatch.get( comTaggedValue, "Notes" ).getString();
  }

  public void SetValue( String value ) {
    Dispatch.put( comTaggedValue, "Value", value );
  }

  public void SetNotes( String notes ) {
    Dispatch.put( comTaggedValue, "Notes", notes );
  }
}
