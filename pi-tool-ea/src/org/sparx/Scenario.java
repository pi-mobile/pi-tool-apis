package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Scenario {

  private Dispatch comScenario;

  public Scenario( Dispatch comScenario ) {
    this.comScenario = comScenario;
  }

  public void Update() {
    Dispatch.call( comScenario, "Update" );
  }

  public int GetType() {
    return Dispatch.get( comScenario, "Type" ).getInt();
  }

  public String GetScenarioGUID() {
    return Dispatch.get( comScenario, "ScenarioGUID" ).getString();
  }

  public String GetName() {
    return Dispatch.get( comScenario, "Name" ).getString();
  }

  public String GetNotes() {
    return Dispatch.get( comScenario, "Notes" ).getString();
  }

  public void SetNotes( String notes ) {
    Dispatch.put( comScenario, "Notes", notes );
  }

  public Collection<ScenarioStep> GetSteps() {
    Dispatch comColl = Dispatch.get( comScenario, "Steps").toDispatch();
    return new Collection<ScenarioStep>( comColl, ScenarioStep.class );
  }
}
