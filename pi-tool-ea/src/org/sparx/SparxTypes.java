package org.sparx;

/**
 * Created by cga on 17.01.2017.
 */
public enum SparxTypes {
  ActionPin,
  ActivityParameter,
  DiagramObject,
  Action,
  Activity,
  Node,
  Connector,
  Port,
  Interface,
  Component,
  RequiredInterface,
  ProvidedInterface;
}
