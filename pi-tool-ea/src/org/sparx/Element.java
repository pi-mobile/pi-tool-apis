package org.sparx;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

import java.util.Date;

/**
 * Created by pru on 08.12.2015.
 */
public class Element extends Releasable {
  
  public Element( Dispatch comObject ) {
    super(comObject);
  }

  public Collection<Element> GetElements() {
    Dispatch pkgColl = Dispatch.get( getComObject(), "Elements").toDispatch();
    return new Collection<Element>( pkgColl, Element.class );
  }

  public Collection<Diagram> GetDiagrams() {
    Dispatch pkgColl = Dispatch.get( getComObject(), "Diagrams").toDispatch();
    return new Collection<Diagram>( pkgColl, Diagram.class );
  }

  /**
   * Returns the (UML-)type of the element.
   *
   * @return
   */
  public String GetType() {
    return Dispatch.get( getComObject(), "Type" ).getString();
  }

  public String GetElementGUID() {
    return Dispatch.get( getComObject(), "ElementGUID" ).getString();
  }

  public void SetParentID( int parentID ) {
    Dispatch.put( getComObject(), "ParentID", parentID );
  }

  public void Update() {
    Dispatch.call( getComObject(), "Update" );
  }

  public void Refresh() {
    Dispatch.call( getComObject(), "Refresh" );
  }

  public String GetVersion() {
    return Dispatch.get( getComObject(), "Version" ).getString();
  }

  public void SetVersion( String version ) {
    Dispatch.put( getComObject(), "Version", version );
  }

  public String GetAuthor() {
    return Dispatch.get( getComObject(), "Author" ).getString();
  }

  public void SetAuthor( String author ) {
    Dispatch.put( getComObject(), "Author", author );
  }

  /**
   * The primary element stereotype; the first of the list of stereotypes you can access
   * using the 'StereotypeEx' attribute.
   * @return
   */
  public String GetStereotype() {
    return Dispatch.get( getComObject(), "Stereotype" ).getString();
  }

  /**
   * The fully-qualified stereotype name in the format "Profile::Stereotype".
   * One or more fully-qualified stereotype names can be assigned to StereotypeEx.
   * @return
   */
  public String GetFQStereotype() {
    return Dispatch.get( getComObject(), "FQStereotype" ).getString();
  }

  /**
   * The primary element stereotype; the first of the list of stereotypes you can access
   * using the 'StereotypeEx' attribute.
   * @param stereotype
   */
  public void SetStereotype( String stereotype ) {
    Dispatch.put( getComObject(), "Stereotype", stereotype );
  }

  /**
   * All the applied stereotypes of the element in a comma-separated list.
   * Reading the value will provide the stereotype name only;
   * assigning the value accepts either fully-qualified or simple names.
   * @param stereotype
   */
  public void SetStereotypeEx( String stereotype ) {
    Dispatch.put( getComObject(), "StereotypeEx", stereotype );
  }
  public void SetName( String name ) {
    Dispatch.put( getComObject(), "Name", name );
  }

  public String GetAlias() {
    return Dispatch.get( getComObject(), "Alias" ).getString();
  }

  public void SetAlias( String alias ) {
    Dispatch.put( getComObject(), "Alias", alias );
  }

  public String GetVisibility() {
    return Dispatch.get( getComObject(), "Visibility" ).getString();
  }

  public void SetVisibility( String visStr ) {
    Dispatch.put( getComObject(), "Visibility", visStr );
  }

  public Collection<Connector> GetConnectors() {
    Dispatch comColl = Dispatch.get( getComObject(), "Connectors").toDispatch();
    return new Collection<Connector>( comColl, Connector.class );
  }

  public int GetElementID() {
    return Dispatch.get( getComObject(), "ElementID" ).getInt();
  }

  public int GetClassifierID() {
    return Dispatch.get( getComObject(), "ClassifierID" ).getInt();
  }

  public String GetName() {
    return Dispatch.get( getComObject(), "Name" ).getString();
  }

  public void SetClassifierID( int classifierID ) {
    Dispatch.put( getComObject(), "ClassifierID", classifierID );
  }

  public int GetPackageID() {
    return Dispatch.get( getComObject(), "PackageID" ).getInt();
  }

  public int GetParentID() {
    return Dispatch.get( getComObject(), "ParentID" ).getInt();
  }

  public Collection<TaggedValue> GetTaggedValues() {
    Dispatch comColl = Dispatch.get( getComObject(), "TaggedValues").toDispatch();
    return new Collection<TaggedValue>( comColl, TaggedValue.class );
  }

  public void SetTag( String keywords ) {
    Dispatch.put( getComObject(), "Tag", keywords );
  }

  public Properties GetProperties() {
    Dispatch comColl = Dispatch.get( getComObject(), "Properties").toDispatch();
    return new Properties( comColl );
  }

  public Collection<CustomProperty> GetCustomProperties() {
    // throw  new RuntimeException("Do not us this function. New written properties (with sql) are not up to date without get the element again using the EAConnection. Use EAConnection.GetCustomProperty() instead ");
    Dispatch comColl = Dispatch.get( getComObject(), "CustomProperties").toDispatch();
    return new Collection<CustomProperty>( comColl, CustomProperty.class );
  }

  public int GetTreePos() {
    return Dispatch.get( getComObject(), "TreePos" ).getInt();
  }

  public void SetTreePos( int pos ) {
    Dispatch.put( getComObject(), "TreePos", pos );
  }

  public void SetRunState( String runState ) {
    Dispatch.put( getComObject(), "RunState", runState );
  }

  /**
   * Indicates if the element is Abstract (1) or Concrete (0).
   * @return
   */
  public String GetAbstract() {
    return Dispatch.get( getComObject(), "Abstract" ).getString();
  }

  public String GetActionFlags() {
    return Dispatch.get( getComObject(), "ActionFlags" ).getString();
  }

  public String GetClassifierName() {
    return Dispatch.get( getComObject(), "ClassifierName" ).getString();
  }

  public String GetClassifierType() {
    return Dispatch.get( getComObject(), "ClassifierType" ).getString();
  }

  public String GetComplexity() {
    return Dispatch.get( getComObject(), "Complexity" ).getString();
  }

  public Date GetCreated() {
    return Dispatch.get( getComObject(), "Created" ).getJavaDate();
  }

  public String GetDifficulty() {
    return Dispatch.get( getComObject(), "Difficulty" ).getString();
  }

  public String GetEventFlags() {
    return Dispatch.get( getComObject(), "EventFlags" ).getString();
  }

  public String GetExtensionPoints() {
    return Dispatch.get( getComObject(), "ExtensionPoints" ).getString();
  }

  public String GetGenfile() {
    return Dispatch.get( getComObject(), "Genfile" ).getString();
  }

  public String GetGenlinks() {
    return Dispatch.get( getComObject(), "Genlinks" ).getString();
  }

  public String GetGentype() {
    return Dispatch.get( getComObject(), "Gentype" ).getString();
  }

  public String GetHeader1() {
    return Dispatch.get( getComObject(), "Header1" ).getString();
  }

  public String GetHeader2() {
    return Dispatch.get( getComObject(), "Header2" ).getString();
  }

  public boolean GetIsActive() {
    return Dispatch.get( getComObject(), "IsActive" ).getBoolean();
  }

  public boolean GetIsLeaf() {
    return Dispatch.get( getComObject(), "IsLeaf" ).getBoolean();
  }

  public boolean GetIsNew() {
    return Dispatch.get( getComObject(), "IsNew" ).getBoolean();
  }

  public boolean GetIsSpec() {
    return Dispatch.get( getComObject(), "IsSpec" ).getBoolean();
  }

  public String GetLastError() {
    return Dispatch.call( getComObject(), "GetLastError" ).getString();
  }

  public String GetLinkedDocument() {
    return Dispatch.call( getComObject(), "GetLinkedDocument" ).getString();
  }

  public boolean ApplyUserLock() {
    return Dispatch.call( getComObject(), "ApplyUserLock").getBoolean();
  }

  public boolean ReleaseUserLock() {
    return Dispatch.call( getComObject(), "ReleaseUserLock").getBoolean();
  }

  public boolean GetLocked() {
    return Dispatch.get( getComObject(), "Locked" ).getBoolean();
  }

  public String GetMetaType() {
    return Dispatch.get( getComObject(), "MetaType" ).getString();
  }

  public Date GetModified() {
    return Dispatch.get( getComObject(), "Modified" ).getJavaDate();
  }

  public String GetMultiplicity() {
    return Dispatch.get( getComObject(), "Multiplicity" ).getString();
  }

  public String GetNotes() {
    return Dispatch.get( getComObject(), "Notes" ).getString();
  }

  /**
   * Returns the kind of an object, such as otElement, otPackage, ...
   * CAUTION: this is not the (UML-)type of the element, see {@link #GetType()}!
   *
   * @return
   */
  public ObjectType GetObjectType() {
    int typeID = Dispatch.get( getComObject(), "ObjectType" ).getInt();
    return ObjectType.values()[typeID];
  }

  public String GetPersistence() {
    return Dispatch.get( getComObject(), "Persistence" ).getString();
  }

  public String GetPhase() {
    return Dispatch.get( getComObject(), "Phase" ).getString();
  }

  public String GetPriority() {
    return Dispatch.get( getComObject(), "Priority" ).getString();
  }

  public int GetPropertyType() {
    return Dispatch.get( getComObject(), "PropertyType" ).getInt();
  }

  public String GetRunState() {
    return Dispatch.get( getComObject(), "RunState" ).getString();
  }

  public String GetStatus() {
    return Dispatch.get( getComObject(), "Status" ).getString();
  }

  public void SetStatus( String status ) {
    Dispatch.put( getComObject(), "Status", status );
  }

  /**
   * All the applied stereotypes of the element in a comma-separated list. Reading the value will provide the stereotype name only; assigning the value accepts either fully-qualified or simple names.
   * @return
   */
  public String GetStereotypeEx() {
    return Dispatch.get( getComObject(), "StereotypeEx" ).getString();
  }

  public String GetStereotypeList() {
    return Dispatch.call( getComObject(), "GetStereotypeList" ).getString();
  }

  public String GetStyleEx() {
    return Dispatch.get( getComObject(), "StyleEx" ).getString();
  }

  public int GetSubtype() {
    return Dispatch.get( getComObject(), "Subtype" ).getInt();
  }

  public String GetTablespace() {
    return Dispatch.get( getComObject(), "Tablespace" ).getString();
  }

  public String GetTag() {
    return Dispatch.get( getComObject(), "Tag" ).getString();
  }

  public Collection<Attribute> GetAttributes() {
    Dispatch comColl = Dispatch.get( getComObject(), "Attributes" ).toDispatch();
    return new Collection<Attribute>( comColl, Attribute.class );
  }

  public Collection<Attribute> GetAttributesEx() {
    Dispatch comColl = Dispatch.get( getComObject(), "AttributesEx" ).toDispatch();
    return new Collection<Attribute>( comColl, Attribute.class );
  }

  public Collection<Element> GetEmbeddedElements() {
    Dispatch comColl = Dispatch.get( getComObject(), "EmbeddedElements" ).toDispatch();
    return new Collection<Element>( comColl, Element.class );
  }

  public Collection<Partition> GetPartitions() {
    Dispatch comColl = Dispatch.get( getComObject(), "Partitions").toDispatch();
    return new Collection<Partition>( comColl, Partition.class );
  }

  public int MiscData( int index ) {
    Variant var = Dispatch.call( getComObject(), "MiscData", index );
    try{
      return var.getInt();
    }
    catch (IllegalStateException ex){
      return Integer.parseInt( var.getString() );
    }
  }

  public Collection<Issue> GetIssues() {
    Dispatch comColl = Dispatch.get( getComObject(), "Issues").toDispatch();
    return new Collection<Issue>( comColl, Issue.class );
  }

  public void SetPackageID( int packageID ) {
    Dispatch.put( getComObject(), "PackageID", packageID );
  }

  public void SetAbstract( String abstractStr ) {
    Dispatch.put( getComObject(), "Abstract", abstractStr );
  }

  public Collection<Method> GetMethods() {
    Dispatch comColl = Dispatch.get( getComObject(), "Methods").toDispatch();
    return new Collection<Method>( comColl, Method.class );
  }

  public void SetSubtype( int value ) {
    Dispatch.put( getComObject(), "Subtype", value );
  }

  public void SetClassifierName( String name ) {
    Dispatch.put( getComObject(), "ClassifierName", name );
  }


  public Collection<Element> GetRealizes() {
    Dispatch comColl = Dispatch.get( getComObject(), "Realizes").toDispatch();
    return new Collection<Element>( comColl, Element.class );
  }

  public void SetNotes( String notes ) {
    Dispatch.put( getComObject(), "Notes", notes );
  }

  public Dispatch getDispatch() {
    return getComObject();
  }

  public Diagram GetCompositeDiagram() {
    Variant variant = Dispatch.get( getComObject(), "CompositeDiagram" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Diagram( variant.toDispatch() );
    }
  }

  public int GetInstanceID() {
    return Dispatch.get( getComObject(), "InstanceID" ).getInt();
  }

  @Override
  public String toString() {
    return GetName() + " (id="+GetElementID()+", type="+GetType()+")";
  }

  public boolean LoadLinkedDocument(String filename) {
    return Dispatch.call( getComObject(), "LoadLinkedDocument", filename ).getBoolean();
  }

  /**
   * Sets the composite diagram of the element.
   * @param diagramGUID the GUID of the composite diagram; a blank GUID will remove the link to the composite diagram
   * @return
   */
  public boolean SetCompositeDiagram( String diagramGUID) {
    return Dispatch.call( getComObject(), "SetCompositeDiagram", diagramGUID ).getBoolean();
  }

  public boolean GetIsComposite() {
    return Dispatch.get( getComObject(), "IsComposite" ).getBoolean();
  }

  public void SetIsComposite( boolean composite ) {
    if (composite) {
      Dispatch.put( getComObject(), "IsComposite", 1 );
    }
    else {
      Dispatch.put( getComObject(), "IsComposite", 0 );
    }
  }

  public Collection<Scenario> GetScenarios() {
    Dispatch comColl = Dispatch.get( getComObject(), "Scenarios").toDispatch();
    return new Collection<Scenario>( comColl, Scenario.class );
  }
  
  public Collection<Constraint> GetConstraints() {
    Dispatch comColl = Dispatch.get( getComObject(), "Constraints").toDispatch();
    return new Collection<Constraint>( comColl, Constraint.class );
  }
}
