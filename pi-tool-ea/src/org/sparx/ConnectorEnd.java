package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class ConnectorEnd {

  private Dispatch comConnectorEnd;

  public ConnectorEnd( Dispatch comConnectorEnd ) {
    this.comConnectorEnd = comConnectorEnd;
  }

  /**
   * The type of Aggregation as it applies to this end; valid values are:<UL>
   * <LI>0 = None</LI>
   * <LI>1 = Shared</LI>
   * <LI>2 = Composite</LI>
   * </UL>
   * @return type of Aggregation
   */
  public int GetAggregation() {
    return Dispatch.get( comConnectorEnd, "Aggregation" ).getInt();
  }

  public void SetAggregation( int kind ) {
    Dispatch.put( comConnectorEnd, "Aggregation", kind );
  }

  public String GetAlias() {
    return Dispatch.get( comConnectorEnd, "Alias" ).getString();
  }

  public boolean GetAllowDuplicates() {
    return Dispatch.get( comConnectorEnd, "AllowDuplicates" ).getBoolean();
  }

  public String GetCardinality() {
    return Dispatch.get( comConnectorEnd, "Cardinality" ).getString();
  }

  public String GetConstraint() {
    return Dispatch.get( comConnectorEnd, "Constraint" ).getString();
  }

  public String GetEnd() {
    return Dispatch.get( comConnectorEnd, "End" ).getString();
  }

  public String GetNavigable() {
    return Dispatch.get( comConnectorEnd, "Navigable" ).getString();
  }

  public int GetOrdering() {
    return Dispatch.get( comConnectorEnd, "Ordering" ).getInt();
  }

  public String GetQualifier() {
    return Dispatch.get( comConnectorEnd, "Qualifier" ).getString();
  }

  public String GetRole() {
    return Dispatch.get( comConnectorEnd, "Role" ).getString();
  }

  public String GetRoleNote() {
    return Dispatch.get( comConnectorEnd, "RoleNote" ).getString();
  }

  public String GetRoleType() {
    return Dispatch.get( comConnectorEnd, "RoleType" ).getString();
  }

  public String GetVisibility() {
    return Dispatch.get( comConnectorEnd, "Visibility" ).getString();
  }

  public void SetNavigable( String navigable ) {
    Dispatch.put( comConnectorEnd, "Navigable", navigable );
  }

  public void SetCardinality( String cardinality ) {
    Dispatch.put( comConnectorEnd, "Cardinality", cardinality );
  }

  public void Update() {
    Dispatch.call( comConnectorEnd, "Update" );
  }

  public void SetRole( String role ) {
    Dispatch.put( comConnectorEnd, "Role", role );
  }

  public boolean GetIsNavigable() {
    return Dispatch.get( comConnectorEnd, "IsNavigable" ).getBoolean();
  }

  public void SetRoleNote( String roleNote ) {
    Dispatch.put( comConnectorEnd, "RoleNote", roleNote );
  }

  public String GetIsChangeable() {
    return Dispatch.get( comConnectorEnd, "IsChangeable" ).getString();
  }

  public String GetStereotype() {
    return Dispatch.get( comConnectorEnd, "Stereotype" ).getString();
  }

  public void SetStereotype( String stereotype ) {
    Dispatch.put( comConnectorEnd, "Stereotype", stereotype );
  }
}
