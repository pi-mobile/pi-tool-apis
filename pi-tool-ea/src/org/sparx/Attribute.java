package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Attribute {

  private Dispatch comAttribute;

  public Attribute( Dispatch comAttribute ) {
    this.comAttribute = comAttribute;
  }

  public int GetAttributeID() {
    return Dispatch.get( comAttribute, "AttributeID" ).getInt();
  }

  public String GetAttributeGUID() {
    return Dispatch.get( comAttribute, "AttributeGUID" ).getString();
  }

  public int GetParentID() {
    return Dispatch.get( comAttribute, "ParentID" ).getInt();
  }

  public String GetLowerBound() {
    return Dispatch.get( comAttribute, "LowerBound" ).getString();
  }

  public String GetUpperBound() {
    return Dispatch.get( comAttribute, "UpperBound" ).getString();
  }

  public boolean GetIsOrdered() {
    return Dispatch.get( comAttribute, "IsOrdered" ).getBoolean();
  }

  public boolean GetAllowDuplicates() {
    return Dispatch.get( comAttribute, "AllowDuplicates" ).getBoolean();
  }

  public boolean GetIsConst() {
    return Dispatch.get( comAttribute, "IsConst" ).getBoolean();
  }

  public String GetName() {
    return Dispatch.get( comAttribute, "Name" ).getString();
  }

  public String GetType() {
    return Dispatch.get( comAttribute, "Type" ).getString();
  }

  public int GetClassifierID() {
    return Dispatch.get( comAttribute, "ClassifierID" ).getInt();
  }

  public String GetStereotype() {
    return Dispatch.get( comAttribute, "Stereotype" ).getString();
  }

  public void SetStereotype( String stereotype ) {
    Dispatch.put( comAttribute, "Stereotype", stereotype );
  }

  public void Update() {
    Dispatch.call( comAttribute, "Update" );
  }

  public String GetVisibility() {
    return Dispatch.get( comAttribute, "Visibility" ).getString();
  }

  public void SetVisibility( String visibility ) {
    Dispatch.put( comAttribute, "Visibility", visibility );
  }

  public String GetDefault() {
    return Dispatch.get( comAttribute, "Default" ).getString();
  }

  public void SetDefault( String defaultValue ) {
    Dispatch.put( comAttribute, "Default", defaultValue );
  }

  public String GetStyle() {
    return Dispatch.get( comAttribute, "Style" ).getString();
  }

  public String GetStyleEx() {
    return Dispatch.get( comAttribute, "StyleEx" ).getString();
  }

  public void SetType( String type ) {
    Dispatch.put( comAttribute, "Type", type );
  }

  public void SetClassifierID( int classifierID ) {
    Dispatch.put( comAttribute, "ClassifierID", classifierID );
  }

  public void SetUpperBound( String upperBound ) {
    Dispatch.put( comAttribute, "UpperBound", upperBound );
  }

  public String GetContainer() {
    return Dispatch.get( comAttribute, "Container" ).getString();
  }

  public void SetContainer( String notes ) {
    Dispatch.put( comAttribute, "Container", notes );
  }

  public String GetNotes() {
    return Dispatch.get( comAttribute, "Notes" ).getString();
  }

  public void SetNotes( String notes ) {
    Dispatch.put( comAttribute, "Notes", notes );
  }

  public Dispatch getDispatch() {
    return comAttribute;
  }

  public Collection<AttributeTag> GetTaggedValues() {
    Dispatch comColl = Dispatch.get( comAttribute, "TaggedValues").toDispatch();
    return new Collection<AttributeTag>( comColl, AttributeTag.class );
  }

  public Collection<AttributeConstraint> GetConstraints() {
    Dispatch comColl = Dispatch.get( comAttribute, "Constraints").toDispatch();
    return new Collection<AttributeConstraint>( comColl, AttributeConstraint.class );
  }
}
