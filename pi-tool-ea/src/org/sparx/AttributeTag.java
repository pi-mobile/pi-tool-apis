package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by cga on 03.03.2016.
 */
public class AttributeTag {

  private Dispatch comAttributeTag;

  public AttributeTag(Dispatch comTaggedValue) {
    this.comAttributeTag = comTaggedValue;
  }


  public String GetName() {
    return Dispatch.get( comAttributeTag, "Name" ).getString();
  }

  public String GetValue() {
    return Dispatch.get( comAttributeTag, "Value" ).getString();
  }

  public void SetValue(String value) {
    Dispatch.put( comAttributeTag, "Value" , value);
  }

  public void Update() {
    Dispatch.call( comAttributeTag, "Update");
  }
}
