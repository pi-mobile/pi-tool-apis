package org.sparx;

import com.jacob.com.Dispatch;
import com.jacob.com.DispatchProxy;

/**
 * Created by cga on 15.03.2016.
 */
public class Releasable {

  private Dispatch comObject;

  public Releasable(Dispatch comObject){
    this.comObject = comObject;
  }

  public void release() {
    getComObject().safeRelease();
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    release();
  }

  public Dispatch getComObject() {
    return comObject;
  }
}
