package org.sparx;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by cga on 09.12.2015.
 */
public class SwimlaneIterator implements Iterator {

  private Swimlanes swimlanes;
  private short position = -1;

  public SwimlaneIterator( Swimlanes swimlanes ) {
    this.swimlanes = swimlanes;
  }

  /**
   * Returns {@code true} if the iteration has more elements.
   * (In other words, returns {@code true} if {@link #next} would
   * return an element rather than throwing an exception.)
   *
   * @return {@code true} if the iteration has more elements
   */
  @Override
  public boolean hasNext() {
    return (this.swimlanes.GetCount() > (position + 1));
  }

  /**
   * Returns the next element in the iteration.
   *
   * @return the next element in the iteration
   * @throws NoSuchElementException if the iteration has no more elements
   */
  @Override
  public Swimlane next() {
    if (!hasNext()) {
      throw new NoSuchElementException();
    }
    else {
      position++;
      return this.swimlanes.GetAt( position );
    }
  }
}
