package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class ScenarioStep {

  private Dispatch comScenario;

  public ScenarioStep( Dispatch comScenario ) {
    this.comScenario = comScenario;
  }

  public void Update() {
    Dispatch.call( comScenario, "Update" );
  }

  public String GetUses() {
    return Dispatch.get( comScenario, "Uses" ).getString();
  }

  public String GetStepGUID() {
    return Dispatch.get( comScenario, "StepGUID" ).getString();
  }

  public String GetName() {
    return Dispatch.get( comScenario, "Name" ).getString();
  }

  public void SetName( String name ) {
    Dispatch.put( comScenario, "Name", name );
  }

  public String GetState() {
    return Dispatch.get( comScenario, "State" ).getString();
  }

  public String GetResults() {
    return Dispatch.get( comScenario, "Results" ).getString();
  }



}
