package org.sparx;

import com.jacob.com.Dispatch;

import java.util.Iterator;

/**
 * Created by pru on 09.12.2015.
 */
public class Properties implements Iterable<Property> {

  private Dispatch collection;

  public Properties( Dispatch collection ) {
    this.collection = collection;
  }

  public Property AddNew( String childName, String childType ) {
    Dispatch entry = Dispatch.call( collection, "AddNew", childName, childType ).toDispatch();
    return new Property( entry );
  }

  public void Refresh() {
    Dispatch.call( collection, "Refresh" );
  }

  /**
   * Returns an iterator over elements of type {@code T}.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<Property> iterator() {
    return new PropertyIterator( this );
  }

  public int GetCount() {
    return Dispatch.call( collection, "Count" ).getInt();
  }

  public Property Item( int i ) {
    Dispatch entry = Dispatch.call( collection, "Item", i ).toDispatch();
    return new Property( entry );
  }

  public void Delete( short i ) {
    Dispatch.call( collection, "Delete", i ).toDispatch();
  }
}
