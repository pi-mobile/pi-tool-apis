package org.sparx;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by pru on 09.12.2015.
 */
public class PropertyIterator implements Iterator<Property> {

  private Properties collection;
  private int position = -1;

  public PropertyIterator( Properties collection ) {
    this.collection = collection;
  }

  /**
   * Returns {@code true} if the iteration has more elements.
   * (In other words, returns {@code true} if {@link #next} would
   * return an element rather than throwing an exception.)
   *
   * @return {@code true} if the iteration has more elements
   */
  @Override
  public boolean hasNext() {
    return (this.collection.GetCount() > (position + 1));
  }

  /**
   * Returns the next element in the iteration.
   *
   * @return the next element in the iteration
   * @throws NoSuchElementException if the iteration has no more elements
   */
  @Override
  public Property next() {
    if (!hasNext()) {
      throw new NoSuchElementException();
    }
    else {
      position++;
      return this.collection.Item( position );
    }
  }
}
