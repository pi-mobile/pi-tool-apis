package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Parameter {

  private Dispatch comParameter;

  public Parameter( Dispatch comParameter ) {
    this.comParameter = comParameter;
  }

  public String GetName() {
    return Dispatch.get( comParameter, "Name" ).getString();
  }

  public String GetType() {
    return Dispatch.get( comParameter, "Type" ).getString();
  }

  public String GetParameterGUID() {
    return Dispatch.get( comParameter, "ParameterGUID" ).getString();
  }

  public String GetStereotype() {
    return Dispatch.get( comParameter, "Stereotype" ).getString();
  }

  public void SetNotes( String notes ) {
    Dispatch.put( comParameter, "Notes", notes );
  }

  public void Update() {
    Dispatch.call( comParameter, "Update" );
  }

  public String GetDefault() {
    return Dispatch.get( comParameter, "Default" ).getString();
  }

  public void SetDefault( String newDefault ) {
    Dispatch.put( comParameter, "Default", newDefault );
  }

  public String GetKind() {
    return Dispatch.get( comParameter, "Kind" ).getString();
  }

  public String GetClassifierID() {
    return Dispatch.get( comParameter, "ClassifierID" ).getString();
  }

  public void SetName( String name ) {
    Dispatch.put( comParameter, "Name", name );
  }

  public void SetType( String parameterType ) {
    Dispatch.put( comParameter, "Type", parameterType );
  }

  public void SetPosition( int index ) {
    Dispatch.put( comParameter, "Position", index );
  }

  public void SetClassifierID( String elementGUID ) {
    Dispatch.put( comParameter, "ClassifierID", elementGUID );
  }

  public String GetNotes() {
    return Dispatch.get( comParameter, "Notes" ).getString();
  }
}
