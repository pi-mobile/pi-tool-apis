package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Property {

  private Dispatch comProperty;

  public Property( Dispatch comProperty ) {
    this.comProperty = comProperty;
  }

  public String GetName() {
    return Dispatch.get( comProperty, "Name" ).getString();
  }

  public String GetValue() {
    return Dispatch.get( comProperty, "Value" ).getString();
  }

  public PropType GetType() {
    int typeID = Dispatch.get( comProperty, "ObjectType" ).getInt();
    return PropType.values()[typeID];
  }

  public String GetValidation() {
    return Dispatch.get( comProperty, "Validation" ).getString();
  }
}
