package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Partition {

  private Dispatch comPartition;

  public Partition( Dispatch comPartition ) {
    this.comPartition = comPartition;
  }

  public String GetName() {
    return Dispatch.get( comPartition, "Name" ).getString();
  }

  public String GetNote() {
    return Dispatch.get( comPartition, "Note" ).getString();
  }

  public String GetOperator() {
    return Dispatch.get( comPartition, "Operator" ).getString();
  }

  public int GetSize() {
    return Dispatch.get( comPartition, "Size" ).getInt();
  }

  public void SetSize( int size ) {
    Dispatch.put( comPartition, "Size", size );
  }

  public void SetName( String name ) {
    Dispatch.put( comPartition, "Name", name );
  }
}
