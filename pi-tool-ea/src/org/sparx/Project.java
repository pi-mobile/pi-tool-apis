package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Project {

  private Dispatch comProject;

  public Project( Dispatch comProject ) {
    this.comProject = comProject;
  }

  public String GUIDtoXML( String identifier ) {
    return Dispatch.call( comProject, "GUIDtoXML", identifier ).toString();
  }

  public void CreateBaseline( String xmlGUID, String versionName, String versionNotes ) {
    Dispatch.call( comProject, "CreateBaseline", xmlGUID, versionName, versionNotes );
  }

  public void ExportPackageXMI( String xmlGUID, EnumXMIType xmiType, int i, int i1, int i2, int i3, String filePath ) {
    Dispatch.call( comProject, "ExportPackageXMI", xmlGUID, xmiType, i, i1, i2, i3, filePath );
  }

  public String ImportPackageXMI( String xmlGUID, String filePath, int importDiagrams, int stripGUID ) {
    return Dispatch.call( comProject, "ImportPackageXMI", xmlGUID, filePath, importDiagrams, stripGUID ).getString();
  }

  public boolean PutDiagramImageOnClipboard( String diagramGUID, int type ) {
    return Dispatch.call( comProject, "PutDiagramImageOnClipboard", diagramGUID, type ).getBoolean();
  }

  public boolean PutDiagramImageToFile( String diagramGUID, String fileName, int type ) {
    return Dispatch.call( comProject, "PutDiagramImageToFile", diagramGUID, fileName, type ).getBoolean();
  }

  public boolean SaveDiagramImageToFile( String fileName ) {
    return Dispatch.call( comProject, "SaveDiagramImageToFile", fileName ).getBoolean();
  }

  public boolean ProjectTransfer( String sourceFilePath, String targetFilePath, String logFilePath ) {
    return Dispatch.call( comProject, "ProjectTransfer", sourceFilePath, targetFilePath, logFilePath ).getBoolean();
  }

  /**
   *
   * @param fileName
   * @param dataSets
   * @return
   *
   * @apiNote since EA 13.5
   */
  public boolean ImportReferenceData (String fileName, String dataSets) {
    return Dispatch.call( comProject, "ImportReferenceData", fileName, dataSets ).getBoolean();
  }

  /**
   *
   * @param fileName
   * @param tables
   * @return
   *
   * @apiNote since EA 13.5
   */
  public boolean ExportReferenceData (String fileName, String tables) {
    return Dispatch.call( comProject, "ExportReferenceData", fileName, tables ).getBoolean();
  }

  public void ShowWindow( int i ) {
    Dispatch.call( comProject, "ShowWindow", i );
  }
}
