package org.sparx;

import com.jacob.com.Dispatch;

import java.util.Date;

/**
 * Created by pru on 08.12.2015.
 */
public class Issue {

  private Dispatch comIssue;

  public Issue( Dispatch comIssue ) {
    this.comIssue = comIssue;
  }

  public void SetResolver( String resolver ) {
    Dispatch.put( comIssue, "Resolver", resolver );
  }

  public void SetPriority( String prio ) {
    Dispatch.put( comIssue, "Priority", prio );
  }

  public void SetDateResolved( Date resolvedDate ) {
    Dispatch.put( comIssue, "DateResolved", resolvedDate );
  }

  public void SetNotes( String notes ) {
    Dispatch.put( comIssue, "Notes", notes );
  }

  public void SetVersion( String version ) {
    Dispatch.put( comIssue, "Version", version );
  }

  public void SetStatus( String status ) {
    Dispatch.put( comIssue, "Status", status );
  }

  public void Update() {
    Dispatch.call( comIssue, "Update" );
  }
}
