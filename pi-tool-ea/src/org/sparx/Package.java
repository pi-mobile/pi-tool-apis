package org.sparx;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Created by pru on 08.12.2015.
 */
public class Package extends Releasable {

  String packageGUID = null;
  int packageID = -1;

  public Package( Dispatch comObject ) {
    super(comObject);
  }

  public Dispatch getDispatch() { return getComObject(); }

  public int GetPackageID() {
    if (packageID < 0) {
      packageID = Dispatch.get( getComObject(), "PackageID" ).getInt();
    }
    return packageID;
  }

  public String GetPackageGUID() {
    if (packageGUID == null) {
      packageGUID = Dispatch.get( getComObject(), "PackageGUID" ).getString();
    }
    return packageGUID;
  }

  public Element GetElement() {
    Variant variant = Dispatch.get( getComObject(), "Element" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Element( variant.toDispatch() );
    }
  }

  public Collection<Package> GetPackages() {
    Dispatch pkgColl = Dispatch.get( getComObject(), "Packages").toDispatch();
    return new Collection<Package>( pkgColl, Package.class );
  }

  public void Update() {
    Dispatch.call( getComObject(), "Update" );
  }

  public String GetVersion() {
    return Dispatch.get( getComObject(), "Version" ).getString();
  }

  public int GetParentID() {
    return Dispatch.get( getComObject(), "ParentID" ).getInt();
  }

  public String GetName() {
    return Dispatch.get( getComObject(), "Name" ).getString();
  }

  public void SetName( String name ) {
    Dispatch.put( getComObject(), "Name", name );
  }

  public void SetParentID( int parentID ) {
    Dispatch.put( getComObject(), "ParentID", parentID );
  }

  public String GetStereotypeEx() {
    return Dispatch.get( getComObject(), "StereotypeEx" ).getString();
  }

  public String GetNotes() {
    return Dispatch.get( getComObject(), "Notes" ).getString();
  }

  public boolean GetIsNamespace() {
    return Dispatch.get( getComObject(), "IsNamespace" ).getBoolean();
  }

  public Collection<Element> GetElements() {
    Dispatch elementColl = Dispatch.get( getComObject(), "Elements").toDispatch();
    return new Collection<Element>( elementColl, Element.class );
  }

  public Collection<Diagram> GetDiagrams() {
    Dispatch diagramColl = Dispatch.get( getComObject(), "Diagrams").toDispatch();
    return new Collection<Diagram>( diagramColl, Diagram.class );
  }

  public String GetAlias() {
    return Dispatch.get( getComObject(), "Alias" ).getString();
  }

  public int GetTreePos() {
    return Dispatch.get( getComObject(), "TreePos" ).getInt();
  }
  public void SetTreePos( int pos ) {
    Dispatch.put( getComObject(), "TreePos", pos );
  }

  public boolean ApplyUserLock() {
    return Dispatch.call( getComObject(), "ApplyUserLock").getBoolean();
  }

  public boolean ReleaseUserLock() {
    return Dispatch.call( getComObject(), "ReleaseUserLock").getBoolean();
  }

  public boolean ApplyUserLockRecursive() {
    return Dispatch.call( getComObject(), "ApplyUserLockRecursive", true, true, true).getBoolean();
  }

  public boolean ReleaseUserLockRecursive() {
    return Dispatch.call( getComObject(), "ReleaseUserLockRecursive", true, true, true).getBoolean();
  }

  public Package Clone() {
    Variant variant = Dispatch.call( getComObject(), "Clone" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Package( variant.toDispatch() );
    }
  }
}
