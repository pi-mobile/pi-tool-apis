package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Constraint applied to an Element.
 *
 * Key: ObjectID + Name + Type
 */
public class Constraint extends Releasable {

  public Constraint( Dispatch comObject ) {
    super(comObject);
  }

  public void SetName( String name ) {
    Dispatch.put( getComObject(), "Name", name );
  }

  public String GetName() {
    return Dispatch.get( getComObject(), "Name" ).getString();
  }

  public void SetNotes( String notes ) {
    Dispatch.put( getComObject(), "Notes", notes );
  }

  public String GetNotes() {
    return Dispatch.get( getComObject(), "Notes" ).getString();
  }

  /**
   * Returns the kind of an object, such as otElement, otPackage, ...
   * CAUTION: this is not the (UML-)type of the element, see {@link #GetType()}!
   *
   * @return
   */
  public ObjectType GetObjectType() {
    int typeID = Dispatch.get( getComObject(), "ObjectType" ).getInt();
    return ObjectType.values()[typeID];
  }

  public void SetParentID( int parentID ) {
    Dispatch.put( getComObject(), "ParentID", parentID );
  }

  public int GetParentID() {
    return Dispatch.get( getComObject(), "ParentID" ).getInt();
  }

  public void SetStatus( String status ) {
    Dispatch.put( getComObject(), "Status", status );
  }

  public String GetStatus() {
    return Dispatch.get( getComObject(), "Status" ).getString();
  }

  /**
   * Returns the Constraint type.
   *
   * @return
   */
  public String GetType() {
    return Dispatch.get( getComObject(), "Type" ).getString();
  }

  public int GeWeight() {
    return Dispatch.get( getComObject(), "Weight" ).getInt();
  }

  public String GetLastError() {
    return Dispatch.call( getComObject(), "GetLastError" ).getString();
  }

  public void Update() {
    Dispatch.call( getComObject(), "Update" );
  }
}
