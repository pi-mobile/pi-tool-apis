package org.sparx;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

import java.lang.reflect.Constructor;
import java.util.Iterator;

/**
 * Created by pru on 08.12.2015.
 */
public class Collection<E extends Object> extends Releasable implements Iterable<E> {

  private Class elementClass;

  public Collection( Dispatch collection, Class elementClass ) {
    super(collection);
    this.elementClass = elementClass;
  }

  public E AddNew( String childName, String childType ) {
    Variant variant = Dispatch.call( getComObject(), "AddNew", childName, childType );
    if (variant.isNull()) {
      return null;
    }
    else {
      try {
        Constructor<E> contstructor = elementClass.getConstructor( Dispatch.class );
        E result = contstructor.newInstance( variant.toDispatch() );
        return result;
      }
      catch (Exception ex) {
        throw new IllegalArgumentException( "Could not create elementClass="+elementClass, ex );
      }
    }
  }

  public void Refresh() {
    Dispatch.call( getComObject(), "Refresh" );
  }

  /**
   * Returns an iterator over elements of type {@code T}.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<E> iterator() {
    return new CollectionIterator<E>( this );
  }

  public short GetCount() {
    return Dispatch.call( getComObject(), "Count" ).getShort();
  }

  public E GetAt( short i ) {
    try {
      Variant variant = Dispatch.call( getComObject(), "GetAt", i );
      if (variant.isNull()) {
        return null;
      }
      else {
        Constructor<E> contstructor = elementClass.getConstructor( Dispatch.class );
        E result = contstructor.newInstance( variant.toDispatch() );
        return result;
      }
    }
    catch (Exception ex) {
      return null;
    }
  }

  public void Delete( short id ) {
    Dispatch.call( getComObject(), "Delete", id );
  }

  public void DeleteAt( short id, boolean refresh ) {
    Dispatch.call( getComObject(), "DeleteAt", id, refresh );
  }

  public E GetByName( String name ) {
    try {
      Variant variant = Dispatch.call( getComObject(), "GetByName", name);
      if (variant.isNull()) {
        return null;
      }
      else {
        Constructor<E> contstructor = elementClass.getConstructor( Dispatch.class );
        E result = contstructor.newInstance( variant.toDispatch() );
        return result;
      }
    }
    catch (Exception ex) {
      return null;
    }
  }
}
