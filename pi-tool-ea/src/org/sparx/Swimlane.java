package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Swimlane {

  private Dispatch comSwimlane;

  public Swimlane( Dispatch comSwimlane ) {
    this.comSwimlane = comSwimlane;
  }

  public int GetWidth() {
    return Dispatch.get( comSwimlane, "Width" ).getInt();
  }

  public String GetTitle() {
    return Dispatch.get( comSwimlane, "Title" ).getString();
  }
}
