package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class DiagramLink {

  private Dispatch comDiagramLink;

  public DiagramLink( Dispatch comDiagramLink) {
    this.comDiagramLink = comDiagramLink;
  }

  public ObjectType GetObjectType() {
    int objTypeInt =  Dispatch.get( comDiagramLink, "ObjectType" ).getInt();
    return ObjectType.values()[objTypeInt];
  }

  public String GetStyle() {
    return Dispatch.get( comDiagramLink, "Style" ).getString();
  }

  public String GetPath() {
    return Dispatch.get( comDiagramLink, "Path" ).getString();
  }

  public String GetGeometry() {
    return Dispatch.get( comDiagramLink, "Geometry" ).getString();
  }

  public boolean GetIsHidden() {
    return Dispatch.get( comDiagramLink, "IsHidden" ).getBoolean();
  }

  public String GetLastError() {
    return Dispatch.get( comDiagramLink, "LastError" ).getString();
  }

  public int GetConnectorID() {
    return Dispatch.get( comDiagramLink, "ConnectorID" ).getInt();
  }

  public void SetConnectorID( int connectorID ) {
    Dispatch.put( comDiagramLink, "ConnectorID", connectorID );
  }

  public void Update() {
    Dispatch.call( comDiagramLink, "Update" );
  }

  public int GetDiagramID() {
    return Dispatch.get( comDiagramLink, "DiagramID" ).getInt();
  }

  public int GetInstanceID() {
    return Dispatch.get( comDiagramLink, "InstanceID" ).getInt();
  }

  public void SetStyle( String style ) {
    Dispatch.put( comDiagramLink, "Style", style );
  }

  public void SetPath( String path ) {
    Dispatch.put( comDiagramLink, "Path", path );
  }

  public void SetGeometry( String geometry ) {
    Dispatch.put( comDiagramLink, "Geometry", geometry );
  }

  public void SetIsHidden( boolean ishidden ) {
    Dispatch.put( comDiagramLink, "IsHidden", ishidden );
  }
}
