package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class MethodTag {

  private Dispatch comMethodTag;

  public MethodTag( Dispatch comMethodTag ) {
    this.comMethodTag = comMethodTag;
  }

  public String GetName() {
    return Dispatch.get( comMethodTag, "Name" ).getString();
  }

  public String GetValue() {
    return Dispatch.get( comMethodTag, "Value" ).getString();
  }

  public void SetValue( String value ) {
    Dispatch.put( comMethodTag, "Value", value );
  }

  public void Update() {
    Dispatch.call( comMethodTag, "Update" );
  }
}
