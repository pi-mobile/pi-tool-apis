package org.sparx;

import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Author {

  private Dispatch comAuthor;

  public Author(Dispatch comAuthor) {
    this.comAuthor = comAuthor;
  }

  public String GetName() {
    return Dispatch.get( comAuthor, "Name" ).getString();
  }

  public ObjectType GetObjectType() {
    int typeID = Dispatch.get( comAuthor, "ObjectType" ).getInt();
    return ObjectType.values()[typeID];
  }

  public String GetNotes() {
    return Dispatch.get( comAuthor, "Notes" ).getString();
  }

  public String GetRoles() {
    return Dispatch.get( comAuthor, "Roles" ).getString();
  }
}
