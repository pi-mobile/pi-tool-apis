package org.sparx;

import java.lang.reflect.Constructor;
import java.util.Iterator;
import com.jacob.com.Dispatch;

/**
 * Created by pru on 08.12.2015.
 */
public class Swimlanes implements Iterable<Swimlane> {

  private Dispatch swimlanes;

  public Swimlanes( Dispatch swimlanes ) {
    this.swimlanes = swimlanes;
  }

  /**
   * Returns an iterator over elements of type {@code T}.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<Swimlane> iterator() {
    return new SwimlaneIterator( this );
  }


  public Swimlane Add( String title, int width ) {
    //TODO not yet implemented
    throw new RuntimeException( "TODO" );
  }

  public short GetCount() {
    return Dispatch.call( swimlanes, "Count" ).getShort();
  }

  public Swimlane GetAt( short i ) {
    Dispatch entry = Dispatch.call( swimlanes, "GetAt", i ).toDispatch();
    return new Swimlane(entry);
  }
}
