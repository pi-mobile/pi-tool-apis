package org.sparx;

/**
 * Created by pru on 08.12.2015.
 */
public enum ObjectType {
  otNone,
  otProject,
  otRepository,
  otCollection,
  otElement,
  otPackage,
  otModel,
  otConnector,
  otDiagram,
  otRequirement,
  otScenario,
  otConstraint,
  otTaggedValue,
  otFile,
  otEffort,
  otMetric,
  otIssue,
  otRisk,
  otTest,
  otDiagramObject,
  otDiagramLink,
  otResource,
  otConnectorEnd,
  otAttribute,
  otMethod,
  otParameter,
  otClient,
  otAuthor,
  otDatatype,
  otStereotype,
  otTask,
  otTerm,
  otProjectIssues,
  otAttributeConstraint,
  otAttributeTag,
  otMethodConstraint,
  otMethodTag,
  otConnectorConstraint,
  otConnectorTag,
  otProjectResource,
  otReference,
  otRoleTag,
  otCustomProperty,
  otPartition,
  otTransition,
  otEventProperty,
  otEventProperties,
  otPropertyType,
  otProperties,
  otProperty,
  otSwimlaneDef,
  otSwimlanes,
  otSwimlane,
  otModelWatcher,
  otStep,
  otExtension;
}
