package de.pidata.excel.api;

/**
 * Created by cga on 05.10.2016.
 */
public class XlSearchOrder {
  public static final int xlByRows = 1;
  public static final int xlByColumns = 2;
}
