package de.pidata.excel.api;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Created by pru on 08.12.2015.
 */
public class Workbook extends Releasable {

  public Workbook( Dispatch comObject ) {
    super(comObject);
  }

  public Sheet GetActiveSheet() {
    Variant variant = Dispatch.call( comObject, "ActiveSheet" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Sheet( variant.toDispatch() );
    }
  }

  public void Close() {
    Dispatch.call( comObject, "Close", Boolean.FALSE );
  }
}
