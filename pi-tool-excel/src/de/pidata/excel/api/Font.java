package de.pidata.excel.api;

import com.jacob.com.Dispatch;

/**
 * Created by cga on 26.02.2016.
 */
public class Font extends Releasable {

  public Font( Dispatch comFont ) {
    super(comFont);
  }

  public void Bold( boolean value ) {
    Dispatch.put( comObject, "Bold", Boolean.valueOf( value ) );
  }

  public void SetSize( int size ) {
    Dispatch.put( comObject, "Size", Integer.valueOf( size ) );
  }

}
