package de.pidata.excel.api;

import com.jacob.com.Dispatch;

/**
 * Created by cga on 26.02.2016.
 */
public class Rows extends Releasable{

  public Rows( Dispatch comRows ) {
    super(comRows);
  }

  public int GetCount() {
    return Dispatch.get(comObject, "Count").getInt();
  }

}
