package de.pidata.excel.api;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Created by cga on 26.02.2016.
 */
public class Window extends Releasable {


  public Window( Dispatch comObject ) {
    super(comObject);
  }

  public void SetFreezePanes( boolean value ) {
    Dispatch.put( comObject, "FreezePanes", Boolean.valueOf( value ) );
  }

  public void SetSplitColumn( int value ) {
    Dispatch.put( comObject, "SplitColumn", Integer.valueOf( value ) );
  }

  public void SetSplitRow( int value ) {
    Dispatch.put( comObject, "SplitRow", Integer.valueOf( value ) );
  }
}
