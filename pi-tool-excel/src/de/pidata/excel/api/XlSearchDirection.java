package de.pidata.excel.api;

/**
 * Created by cga on 05.10.2016.
 */
public class XlSearchDirection {
  public static final int xlNext = 1;
  public static final int xlPrevious = 2;
}
