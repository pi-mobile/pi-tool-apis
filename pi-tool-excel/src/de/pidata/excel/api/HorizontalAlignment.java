package de.pidata.excel.api;

public class HorizontalAlignment {

  public static final int Center = -4108;
  public static final int CenterAcrossSelection = 7;
  public static final int Distributed =	-4117;
  public static final int Fill = 5;
  public static final int General = 1; // Align according to data type
  public static final int Justify = -4130;
  public static final int Left = -4131;
  public static final int Right = -4152;

}
