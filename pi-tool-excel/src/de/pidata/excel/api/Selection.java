package de.pidata.excel.api;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Created by cga on 26.02.2016.
 */
public class Selection extends Releasable {

  public Selection( Dispatch comSelection ) {
    super(comSelection);
  }

  public double GetHeight() {
    return Dispatch.call( comObject, "Height" ).getDouble();
  }

  public double GetWidth() {
    return Dispatch.call( comObject, "Width" ).getDouble();
  }

  public void SetHeight( double newHeight ) {
    Dispatch.put( comObject, "Height", Double.valueOf( newHeight ) );
  }

  public void SetWidth( double newWidth ) {
    Dispatch.put( comObject, "Width", Double.valueOf( newWidth ) );
  }
}
