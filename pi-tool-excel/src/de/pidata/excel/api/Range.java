package de.pidata.excel.api;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Created by cga on 26.02.2016.
 */
public class Range extends Releasable {
  

  public Range( Dispatch comObject ) {
    super(comObject);
  }

  public Cell Cells(int row, int column){
    Variant variant = Dispatch.invoke(comObject, "Cells", Dispatch.Get, new Object[] {Integer.valueOf( row ), Integer.valueOf( column )}, new int[0]);
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Cell( variant.toDispatch() );
    }
  }

  public Rows Rows() {
    Variant variant = Dispatch.call( comObject, "Rows" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Rows( variant.toDispatch() );
    }
  }

  public Columns Columns() {
    Variant variant = Dispatch.call( comObject, "Columns" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Columns( variant.toDispatch() );
    }
  }
  public void release() {
    comObject.safeRelease();
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    release();
  }

  public Range EntireRow() {
    Variant variant = Dispatch.call( comObject, "EntireRow" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Range( variant.toDispatch() );
    }
  }

  // TODO: Get the Find working with all optional parameters.
  // e.g.: Range nameRange = headerRowRange.EntireRow().Find(STEREOTYPE, 0, XlFindLookIn.xlValues, XlLookAt.xlWhole, XlSearchOrder.xlByColumns, XlSearchDirection.xlNext, true, false, false);
  // public Range Find( Object What, int After, int LookIn, int LookAt, int SearchOrder, int SearchDirection, boolean MatchCase, boolean MatchByte, boolean SearchFormat ) {
  public Range Find( Object What ) {
    // Variant variant = Dispatch.call( comObject, "Find", What, After, LookIn, LookAt, SearchOrder, SearchDirection, MatchCase, MatchByte, SearchFormat);
    Variant variant = Dispatch.call( comObject, "Find", What);
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Range( variant.toDispatch() );
    }
  }

  public Range End( int xlDirection ) {
    Variant variant = Dispatch.call( comObject, "End", Integer.valueOf( xlDirection ) );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Range( variant.toDispatch() );
    }
  }

  public void SetValue2( String[][] value ) {
    Dispatch.put( comObject, "Value2", value );
  }

  public void CopyPicture( int xlPictureAppearance, int xlCopyPictureFormat ) {
    Dispatch.call( comObject, "CopyPicture", Integer.valueOf( xlPictureAppearance ), Integer.valueOf( xlCopyPictureFormat ) );
  }

  public void SetRowHeight( int size ) {
    Dispatch.put( comObject, "RowHeight", Integer.valueOf( size ) );
  }

  public int Column() {
    return Dispatch.call( comObject, "Column" ).getInt();
  }

  public void AutoFilter() {
    Dispatch.call( comObject, "AutoFilter" );
  }

  public void Select() {
    Dispatch.call( comObject, "Select" );
  }

  public void WrapText( boolean wrap ) {
    Dispatch.put( comObject, "WrapText", Boolean.valueOf( wrap ) );
  }

  public void SetVerticalAlignment( int alignment ) {
    Dispatch.put( comObject, "VerticalAlignment", Integer.valueOf( alignment ) );
  }

  public void SetHorizontalAlignment( int alignment ) {
    Dispatch.put( comObject, "HorizontalAlignment", Integer.valueOf( alignment ) );
  }

  public Font GetFont() {
    Variant variant = Dispatch.call( comObject, "Font" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Font( variant.toDispatch() );
    }
  }

  public Interior GetInterior() {
    Variant variant = Dispatch.call( comObject, "Interior" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Interior( variant.toDispatch() );
    }
  }
}
