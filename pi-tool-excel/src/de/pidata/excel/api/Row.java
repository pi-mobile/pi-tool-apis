package de.pidata.excel.api;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Created by cga on 26.02.2016.
 */
public class Row extends Releasable {

  public Row( Dispatch comRow ) {
    super(comRow);
  }


  public void SetRowHeight(double height) {
    Dispatch.put( comObject, "RowHeight", Double.valueOf( height ) );
  }

  public void SetVerticalAlignment( int alignment ) {
    Dispatch.put( comObject, "VerticalAlignment", Integer.valueOf( alignment ) );
  }
}
