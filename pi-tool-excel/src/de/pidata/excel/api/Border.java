package de.pidata.excel.api;

import com.jacob.com.Dispatch;

/**
 * Created by cga on 15.04.2024.
 */
public class Border extends Releasable {

  public Border( Dispatch comInterior ) {
    super(comInterior);
  }

  public void SetColorIndex( int colorIndex ) {
    Dispatch.put( comObject, "ColorIndex", Integer.valueOf( colorIndex ) );
  }

}
