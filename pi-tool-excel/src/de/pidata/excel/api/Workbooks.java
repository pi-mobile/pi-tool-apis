package de.pidata.excel.api;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Created by pru on 08.12.2015.
 */
public class Workbooks extends Releasable {

  public Workbooks( Dispatch comWorkBook ) {
    super(comWorkBook);
  }

  public Workbook Add() {
    Variant variant = Dispatch.call( comObject, "Add" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Workbook( variant.toDispatch() );
    }
  }

  public Workbook Open( String filePath ) {
    Variant variant = Dispatch.call( comObject, "Open", filePath );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Workbook( variant.toDispatch() );
    }
  }
}
