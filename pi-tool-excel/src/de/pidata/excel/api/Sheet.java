package de.pidata.excel.api;

import com.jacob.com.Dispatch;
import com.jacob.com.NotImplementedException;
import com.jacob.com.Variant;

/**
 * Created by cga on 26.02.2016.
 */
public class Sheet extends Releasable {
  

  public Sheet( Dispatch comObject ) {
    super(comObject);
  }

  public void Paste() {
    Dispatch.call(comObject, "Paste");
  }


  public Range GetUsedRange() {
    Variant variant = Dispatch.call( comObject, "UsedRange" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Range( variant.toDispatch() );
    }
  }

  public Range Rows(int index){
    Variant variant = Dispatch.invoke(comObject, "Rows", Dispatch.Get, new Object[] {Integer.valueOf( index )}, new int[0]);
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Range( variant.toDispatch() );
    }
  }

  public Range GetRange(String string){
    Variant variant = Dispatch.invoke(comObject, "Range", Dispatch.Get, new Object[] { string }, new int[1]);
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Range( variant.toDispatch() );
    }
  }

  public Range GetRange(Cell startCell, Cell endCell){

    Variant variant = Dispatch.invoke(comObject, "Range", Dispatch.Get, new Object[] { startCell.comObject, endCell.comObject }, new int[1]);
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Range( variant.toDispatch() );
    }
  }

  public void Activate() {
    Dispatch.call( comObject, "Activate" );
  }
}
