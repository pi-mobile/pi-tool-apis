package de.pidata.excel.api;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import de.pidata.jacob.JacobHelper;

/**
 * Created by cga on 26.02.2016.
 */
public class Excel {

  private ActiveXComponent excel;

  public Excel(){

    JacobHelper.loadJacob();
    // Init MTA for Jacob-COM to work with several (and this) threads.
    ComThread.InitMTA();

    excel = new ActiveXComponent( "Excel.Application" );
    Visible(true);
  }

  public void Quit() {
    excel.invoke( "Quit", new Variant[]{} );
  }


  public void Visible(boolean isVisible){
    excel.setProperty( "Visible", isVisible );
  }

  public Workbooks GetWorkbooks() {
    Variant variant = Dispatch.call( excel, "Workbooks" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Workbooks( variant.toDispatch() );
    }
  }

  public Sheet GetActiveSheet() {
    Variant variant = Dispatch.call( excel, "ActiveSheet" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Sheet( variant.toDispatch() );
    }
  }

  public Window GetActiveWindow() {
    Variant variant = Dispatch.call( excel, "ActiveWindow" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Window( variant.toDispatch() );
    }
  }

  public Cell Cells(int row, int column){
    Variant variant = Dispatch.invoke(excel, "Cells", Dispatch.Get, new Object[] {Integer.valueOf( row ), Integer.valueOf( column )}, new int[0]);
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Cell( variant.toDispatch() );
    }
  }

  public Row Rows(int index){
    Variant variant = Dispatch.invoke(excel, "Rows", Dispatch.Get, new Object[] {Integer.valueOf( index )}, new int[0]);
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Row( variant.toDispatch() );
    }
  }

  public Column Columns(int index){
    Variant variant = Dispatch.invoke(excel, "Columns", Dispatch.Get, new Object[] {Integer.valueOf( index )}, new int[0]);
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Column( variant.toDispatch() );
    }
  }

  public Selection GetSelection() {
    Variant variant = Dispatch.call( excel, "Selection" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Selection( variant.toDispatch() );
    }
  }
}
