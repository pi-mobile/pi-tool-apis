package de.pidata.excel.api;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Created by cga on 26.02.2016.
 */
public class Column extends Releasable {

  public Column( Dispatch comColumn ) {
    super(comColumn);
  }

  public void SetColumnWith( Double width ) {
    Dispatch.put( comObject, "ColumnWidth", width );
  }

  public double GetWidth() {
    return Dispatch.get(comObject, "Width").getDouble();
  }
}
