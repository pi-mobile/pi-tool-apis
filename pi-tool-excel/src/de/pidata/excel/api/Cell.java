package de.pidata.excel.api;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Created by cga on 26.02.2016.
 */
public class Cell extends Releasable {

  public Cell( Dispatch comCell ) {
    super(comCell);
  }

  public void SetValue( String value ) {
    Dispatch.put( comObject, "Value", value );
  }

  public void SetValue2( String value ) {
    Dispatch.put( comObject, "Value2", value );
  }

  public String GetValue( ) {
    String value = Dispatch.get( comObject, "Value").getString();
    if(value == null){
      return "";
    }
    return value;
  }

  public String GetValue2( ) {
    String value2 = Dispatch.get( comObject, "Value2").getString();
    if(value2 == null){
      return "";
    }
    return value2;
  }

  public Font GetFont() {
    Variant variant = Dispatch.call( comObject, "Font" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Font( variant.toDispatch() );
    }
  }

  public Interior GetInterior() {
    Variant variant = Dispatch.call( comObject, "Interior" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Interior( variant.toDispatch() );
    }
  }

  public Border GetBorder() {
    Variant variant = Dispatch.call( comObject, "Borders" );
    if (variant.isNull()) {
      return null;
    }
    else {
      return new Border( variant.toDispatch() );
    }
  }

  public void Select(){
    Dispatch.call( comObject, "Select" );
  }

  public void WrapText( boolean wrap ) {
    Dispatch.put( comObject, "WrapText", Boolean.valueOf( wrap ) );
  }

  public void SetHtmlString( String text ) {
    Dispatch.put( comObject, "Value", text );
  }
}
