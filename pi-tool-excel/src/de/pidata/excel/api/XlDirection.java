package de.pidata.excel.api;

/**
 * Created by cga on 05.10.2016.
 */
public class XlDirection {
  public static final int xlDown = -4121;
  public static final int xlToLeft = -4159;
  public static final int xlToRight = -4161;
  public static final int xlUp = -4162;

}
