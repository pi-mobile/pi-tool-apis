package de.pidata.excel.api;

import com.jacob.com.Dispatch;

/**
 * Created by cga on 26.02.2016.
 */
public class Interior extends Releasable {

  public Interior( Dispatch comInterior ) {
    super(comInterior);
  }

  public void SetColorIndex( int colorIndex ) {
    Dispatch.put( comObject, "ColorIndex", Integer.valueOf( colorIndex ) );
  }

}
