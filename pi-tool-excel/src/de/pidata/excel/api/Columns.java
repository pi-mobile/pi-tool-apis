package de.pidata.excel.api;

import com.jacob.com.Dispatch;

/**
 * Created by cga on 26.02.2016.
 */
public class Columns extends Releasable {

  public Columns( Dispatch comColumns ) {
    super(comColumns);
  }

  public int GetCount() {
    return Dispatch.get(comObject, "Count").getInt();
  }
}
