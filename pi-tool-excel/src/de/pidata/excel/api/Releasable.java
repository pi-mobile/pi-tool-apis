package de.pidata.excel.api;

import com.jacob.com.*;
/**
 * Created by cga on 15.03.2016.
 */
public class Releasable {

  protected Dispatch comObject;

  public Releasable(Dispatch comObject){
    this.comObject = comObject;
  }

  public void release() {
    comObject.safeRelease();
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    release();
  }

}
