package de.pidata.excel.api;

public class VerticalAlignment {

  public static final int Bottom = -4107;
  public static final int Center = -4108;
  public static final int Distributed =	-4117;
  public static final int Justify = -4130;
  public static final int Top =	-4160;

}
