/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.jacob;

import com.jacob.com.ComThread;
import com.jacob.com.LibraryLoader;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

/**
 * Helper to wrap jacob access, especially the DLL load mechanism.
 */
public class JacobHelper {

  public static final String JACOB_DEBUG = "com.jacob.debug";

  private final static Logger LOGGER = Logger.getLogger( JacobHelper.class.getName() );

  public static void enableDebug() {
    System.setProperty( JACOB_DEBUG, "true" );
  }

  /**
   * Tries to load the jacob DLL from the regular configured locations (PATH / java.library.path).
   * If this was not successful, tries to load the DLL from the directory in which the jacob.jar
   * was loaded.
   */
  public static void loadJacob() {

    // instruct jacob to manage memory more efficiently
    System.setProperty( "com.jacob.autogc", "true" );

    String dllName = LibraryLoader.getPreferredDLLName();
    try {
      System.loadLibrary( dllName );
      LOGGER.info( "successfully loaded " + dllName );
      return;
    }
    catch (Throwable thrown) {
      // not found, try loading next to jar
      LOGGER.info( dllName + " not present in PATH / java.library.path" );
    }

    // try local
    Path runningPath;
    try {
      runningPath = Paths.get( ComThread.class.getProtectionDomain().getCodeSource().getLocation().toURI() ).getParent();
    }
    catch (URISyntaxException e) {
      throw new RuntimeException( "error finding path of loaded jacob.jar", e );
    }

    Path lib = runningPath.resolve( dllName + ".dll" );
    if (!lib.toFile().exists()) {
      throw new RuntimeException( "runtime DLL not found: " + lib.toString() );
    }
    LOGGER.info( "try local copy: " + lib.toString() );
    System.setProperty( LibraryLoader.JACOB_DLL_PATH, lib.toString() );
    try {
      LibraryLoader.loadJacobLibrary();
      LOGGER.info( "...loaded" );
    }
    catch (Throwable thrown) {
      LOGGER.severe( "...failed" );
      thrown.printStackTrace();
      throw thrown;
    }
  }
}
