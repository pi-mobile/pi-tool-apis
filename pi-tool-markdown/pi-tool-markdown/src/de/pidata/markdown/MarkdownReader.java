package de.pidata.markdown;

import java.io.IOException;
import java.io.Reader;

public class MarkdownReader extends Reader {

  private final MarkdownParser parser;

  public MarkdownReader( MarkdownParser parser ) {
    this.parser = parser;
  }

  @Override
  public int read( char[] cbuf, int off, int len ) throws IOException {
    // Always return only one character to avoid parsing beyond end of html snippet
    int c = parser.nextChar();
    if (c == -1) {
      return -1;
    }
    else {
      cbuf[off] = (char) c;
      return 1;
    }
  }

  @Override
  public void close() throws IOException {
    throw new IllegalArgumentException( "CLose must be done by caller!" );
  }
}
