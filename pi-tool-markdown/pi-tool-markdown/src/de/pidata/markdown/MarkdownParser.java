package de.pidata.markdown;

import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.simple.StringType;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.parser.ParseException;
import de.pidata.parser.Parser;
import de.pidata.xhtml.model.xhtml.*;
import de.pidata.xhtml.model.xhtml.ModelFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class MarkdownParser extends Parser {

  private Map<String,String> frontMatterValues = new HashMap<>();
  private Xhtml_div_type htmlContainer;

  public synchronized final void parse( Xhtml_div_type htmlContainer, InputStream dataStream, String encoding ) throws IOException {
    initBuffer( dataStream, encoding );
    this.htmlContainer = htmlContainer;
    htmlContainer.namespaceTable().addNamespace( ModelFactory.NAMESPACE, "" );
    ModelFactoryTable.getInstance().getOrSetFactory( BaseFactory.NAMESPACE, BaseFactory.class );
    ModelFactoryTable.getInstance().getOrSetFactory( ModelFactory.NAMESPACE, ModelFactory.class );
    doParse();
  }

  private void parseToLineEnd( StringBuilder line ) throws IOException {
    parseUntil( line, "\n\r", true );
  }

  @Override
  public void doParse() throws IOException {
    while (true) {
      int ch = skipWhitespace( true );
      if (ch == -1) {
        return;
      }
      else if (ch == '-') {
        int ch2 = nextChar();
        int ch3 = nextChar();
        if ((ch2 == '-') && (ch3 == '-')) {
          parseFrontMatter();
        }
      }
      else if (ch == '#') {
        parseHeadline();
      }
      else if (ch == '*') {
        int ch2 = nextChar();
        if (ch2 == ' ') {
          Xhtml_ul_type ul = new Xhtml_ul_type();
          htmlContainer.addUl( ul );
          parseBullet( ul );
        }
        else if (ch2 == '*') {
          parseEmphasize( ch2, htmlContainer );
        }
      }
      else if (ch == '!') {
        parseImage( htmlContainer );
      }
      else if (ch == '|') {
        parseTable();
      }
      else if (ch == '`') {
        int ch2 = nextChar();
        if (ch2 == '`') {
          int ch3 = nextChar();
          if (ch3 == '`') {
            parseSourceCode();
          }
        }
      }
      else if (ch == '<') {
        parseHtml( htmlContainer );
      }
      else if (ch == '{') {
        parseCommand( htmlContainer );
      }
      else if ((ch >= '0') && (ch <= '9')) {
        int ch2 = nextChar();
        if (ch2 == '.') {
          int ch3 = nextChar();
          if (ch3 == ' ') {
            Xhtml_ol_type ol = new Xhtml_ol_type();
            htmlContainer.addOl( ol );
            parseNumline( ol );
          }
        }
        else {
          back();
          parseParagraph();
        }
      }
      else {
        back();
        parseParagraph();
      }
    }
  }

  private int parseText( Model htmlElement, String stopChars ) throws IOException {
    // TODO extract emphasize, images, links, ...
    StringBuilder buf = new StringBuilder();
    int ch = nextChar();
    char lastChar = 0;
    while ((ch >= 0) && (stopChars.indexOf( ch ) < 0)) {
      if (ch == '*') {
        buf = addCData( htmlElement, buf );
        ch = nextChar();
        parseEmphasize( ch, htmlElement );
      }
      else if (ch == '{') {
        parseCommand( htmlElement );
      }
      else if (ch == '!') {
        buf = addCData( htmlElement, buf );
        parseImage( htmlElement );
      }
      else if (ch == '[') {
        buf = addCData( htmlElement, buf );
        parseHyperlink( htmlElement );
      }
      else if (ch == '<') {
        buf = addCData( htmlElement, buf );
        parseHtml( htmlElement );
      }
      else {
        if ((ch == ' ') && (lastChar == ' ')) {
          // skip repeated space
        }
        else {
          buf.append( (char) ch );
        }
        lastChar = (char) ch;
      }
      ch = nextChar();
    }
    addCData( htmlElement, buf );
    return ch;
  }

  private static StringBuilder addCData( Model htmlElement, StringBuilder buf ) {
    if (buf.length() > 0) {
      String text = buf.toString();
      if (text.trim().length() > 0) {
        htmlElement.add( ComplexType.CDATA, new SimpleModel( StringType.getDefString(), text ) );
        buf = new StringBuilder();
      }
    }
    return buf;
  }

  private int parseTextLine( Model htmlElement ) throws IOException {
    return parseText( htmlElement, "\n" );
  }

  private void parseFrontMatter() throws IOException {
    StringBuilder buf = new StringBuilder();
    parseUntil( buf, "\n\r", true );
    // TODO check for Garbage
    while (true) {
      int ch = skipWhitespace( true );
      if (ch == '-') {
        StringBuilder line = new StringBuilder( "-" );
        parseToLineEnd( line );
        String str = line.toString().trim();
        if (str.equals( "---" )) {
          return;
        }
        else {
          throw new ParseException( "Expected end of FrontMatter: '---', but found '" + str + "'" );
        }
      }
      else {
        back();
        parseFrontMatterRow();
      }
    }
  }

  private void parseFrontMatterRow() throws IOException {
    StringBuilder keyBuf = new StringBuilder();
    int lastChar = parseUntil( keyBuf, ":\n", true );
    if (lastChar == ':') {
      StringBuilder valueBuf = new StringBuilder();
      parseToLineEnd( valueBuf );
      frontMatterValues.put( keyBuf.toString(), valueBuf.toString() );
    }
    else {
      throw new ParseException( "Error parsing FrontMatter key, expected ':', but found '"+(char) lastChar+"'" );
    }
  }

  private String processCommand( String cmd ) {
    if (cmd.startsWith( "{% link " )) {
      int start = "{% link ".length();
      int end = cmd.indexOf( ' ', start );
      return cmd.substring( start, end );
    }
    else {   // TODO
      return cmd;
    }
  }

  private void parseHeadline() throws IOException {
    int level = 1;
    int ch = nextChar();
    while (ch == '#') {
      level++;
      ch = nextChar();
    }
    if (ch == ' ') {
      Model htmlElement;
      switch (level) {
        case 1: {
          Xhtml_h1_type h1 = new Xhtml_h1_type();
          htmlContainer.addH1( h1 );
          htmlElement = h1;
          break;
        }
        case 2: {
          Xhtml_h2_type h2 = new Xhtml_h2_type();
          htmlContainer.addH2( h2 );
          htmlElement = h2;
          break;
        }
        case 3: {
          Xhtml_h3_type h3 = new Xhtml_h3_type();
          htmlContainer.addH3( h3 );
          htmlElement = h3;
          break;
        }
        case 4: {
          Xhtml_h4_type h4 = new Xhtml_h4_type();
          htmlContainer.addH4( h4 );
          htmlElement = h4;
          break;
        }
        case 5: {
          Xhtml_h5_type h5 = new Xhtml_h5_type();
          htmlContainer.addH5( h5 );
          htmlElement = h5;
          break;
        }
        default: {
          Xhtml_h6_type h6 = new Xhtml_h6_type();
          htmlContainer.addH6( h6 );
          htmlElement = h6;
        }
      }
      parseText( htmlElement, "\n\r" );
    }
    else {
      throw new ParseException( "Expected space after headline marker, but found '"+(char) ch+"'" );
    }
  }

  private void parseBullet( Xhtml_ul_type ul ) throws IOException {
    while (true) {
      Xhtml_li_type li = new Xhtml_li_type();
      ul.addLi( li );
      parseTextLine( li );
      int ch = nextChar();
      if (ch == ' ') {
        parseTextLine( li );
      }
      else if (ch != '*') {
        back();
        return;
      }
    }
  }

  private void parseNumline( Xhtml_ol_type ol ) throws IOException {
    while (true) {
      Xhtml_li_type li = new Xhtml_li_type();
      ol.addLi( li );
      parseTextLine( li );
      int ch = nextChar();
      if (ch == ' ') {
        parseTextLine( li );
      }
      else if ((ch >= '0') && (ch <= '9')) {
        int ch2 = nextChar();
        if (ch2 == '.') {
          int ch3 = nextChar();
          if (ch3 != ' ') {
            back();
            back();
            back();
            return;
          }
        }
        else {
          back();
          back();
          return;
        }
      }
      else {
        back();
        return;
      }
    }
  }

  private void parseEmphasize( int ch2, Model htmlElement ) throws IOException {
    // already has parsed "**"
    boolean bold = false;
    boolean italic = false;
    if (ch2 == '*') {
      bold = true;
      int ch3 = nextChar();
      if (ch3 == '*') {
        italic = true;
      }
      else {
        italic = false;
      }
    }
    else {
      italic = true;
    }
    if (currentChar() == ' ') {
      bold = false;
      italic = false;
    }
    else {
      if (italic) {
        Xhtml_InlPres_type italicPres = new Xhtml_InlPres_type();
        htmlElement.add( Xhtml_div_type.ID_I, italicPres );
        htmlElement = italicPres;
      }
      if (bold) {
        Xhtml_InlPres_type boldPres = new Xhtml_InlPres_type();
        htmlElement.add( Xhtml_div_type.ID_B, boldPres );
        htmlElement = boldPres;
      }
      back();
      parseText( htmlElement, "*" );
      if (bold) {
        int endCh = nextChar();
        if ((endCh == '*') && italic) {
          endCh = nextChar();
        }
        if (endCh != '*') {
          back();
        }
      }
    }
  }

  private String parseURL() throws IOException {
    StringBuilder urlBuf = new StringBuilder();
    parseUntil( urlBuf, ")", true );
    String url = urlBuf.toString();
    if (url.startsWith( "{" )) {
      url = processCommand( url );
    }
    return url;
  }

  private void parseImage( Model htmlElement ) throws IOException {
    int ch2 = nextChar();
    if (ch2 == '[') {
      StringBuilder aliasBuf = new StringBuilder();
      parseUntil( aliasBuf, "]", true );
      int ch3 = skipWhitespace( true );
      if (ch3 == '(') {
        String urlStr = parseURL();
        Xhtml_img_type img = new Xhtml_img_type();
        img.setAlt( aliasBuf.toString() );
        img.setSrc( urlStr );
        htmlElement.add( Xhtml_div_type.ID_IMG, img );
      }
    }
  }

  private void parseHyperlink( Model htmlElement ) throws IOException {
    StringBuilder linkTextBuf = new StringBuilder();
    parseUntil( linkTextBuf, "]", true );
    int ch3 = skipWhitespace( true );
    if (ch3 == '(') {
      String urlStr = parseURL();
      Xhtml_a_type a = new Xhtml_a_type();
      a.setHref( urlStr );
      a.add( ComplexType.CDATA, new SimpleModel( StringType.getDefString(), linkTextBuf.toString() ) );
      htmlElement.add( Xhtml_div_type.ID_A, a );
    }
  }

  private void parseTableRow( Xhtml_table_type htmlTable ) throws IOException {
    Xhtml_tr_type tr = new Xhtml_tr_type();
    htmlTable.addTr( tr );
    int ch = '|';
    while (ch == '|') {
      ch = nextChar();
      if (ch == '-') {
        // Skip line below heading
        StringBuilder buf = new StringBuilder();
        parseToLineEnd( buf );
        // TODO convert first line to th
      }
      else if (ch == '\n') {
        return;
      }
      else {
        Xhtml_td_type td = new Xhtml_td_type();
        ch = parseText( td, "|\n" );
        if (ch == '|') {
          tr.addTd( td );
        }
      }
    }
  }

  private void parseTable() throws IOException {
    Xhtml_table_type table = new Xhtml_table_type();
    htmlContainer.addTable( table );
    int ch = '|';
    while (ch == '|') {
      parseTableRow( table );
      ch = skipWhitespace( true );
    }
    back();
  }

  private void parseHtml( Model htmlElement ) throws IOException {
    back();
    XmlReader xmlReader = new XmlReader();
    // read XML until end of sub structure using this.nextChar()
    try {
      MarkdownReader markdownReader = new MarkdownReader( this );
      xmlReader.includeData( htmlElement, markdownReader, null );
    }
    catch (IOException e) {
      throw new RuntimeException( e );
    }
  }

  private void parseParagraph() throws IOException {
    Xhtml_p_type paragraph = new Xhtml_p_type();
    htmlContainer.addP( paragraph );
    while (true) {
      int ch = parseTextLine( paragraph );
      if (ch == -1) {
        return;
      }
      ch = nextChar();
      if ((ch == -1) || (ch == '\n') || (ch == ' ')) {
        return;
      }
      else if (ch == '*') {
        back();
        return;
      }
      else {
        back();
      }
    }
  }

  private void parseSourceCode() throws IOException {
    Xhtml_pre_type pre = new Xhtml_pre_type();
    htmlContainer.addPre( pre );
    StringBuilder codeBuf = new StringBuilder();
    while (true) {
      int ch = nextChar();
      if (ch == '`') {
        int ch2 = nextChar();
        if (ch2 == '`') {
          int ch3 = nextChar();
          if (ch3 == '`') {
            pre.add( ComplexType.CDATA, new SimpleModel( StringType.getDefString(), codeBuf.toString() ) );
            return;
          }
          back();
        }
        back();
      }
      back();
      parseToLineEnd( codeBuf );
      codeBuf.append( "\n" );
    }
  }

  private void parseCommand( Model htmlElement ) throws IOException {
    // TODO Not yet implemented
    StringBuilder buf = new StringBuilder( "{");
    parseToLineEnd( buf );
    htmlElement.add( ComplexType.CDATA, new SimpleModel( StringType.getDefString(), buf.toString() ) );
  }
}
