/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.reqif.common;

import de.pidata.qnames.QName;
import de.pidata.reqif.api.AttributeValue;

import java.util.List;

/**
 * Requirement tool independent representation of an item within the requirement tool's model.
 * @apiNote IMPORTANT: should be moved to a more tool independent place
 */
public interface RequirementObject {

  QName getIdentifier();

  /**
   * Finds the given attribute by name.
   * Returns the string representation of the current value of that attribute.
   * Returns an empty string ("") if the attribute does not exist in the object's type
   * or the attribute value is not specified within the current object.
   *
   * @param attributeName
   * @return
   */
  String getAttributeStringValue( String attributeName );

  /**
   * Finds the given attribute by name within the current {@@link SPEC_OBJECT_TYPE_}.
   * Returns a list of strings from the values of that attribute.
   * Returns an empty list if the attribute does not exist in the object's type
   * or the attribute value is not specified within the current object.
   *
   * @param mimeType
   * @return
   */
  List<String> getAllObjectUris( String mimeType );

  /**
   * Finds the given attribute by name within the current {@@link SPEC_OBJECT_TYPE_}.
   * Returns a list of strings from the values of that attribute.
   * Returns an empty list if the attribute does not exist in the object's type
   * or the attribute value is not specified within the current object.
   *
   * @param attributeName
   * @return
   */
   List<String> getAttributeEnumValues( String attributeName );

  /**
   * Returns the string representation of the current value of the attribute identified by the given identifier.
   * Returns an empty string ("") if the attribute does not exist in the object's type
   * or the attribute value is not specified within the current object.
   *
   * @param attributeDef
   * @return
   */
  String getAttributeStringValue( RequirementAttribute attributeDef );

  /**
   * Returns the tool independent value representation of the value specified by the given name.
   *
   * @param attributeName
   * @return
   */
  AttributeValue getAttributeValue( String attributeName );

  /**
   * Returns the string representation of the current value of the attribute identified by the given identifier.
   * Returns an empty string ("") if the attribute does not exist in the object's type
   * or the attribute value is not specified within the current object.
   *
   * @param attributeDef
   * @return
   */
  AttributeValue getAttributeValue( RequirementAttribute attributeDef );

  /**
   * If the current object allows children: returns the list of the children. If recursive is true: descends into the children.
   *
   * @param recursive
   * @return
   */
  List<RequirementObject> getChildObjects( boolean recursive );

  /**
   * Returns the object which represents the direct parent object of this.
   *
   * @return
   */
  RequirementObject getParent();
}
