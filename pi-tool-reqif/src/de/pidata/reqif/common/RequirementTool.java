/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.reqif.common;

import de.pidata.qnames.QName;
import de.pidata.reqif.model.SPEC_RELATION;

import java.util.ArrayList;
import java.util.List;

/**
 * General access methods provided by a requirement tool such as DOORS, PREEvision.
 * @apiNote IMPORTANT: should be moved to a more tool independent place
 */
public interface RequirementTool {

  String getToolName();

  String getIdentifier();

  String getToolUserId();

  List<RequirementObject> getRootObjects();

  /**
   * Returns the relations a SPEC_OBJECT by its ID and a given relation type.
   *
   * @param specRelationType
   * @param sourceObjectRefId
   * @return
   */
  List<SPEC_RELATION> getSpecRelations( String specRelationType, QName sourceObjectRefId );

  /**
   * Returns the object representing a chapter which matches the given chapter name.
   *
   * @param chapterName
   * @return
   */
  RequirementObject getChapter( String chapterName ) throws Exception;

  /**
   * Returns a list of objects which match the given object type name.
   *
   * @param objectTypeName
   * @return
   */
  List<RequirementObject> findObjectsByType( String objectTypeName );

  /**
   * Returns the content of the requirement's attribute containing the object type.
   *
   * @param requirementObject
   * @return
   */
  String getObjectType( RequirementObject requirementObject );

  /**
   * Returns the content of the requirement's heading attribute.
   *
   * @param requirementObject
   * @return
   */
  String getObjectHeading( RequirementObject requirementObject );

  /**
   * Returns the content of the requirement's text attribute as String as provided.
   * For example HTML, RTF.
   *
   * @param requirementObject
   * @return
   */
  String getObjectText( RequirementObject requirementObject );

  /**
   * Returns the content of the requirement's text attribute
   * represented as a String containing HTML.
   *
   * @param requirementObject
   * @return
   */
  String getObjectTextHTML( RequirementObject requirementObject );

  /**
   * Returns the content of the requirement's text attribute as plain text.
   * All formatting information such as RTF, HTML are stripped.
   *
   * @param requirementObject
   * @return
   */
  String getObjectTextPlain( RequirementObject requirementObject );

  /**
   * Returns the content of the requirement's text attribute represented as a String containing HTML.
   *
   * @param requirementObject
   * @param attributeName
   * @return
   */
  String getAttributeHtmlValue( RequirementObject requirementObject, String attributeName );


  /**
   * Returns the content of the requirement's text attribute represented as a String containing HTML.
   *
   * @param requirementObject
   * @param attributeDef
   * @return
   */
  String getAttributeHtmlValue( RequirementObject requirementObject, RequirementAttribute attributeDef );
}
