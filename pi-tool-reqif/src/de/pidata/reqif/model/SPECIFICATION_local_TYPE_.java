// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.reqif.model;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.SimpleModel;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Hashtable;

public class SPECIFICATION_local_TYPE_ extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.omg.org/spec/ReqIF/20110401/reqif.xsd" );

  public static final QName ID_SPECIFICATION_TYPE_REF = NAMESPACE.getQName("SPECIFICATION-TYPE-REF");

  public SPECIFICATION_local_TYPE_() {
    super( null, ModelFactory.SPECIFICATION_LOCAL_TYPE_TYPE, null, null, null );
  }

  public SPECIFICATION_local_TYPE_(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ModelFactory.SPECIFICATION_LOCAL_TYPE_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected SPECIFICATION_local_TYPE_(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the element SPECIFICATION-TYPE-REF.
   *
   * @return The element SPECIFICATION-TYPE-REF
   */
  public QName getSPECIFICATION_TYPE_REF() {
    Model m = get( ID_SPECIFICATION_TYPE_REF, null );
    if (m == null) return null;
    else return (QName) m.getContent();
  }

  /**
   * Set the element SPECIFICATION-TYPE-REF.
   *
   * @param sPECIFICATION_TYPE_REF new value for element SPECIFICATION-TYPE-REF
   */
  public void setSPECIFICATION_TYPE_REF( QName sPECIFICATION_TYPE_REF ) {
    setChild( ID_SPECIFICATION_TYPE_REF, new SimpleModel( new QNameType( NAMESPACE.getQName("LOCAL-REF"), QNameType.getQName(), 0, Integer.MAX_VALUE ), sPECIFICATION_TYPE_REF ) );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
