// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.reqif.model.driver;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelCollection;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.xml.binder.Space;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Collection;
import java.util.Hashtable;

public class Xhtml_ul_type extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/1999/xhtml" );

  public static final QName ID_CLASS = NAMESPACE.getQName("class");
  public static final QName ID_ID = NAMESPACE.getQName("id");
  public static final QName ID_LANG = Namespace.getInstance( "http://www.w3.org/XML/1998/namespace" ).getQName("lang");
  public static final QName ID_LI = NAMESPACE.getQName("li");
  public static final QName ID_SPACE = Namespace.getInstance( "http://www.w3.org/XML/1998/namespace" ).getQName("space");
  public static final QName ID_STYLE = NAMESPACE.getQName("style");
  public static final QName ID_TITLE = NAMESPACE.getQName("title");
  public static final QName ID_TYPE = NAMESPACE.getQName("type");

  private Collection<Xhtml_li_type> lis = new ModelCollection<Xhtml_li_type>( ID_LI, children );

  public Xhtml_ul_type() {
    super( null, ModelFactory.XHTML_UL_TYPE_TYPE, null, null, null );
  }

  public Xhtml_ul_type(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ModelFactory.XHTML_UL_TYPE_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected Xhtml_ul_type(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute space.
   *
   * @return The attribute space
   */
  public Space getSpace() {
    return (Space) get( ID_SPACE );
  }

  /**
   * Set the attribute space.
   *
   * @param space new value for attribute space
   */
  public void setSpace( Space space ) {
    set( ID_SPACE, space );
  }

  /**
   * Returns the attribute id.
   *
   * @return The attribute id
   */
  public QName getId() {
    return (QName) get( ID_ID );
  }

  /**
   * Set the attribute id.
   *
   * @param id new value for attribute id
   */
  public void setId( QName id ) {
    set( ID_ID, id );
  }

  /**
   * Returns the attribute class.
   *
   * @return The attribute class
   */
  public String getClass_() {
    return (String) get( ID_CLASS );
  }

  /**
   * Set the attribute class.
   *
   * @param _class new value for attribute class
   */
  public void setClass( String _class ) {
    set( ID_CLASS, _class );
  }

  /**
   * Returns the attribute title.
   *
   * @return The attribute title
   */
  public String getTitle() {
    return (String) get( ID_TITLE );
  }

  /**
   * Set the attribute title.
   *
   * @param title new value for attribute title
   */
  public void setTitle( String title ) {
    set( ID_TITLE, title );
  }

  /**
   * Returns the attribute lang.
   *
   * @return The attribute lang
   */
  public String getLang() {
    return (String) get( ID_LANG );
  }

  /**
   * Set the attribute lang.
   *
   * @param lang new value for attribute lang
   */
  public void setLang( String lang ) {
    set( ID_LANG, lang );
  }

  /**
   * Returns the attribute style.
   *
   * @return The attribute style
   */
  public String getStyle() {
    return (String) get( ID_STYLE );
  }

  /**
   * Set the attribute style.
   *
   * @param style new value for attribute style
   */
  public void setStyle( String style ) {
    set( ID_STYLE, style );
  }

  /**
   * Returns the attribute type.
   *
   * @return The attribute type
   */
  public String getType() {
    return (String) get( ID_TYPE );
  }

  /**
   * Set the attribute type.
   *
   * @param type new value for attribute type
   */
  public void setType( String type ) {
    set( ID_TYPE, type );
  }

  /**
   * Returns the li element identified by the given key.
   *
   * @return the li element identified by the given key
   */
  public Xhtml_li_type getLi( Key liID ) {
    return (Xhtml_li_type) get( ID_LI, liID );
  }

  /**
   * Adds the li element.
   *
   * @param li the li element to add
   */
  public void addLi( Xhtml_li_type li ) {
    add( ID_LI, li );
  }

  /**
   * Removes the li element.
   *
   * @param li the li element to remove
   */
  public void removeLi( Xhtml_li_type li ) {
    remove( ID_LI, li );
  }

  /**
   * Returns the li iterator.
   *
   * @return the li iterator
   */
  public ModelIterator<Xhtml_li_type> liIter() {
    return iterator( ID_LI, null );
  }

  /**
   * Returns the number of lis.
   *
   * @return the number of lis
   */
  public int liCount() {
    return childCount( ID_LI );
  }

  /**
   * Returns the li collection.
   *
   * @return the li collection
   */
  public Collection<Xhtml_li_type> getLis() {
    return lis;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
