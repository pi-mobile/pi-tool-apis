// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.reqif.model;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Hashtable;

public class ATTRIBUTE_DEFINITION_STRING_local_DEFAULT_VALUE extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.omg.org/spec/ReqIF/20110401/reqif.xsd" );

  public static final QName ID_ATTRIBUTE_VALUE_STRING = NAMESPACE.getQName("ATTRIBUTE-VALUE-STRING");

  public ATTRIBUTE_DEFINITION_STRING_local_DEFAULT_VALUE() {
    super( null, ModelFactory.ATTRIBUTE_DEFINITION_STRING_LOCAL_DEFAULT_VALUE_TYPE, null, null, null );
  }

  public ATTRIBUTE_DEFINITION_STRING_local_DEFAULT_VALUE(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ModelFactory.ATTRIBUTE_DEFINITION_STRING_LOCAL_DEFAULT_VALUE_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected ATTRIBUTE_DEFINITION_STRING_local_DEFAULT_VALUE(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the element ATTRIBUTE-VALUE-STRING.
   *
   * @return The element ATTRIBUTE-VALUE-STRING
   */
  public ATTRIBUTE_VALUE_STRING getATTRIBUTE_VALUE_STRING() {
    return (ATTRIBUTE_VALUE_STRING) get( ID_ATTRIBUTE_VALUE_STRING, null );
  }

  /**
   * Set the element ATTRIBUTE-VALUE-STRING.
   *
   * @param aTTRIBUTE_VALUE_STRING new value for element ATTRIBUTE-VALUE-STRING
   */
  public void setATTRIBUTE_VALUE_STRING( ATTRIBUTE_VALUE_STRING aTTRIBUTE_VALUE_STRING ) {
    setChild( ID_ATTRIBUTE_VALUE_STRING, aTTRIBUTE_VALUE_STRING );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
