// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.reqif.model;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.simple.DateObject;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.reqif.api.AttributeDefinition;
import java.lang.Boolean;
import java.lang.String;
import java.util.Hashtable;

public class ATTRIBUTE_DEFINITION_INTEGER extends de.pidata.models.tree.SequenceModel implements AttributeDefinition {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.omg.org/spec/ReqIF/20110401/reqif.xsd" );

  public static final QName ID_ALTERNATIVE_ID = NAMESPACE.getQName("ALTERNATIVE-ID");
  public static final QName ID_DEFAULT_VALUE = NAMESPACE.getQName("DEFAULT-VALUE");
  public static final QName ID_DESC = NAMESPACE.getQName("DESC");
  public static final QName ID_IDENTIFIER = NAMESPACE.getQName("IDENTIFIER");
  public static final QName ID_IS_EDITABLE = NAMESPACE.getQName("IS-EDITABLE");
  public static final QName ID_LAST_CHANGE = NAMESPACE.getQName("LAST-CHANGE");
  public static final QName ID_LONG_NAME = NAMESPACE.getQName("LONG-NAME");
  public static final QName ID_TYPE = NAMESPACE.getQName("TYPE");

  public ATTRIBUTE_DEFINITION_INTEGER() {
    super( null, ModelFactory.ATTRIBUTE_DEFINITION_INTEGER_TYPE, null, null, null );
  }

  public ATTRIBUTE_DEFINITION_INTEGER(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ModelFactory.ATTRIBUTE_DEFINITION_INTEGER_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected ATTRIBUTE_DEFINITION_INTEGER(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute DESC.
   *
   * @return The attribute DESC
   */
  public String getDESC() {
    return (String) get( ID_DESC );
  }

  /**
   * Set the attribute DESC.
   *
   * @param dESC new value for attribute DESC
   */
  public void setDESC( String dESC ) {
    set( ID_DESC, dESC );
  }

  /**
   * Returns the attribute IDENTIFIER.
   *
   * @return The attribute IDENTIFIER
   */
  public QName getIDENTIFIER() {
    return (QName) get( ID_IDENTIFIER );
  }

  /**
   * Set the attribute IDENTIFIER.
   *
   * @param iDENTIFIER new value for attribute IDENTIFIER
   */
  public void setIDENTIFIER( QName iDENTIFIER ) {
    set( ID_IDENTIFIER, iDENTIFIER );
  }

  /**
   * Returns the attribute IS-EDITABLE.
   *
   * @return The attribute IS-EDITABLE
   */
  public Boolean getIS_EDITABLE() {
    return (Boolean) get( ID_IS_EDITABLE );
  }

  /**
   * Set the attribute IS-EDITABLE.
   *
   * @param iS_EDITABLE new value for attribute IS-EDITABLE
   */
  public void setIS_EDITABLE( Boolean iS_EDITABLE ) {
    set( ID_IS_EDITABLE, iS_EDITABLE );
  }

  /**
   * Returns the attribute LAST-CHANGE.
   *
   * @return The attribute LAST-CHANGE
   */
  public DateObject getLAST_CHANGE() {
    return (DateObject) get( ID_LAST_CHANGE );
  }

  /**
   * Set the attribute LAST-CHANGE.
   *
   * @param lAST_CHANGE new value for attribute LAST-CHANGE
   */
  public void setLAST_CHANGE( DateObject lAST_CHANGE ) {
    set( ID_LAST_CHANGE, lAST_CHANGE );
  }

  /**
   * Returns the attribute LONG-NAME.
   *
   * @return The attribute LONG-NAME
   */
  public String getLONG_NAME() {
    return (String) get( ID_LONG_NAME );
  }

  /**
   * Set the attribute LONG-NAME.
   *
   * @param lONG_NAME new value for attribute LONG-NAME
   */
  public void setLONG_NAME( String lONG_NAME ) {
    set( ID_LONG_NAME, lONG_NAME );
  }

  /**
   * Returns the element ALTERNATIVE-ID.
   *
   * @return The element ALTERNATIVE-ID
   */
  public ATTRIBUTE_DEFINITION_INTEGER_local_ALTERNATIVE_ID getALTERNATIVE_ID() {
    return (ATTRIBUTE_DEFINITION_INTEGER_local_ALTERNATIVE_ID) get( ID_ALTERNATIVE_ID, null );
  }

  /**
   * Set the element ALTERNATIVE-ID.
   *
   * @param aLTERNATIVE_ID new value for element ALTERNATIVE-ID
   */
  public void setALTERNATIVE_ID( ATTRIBUTE_DEFINITION_INTEGER_local_ALTERNATIVE_ID aLTERNATIVE_ID ) {
    setChild( ID_ALTERNATIVE_ID, aLTERNATIVE_ID );
  }

  /**
   * Returns the element DEFAULT-VALUE.
   *
   * @return The element DEFAULT-VALUE
   */
  public ATTRIBUTE_DEFINITION_INTEGER_local_DEFAULT_VALUE getDEFAULT_VALUE() {
    return (ATTRIBUTE_DEFINITION_INTEGER_local_DEFAULT_VALUE) get( ID_DEFAULT_VALUE, null );
  }

  /**
   * Set the element DEFAULT-VALUE.
   *
   * @param dEFAULT_VALUE new value for element DEFAULT-VALUE
   */
  public void setDEFAULT_VALUE( ATTRIBUTE_DEFINITION_INTEGER_local_DEFAULT_VALUE dEFAULT_VALUE ) {
    setChild( ID_DEFAULT_VALUE, dEFAULT_VALUE );
  }

  /**
   * Returns the element TYPE.
   *
   * @return The element TYPE
   */
  public ATTRIBUTE_DEFINITION_INTEGER_local_TYPE_ getTYPE() {
    return (ATTRIBUTE_DEFINITION_INTEGER_local_TYPE_) get( ID_TYPE, null );
  }

  /**
   * Set the element TYPE.
   *
   * @param tYPE new value for element TYPE
   */
  public void setTYPE( ATTRIBUTE_DEFINITION_INTEGER_local_TYPE_ tYPE ) {
    setChild( ID_TYPE, tYPE );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  /**
   * Returns the identifier of the referenced attribute datatype regardless of the actual attribute type.
   *
   * @return
   */
  @Override
  public QName getDatatypeDefinitionRef() {
    return getTYPE().getDATATYPE_DEFINITION_INTEGER_REF();
  }

  @Override
  public String toString() {
    String long_name = getLONG_NAME();
    if (long_name != null) {
      return long_name;
    }
    return super.toString();
  }
}
