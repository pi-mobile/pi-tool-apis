// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.reqif.model;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.SimpleModel;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Hashtable;

public class ATTRIBUTE_VALUE_XHTML_local_DEFINITION extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.omg.org/spec/ReqIF/20110401/reqif.xsd" );

  public static final QName ID_ATTRIBUTE_DEFINITION_XHTML_REF = NAMESPACE.getQName("ATTRIBUTE-DEFINITION-XHTML-REF");

  public ATTRIBUTE_VALUE_XHTML_local_DEFINITION() {
    super( null, ModelFactory.ATTRIBUTE_VALUE_XHTML_LOCAL_DEFINITION_TYPE, null, null, null );
  }

  public ATTRIBUTE_VALUE_XHTML_local_DEFINITION(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ModelFactory.ATTRIBUTE_VALUE_XHTML_LOCAL_DEFINITION_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected ATTRIBUTE_VALUE_XHTML_local_DEFINITION(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the element ATTRIBUTE-DEFINITION-XHTML-REF.
   *
   * @return The element ATTRIBUTE-DEFINITION-XHTML-REF
   */
  public QName getATTRIBUTE_DEFINITION_XHTML_REF() {
    Model m = get( ID_ATTRIBUTE_DEFINITION_XHTML_REF, null );
    if (m == null) return null;
    else return (QName) m.getContent();
  }

  /**
   * Set the element ATTRIBUTE-DEFINITION-XHTML-REF.
   *
   * @param aTTRIBUTE_DEFINITION_XHTML_REF new value for element ATTRIBUTE-DEFINITION-XHTML-REF
   */
  public void setATTRIBUTE_DEFINITION_XHTML_REF( QName aTTRIBUTE_DEFINITION_XHTML_REF ) {
    setChild( ID_ATTRIBUTE_DEFINITION_XHTML_REF, new SimpleModel( new QNameType( NAMESPACE.getQName("LOCAL-REF"), QNameType.getQName(), 0, Integer.MAX_VALUE ), aTTRIBUTE_DEFINITION_XHTML_REF ) );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
