// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.reqif.model;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.simple.DateObject;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.reqif.api.ReqIFHelper;
import java.lang.String;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

public class SPECIFICATION extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.omg.org/spec/ReqIF/20110401/reqif.xsd" );

  public static final QName ID_ALTERNATIVE_ID = NAMESPACE.getQName("ALTERNATIVE-ID");
  public static final QName ID_CHILDREN = NAMESPACE.getQName("CHILDREN");
  public static final QName ID_DESC = NAMESPACE.getQName("DESC");
  public static final QName ID_IDENTIFIER = NAMESPACE.getQName("IDENTIFIER");
  public static final QName ID_LAST_CHANGE = NAMESPACE.getQName("LAST-CHANGE");
  public static final QName ID_LONG_NAME = NAMESPACE.getQName("LONG-NAME");
  public static final QName ID_TYPE = NAMESPACE.getQName("TYPE");
  public static final QName ID_VALUES = NAMESPACE.getQName("VALUES");

  public SPECIFICATION() {
    super( null, ModelFactory.SPECIFICATION_TYPE, null, null, null );
  }

  public SPECIFICATION(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, ModelFactory.SPECIFICATION_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected SPECIFICATION(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute DESC.
   *
   * @return The attribute DESC
   */
  public String getDESC() {
    return (String) get( ID_DESC );
  }

  /**
   * Set the attribute DESC.
   *
   * @param dESC new value for attribute DESC
   */
  public void setDESC( String dESC ) {
    set( ID_DESC, dESC );
  }

  /**
   * Returns the attribute IDENTIFIER.
   *
   * @return The attribute IDENTIFIER
   */
  public QName getIDENTIFIER() {
    return (QName) get( ID_IDENTIFIER );
  }

  /**
   * Set the attribute IDENTIFIER.
   *
   * @param iDENTIFIER new value for attribute IDENTIFIER
   */
  public void setIDENTIFIER( QName iDENTIFIER ) {
    set( ID_IDENTIFIER, iDENTIFIER );
  }

  /**
   * Returns the attribute LAST-CHANGE.
   *
   * @return The attribute LAST-CHANGE
   */
  public DateObject getLAST_CHANGE() {
    return (DateObject) get( ID_LAST_CHANGE );
  }

  /**
   * Set the attribute LAST-CHANGE.
   *
   * @param lAST_CHANGE new value for attribute LAST-CHANGE
   */
  public void setLAST_CHANGE( DateObject lAST_CHANGE ) {
    set( ID_LAST_CHANGE, lAST_CHANGE );
  }

  /**
   * Returns the attribute LONG-NAME.
   *
   * @return The attribute LONG-NAME
   */
  public String getLONG_NAME() {
    return (String) get( ID_LONG_NAME );
  }

  /**
   * Set the attribute LONG-NAME.
   *
   * @param lONG_NAME new value for attribute LONG-NAME
   */
  public void setLONG_NAME( String lONG_NAME ) {
    set( ID_LONG_NAME, lONG_NAME );
  }

  /**
   * Returns the element ALTERNATIVE-ID.
   *
   * @return The element ALTERNATIVE-ID
   */
  public SPECIFICATION_local_ALTERNATIVE_ID getALTERNATIVE_ID() {
    return (SPECIFICATION_local_ALTERNATIVE_ID) get( ID_ALTERNATIVE_ID, null );
  }

  /**
   * Set the element ALTERNATIVE-ID.
   *
   * @param aLTERNATIVE_ID new value for element ALTERNATIVE-ID
   */
  public void setALTERNATIVE_ID( SPECIFICATION_local_ALTERNATIVE_ID aLTERNATIVE_ID ) {
    setChild( ID_ALTERNATIVE_ID, aLTERNATIVE_ID );
  }

  /**
   * Returns the element VALUES.
   *
   * @return The element VALUES
   */
  public SPECIFICATION_local_VALUES getVALUES() {
    return (SPECIFICATION_local_VALUES) get( ID_VALUES, null );
  }

  /**
   * Set the element VALUES.
   *
   * @param vALUES new value for element VALUES
   */
  public void setVALUES( SPECIFICATION_local_VALUES vALUES ) {
    setChild( ID_VALUES, vALUES );
  }

  /**
   * Returns the element CHILDREN.
   *
   * @return The element CHILDREN
   */
  public SPECIFICATION_local_CHILDREN getCHILDREN() {
    return (SPECIFICATION_local_CHILDREN) get( ID_CHILDREN, null );
  }

  /**
   * Set the element CHILDREN.
   *
   * @param cHILDREN new value for element CHILDREN
   */
  public void setCHILDREN( SPECIFICATION_local_CHILDREN cHILDREN ) {
    setChild( ID_CHILDREN, cHILDREN );
  }

  /**
   * Returns the element TYPE.
   *
   * @return The element TYPE
   */
  public SPECIFICATION_local_TYPE_ getTYPE() {
    return (SPECIFICATION_local_TYPE_) get( ID_TYPE, null );
  }

  /**
   * Set the element TYPE.
   *
   * @param tYPE new value for element TYPE
   */
  public void setTYPE( SPECIFICATION_local_TYPE_ tYPE ) {
    setChild( ID_TYPE, tYPE );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  /**
   * Descends into the specification hierarchy to find a node which references the {@link SPEC_OBJECT} identified by the given specObjectIdentifier.
   * Returns the found {@link SPEC_HIERARCHY} node.
   *
   * @param specObjectIdentifier
   * @return
   */
  public SPEC_HIERARCHY getSpecHierarchy( QName specObjectIdentifier ) {
    SPEC_HIERARCHY result = null;
    SPECIFICATION_local_CHILDREN children = getCHILDREN();
    Collection<SPEC_HIERARCHY> childHierarchies = children.getSPEC_HIERARCHYs();
    for (SPEC_HIERARCHY hierarchy : childHierarchies) {
      result = hierarchy.findHierarchy( specObjectIdentifier );
      if (result != null) {
        break;
      }
    }
    return result;
  }

  /**
   * Descends into the specification hierarchy starting with the hierarchy node which references the given {@link SPEC_OBJECT}
   * and collects all {@link SPEC_OBJECT}s referenced by the {@link SPEC_HIERARCHY} nodes.
   *
   * @param specObject
   * @param recursive
   * @return
   */
  public List<SPEC_OBJECT> getChildSpecObjects( SPEC_OBJECT specObject, boolean recursive ) {
    SPEC_HIERARCHY specHierarchy = getSpecHierarchy( specObject.getIDENTIFIER() );
    return getChildSpecObjects( specHierarchy, recursive );
  }

  /**
   * Descends into the specification hierarchy starting with the given parent hierarchy node
   * }and collects all {@link SPEC_OBJECT}s referenced by the {@link SPEC_HIERARCHY} nodes.
   *
   * @param parentHierarchy
   * @param recursive
   * @return
   */
  public List<SPEC_OBJECT> getChildSpecObjects( SPEC_HIERARCHY parentHierarchy, boolean recursive ) {
    List<SPEC_OBJECT> result = new ArrayList<>();
    REQ_IF reqIF = ReqIFHelper.getReqIF( this );
    SPEC_HIERARCHY_local_CHILDREN children = parentHierarchy.getCHILDREN();
    Collection<SPEC_HIERARCHY> childHierarchies = children.getSPEC_HIERARCHYs();
    for (SPEC_HIERARCHY child : childHierarchies) {
      SPEC_HIERARCHY_local_OBJECT object = child.getOBJECT();
      QName spec_object_ref = object.getSPEC_OBJECT_REF();
      result.add( reqIF.getSpecObject( spec_object_ref ) );
      if (recursive) {
        result.addAll( getChildSpecObjects( child, recursive ) );
      }
    }
    return result;
  }

  @Override
  public String toString() {
    String long_name = getLONG_NAME();
    if (long_name != null) {
      return long_name;
    }
    return super.toString();
  }
}
