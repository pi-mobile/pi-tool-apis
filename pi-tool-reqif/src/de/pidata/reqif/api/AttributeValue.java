/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.reqif.api;

import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides access to common features of attribute values regardless of the actual attribute value type.
 * Extends {@link Model} to be able to use e.g. {@link de.pidata.models.tree.ModelIterator}.
 */
public interface AttributeValue extends Model {

  Object getTHE_VALUE();

  /**
   * Returns the identifier of the referenced attribute definition regardless of the actual attribute type.
   *
   * @return
   */
  QName getAttributeDefinitionRef();

  /**
   * Returns the string representation of the value of the attribute within the current {@link de.pidata.reqif.model.SPEC_OBJECT}.
   * Returns an empty string ("") if the attribute value is not specified.
   *
   * @return
   */
  String getStringValue();

  /**
   * Returns the URIs as string for the MIME-Type image/png of the value of the attribute within the current {@link de.pidata.reqif.model.SPEC_OBJECT}.
   * Returns an empty List if the attribute value is not specified.
   *
   * @param mimeType the MIME type of the objects to search
   * @return
   */
  default List<String> getObjectUris( String mimeType ) {
    return new ArrayList<>();
  }
}
