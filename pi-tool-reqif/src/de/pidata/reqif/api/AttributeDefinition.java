/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.reqif.api;

import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.reqif.common.RequirementAttribute;

/**
 * Provides access to common features of attribute definition regardless of the actual attribute definition.
 * Extend {@link Model} to be able to use e.g. {@link de.pidata.models.tree.ModelIterator}.
 */
public interface AttributeDefinition extends Model, RequirementAttribute {

  QName getIDENTIFIER();

  String getLONG_NAME();

  /**
   * Returns the identifier of the referenced attribute datatype regardless of the actual attribute type.
   *
   * @return
   */
  QName getDatatypeDefinitionRef();

  @Override
  default QName getIdentifier() {
    return getIDENTIFIER();
  }
}
