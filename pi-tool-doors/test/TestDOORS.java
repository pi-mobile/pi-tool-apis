/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import de.pidata.doors.api.Doors;
import de.pidata.doors.api.DoorsExplorer;
import de.pidata.doors.api.DoorsModule;
import de.pidata.log.Logger;
import de.pidata.system.desktop.DesktopSystem;

public class TestDOORS {

  public static void main( String[] args) {
    try {
      new DesktopSystem();
      Doors doors = new Doors();
      DoorsModule module = new DoorsModule( doors, "/Scratch/Test-PI-Data", false );

      String result = module.readDOORSAttribute( DoorsModule.DOORSATTR_OBJECT_TEXT );
      Logger.info( "Object Text: " + result );

      module.loadBaseline( "0.1", true );
      result = module.readDOORSAttribute( DoorsModule.DOORSATTR_OBJECT_TEXT );
      Logger.info( "Object Text: " + result );

      DoorsExplorer doorsExplorer = new DoorsExplorer( doors );
      //result = doorsExplorer.currentModule();
      //Logger.info( "Current Module: " + result );

      int i = doorsExplorer.getCountOpenVisibleModules();
      Logger.info( "Open visible Modules: " + i );

      String modList = doorsExplorer.listModules();
      Logger.info( modList );
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
