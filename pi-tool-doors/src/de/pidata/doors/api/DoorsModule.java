/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.doors.api;

import de.pidata.doors.models.MapTag;
import de.pidata.doors.models.Mappings;
import de.pidata.log.Logger;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.string.Helper;

import java.util.*;
import java.util.stream.Collectors;

public class DoorsModule {

  public static final String DOORSATTR_OBJECT_TEXT = "Object Text";
  public static final String DOORSATTR_OBJECT_HEADING = "Object Heading";

  public static final String READ_ONLY_OR_BASELINE_EXCEPTION = "Module has to be opened in edit mode with no baseline loaded";
  public static final String SPLIT_STRING = "@PI-Split@";

  private String requirementTypeAttribute;

  private enum ImportantAttributes {
    EALINK("EALink"),
    SOURCEID("SourceID"),
    COMMENT("comment"),
    OBJECT_TYPE("Object Type"),
    FO_OBJECT_TYPE("FO_Object_Type");

    private String value;

    ImportantAttributes(String value) {
      this.value = value;
    }

    public String getValue() {
      return this.value;
    }

    public String toString() {
      return value;
    }
  }

  private Doors objDOORS;
  private String fullModulePath;
  private String commonDOORSAttributes = "";
  private boolean asilAttributeExists;
  private boolean eaLinkAttributeExists;
  private boolean isBaseline;
  private boolean isFiltering;
  private String baseLineString = "";
  private String baseLineMajor = "0";
  private String baseLineMinor = "0";
  private String baseLineSuffix = "";
  private boolean isInEditMode;

  private HashMap<String, List<Integer>> cachedSourceIds;
  private String valuesOfObjectType;

  public Doors getDoors() {
    return objDOORS;
  }

  public String getFullModulePath() {
    return fullModulePath;
  }

  public String getCommonDoorsAttributes() {
    return commonDOORSAttributes;
  }

  public void setCommonDOORSAttributes( String commonDOORSAttributes ) {
    this.commonDOORSAttributes = commonDOORSAttributes;
  }

  public boolean asilAttributeExists() {
    return asilAttributeExists;
  }

  public boolean eaLinkAttributeExists() {
    return eaLinkAttributeExists;
  }

  public boolean isBaseline() {
    return isBaseline;
  }

  public boolean isFiltering() {
    return isFiltering;
  }

  public DoorsModule( Doors objDOORS, String fullModulePath, boolean edit, boolean checkImportantAttributes ) {
    Logger.info("Created new DoorsModule: " + fullModulePath);
    this.isInEditMode = edit;
    DoorsExplorer doorsExplorer = objDOORS.getDoorsExplorer();
    this.objDOORS = objDOORS;
    this.fullModulePath = fullModulePath;
    this.eaLinkAttributeExists = existsAttribute( "EALink" );
    this.asilAttributeExists = existsAttribute( "ASIL" );
    if ( edit ) {
      if ( fullModulePath != null ) {
        if (doorsExplorer.isModuleOpenEdit( fullModulePath )) {
          this.isBaseline = doorsExplorer.firstCurrentEditModuleIsBaseline();
        }
        else{
          this.isBaseline = false;
        }
        this.isFiltering = filtering();
        if ( checkImportantAttributes ) {
          checkImportantAttributes();
        }
      }
    }
    else {
      if ( fullModulePath != null ) {
        if(doorsExplorer.isModuleOpenVisible( fullModulePath )) {
          this.isBaseline = doorsExplorer.firstCurrentVisibleModuleIsBaseline();
        }
        else{
          this.isBaseline = false;
        }
        this.isFiltering = filtering();
      }
    }
  }

  public DoorsModule( Doors objDOORS, String fullModulePath, boolean edit ) {
    this( objDOORS, fullModulePath, edit, true );
  }

  public DoorsModule( Doors objDOORS, String fullModulePath ) {
    this( objDOORS, fullModulePath, true, true );
  }

  public static String replaceIllegalCharsOld( String str ) {
    str = str.replace( "\"", "\'" );
    str = str.replace( "\\", "\\\\" );
    return str;
  }

  /**
   * set doors attribute that will be used for getting the type of a requirement in this module
   *
   * @param attributeName name of the attribute in current doors module
   * @throws IllegalArgumentException if no attribute exists with given attribute name in current doors module
   */
  public void setRequirementTypeAttribute( String attributeName ) {
    if (!Helper.isNullOrEmpty( attributeName )) {
      if (this.existsAttribute( attributeName )) {
        if (this.isEnumType( attributeName )) {
          this.requirementTypeAttribute = attributeName;
        }
        else {
          throw new IllegalArgumentException( "Setting requirementTypeAttribute failed: Attribute '" + attributeName + "' is not defined as enum type in doors module '" + fullModulePath + "'" );
        }
      }
      else {
        throw new IllegalArgumentException( "Setting requirementTypeAttribute failed: Attribute '" + attributeName + "' does not exist in doors module '" + fullModulePath + "'" );
      }
    }
  }

  /**
   * get doors attribute that will be used for getting the type of a requirement in this module
   *
   * @return name of the attribute
   */
  public String getRequirementTypeAttribute() {
    return this.requirementTypeAttribute;
  }

  /**
   * returns the value of the defined requirementTypeAttribute for element referenced by given absNum
   * if requirementTypeAttribute is not defined, null will be returned
   *
   * @param absNum reference to doors object
   * @return value of requirementTypeAttribute
   */
  public String getRequirementType( int absNum ) {
    if (Helper.isNullOrEmpty( requirementTypeAttribute )) {
      return null;
    }
    else {
      return getAttributeFromDOORS( absNum, requirementTypeAttribute );
    }
  }

  public void setRequirementType (int absNum, String value){
    if (!Helper.isNullOrEmpty( requirementTypeAttribute )) {
      setAttributeInDOORS( absNum, requirementTypeAttribute, value );
    }
  }

  public void setEditMode( boolean inEditMode ) {
    isInEditMode = inEditMode;
  }

  /**
   * Wir wollen verhindern, dass der generierte DXL-Code kaputt geht und der Interpreter einen Fehler wirft.
   * Problematische Zeichen: '"', '\'
   * "\" wird durch "\\" ersetzt.
   * '"' wird durch "'" ersetzt. '\"' würde auch gehen.
   * Die Reihenfolge spielt eine Rolle.
   * Wenn wir zuerst '"' in '\"' wandeln und dann "\" in "\\", erhalten wir '\\"' und einen DXL-Fehler.
   * Daher zuerst Backslash wandeln und dann Anführungszeichen.
   *
   * Ein Zeilenumbruch ist erstaunlich unproblematisch:
   * Bei RTF wird ein "\par" daraus.
   * Im DXL ist folgendes kein Problem:
   * string text = "1234
   * 5678";
   * Der Zeilenumbruch wird dann ins DOORS übertragen.
   *
   * @param str: Ein String, der nach DOORS übertragen werden soll.
   * @return String, der im DXL keine Fehler erzeugt.
   */
  public static String escapeIllegalChars(String str ) {
    if (str == null) {
      return "";
    }
    if ( str.contains( "\\" ) ) {
      str = str.replace("\\", "\\\\");
    }
    if ( str.contains( "\"" ) ) {
      str = str.replace("\"", "\\\"");
    }
    return str;
  }

  protected void createImportantAttributes() {
    String result = createAttributeTypeT_ObjectType();
    result = createAttribute( "object","Object Type", "t_Object Type", "", "" );
    result = createAttributeTypeFO_Object_Type();
    result = createAttribute( "object","FO_Object_Type", "FO_Object_Type", "", "" );
    result = createAttributeTypeT_ASIL();
    result = createAttribute( "object","ASIL", "T_ASIL", "", "" );
    result = createAttribute( "object","SourceID", "String", "new", "" );
    result = createAttribute( "object","EALink", "String", "", "UID imported from enterprise architect" );
    result = createAttribute( "object","comment", "Text", "", "" );
  }

  StringBuilder errorMessages = new StringBuilder(  );

  private void checkImportantAttributes() {
    errorMessages.setLength(0);
    for ( ImportantAttributes importantAttribute: ImportantAttributes.values() ) {
      if ( !existsAttribute( importantAttribute.getValue() ) ) {
        errorMessages.append( "Important attribute '" );
        errorMessages.append( importantAttribute.toString() );
        errorMessages.append( "' missing.\n" );
      }
    }
    if ( !errorMessages.toString().equals( "" ) ) {
      throw new IllegalArgumentException( errorMessages.toString() );
    }
  }

  private void useBaseline( String baseLineString ) {
      this.baseLineString = baseLineString;
      this.baseLineMajor = getMajorFromBaselineString( baseLineString );
      this.baseLineMinor = getMinorFromBaselineString( baseLineString );
      this.baseLineSuffix = getSuffixFromBaselineString( baseLineString );
  }

  public void useCurrent() {
      this.baseLineString = "";
      this.baseLineMajor = "0";
      this.baseLineMinor = "0";
      this.baseLineSuffix = "";
  }

  public boolean filtering() {
    String dxl;
    dxl = readModule()
        + "if ( filtering( m ) ) {\r\n"
        + "  oleSetResult( \"true\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"false\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }


  private String readCurrentModule() {
    return "Module m = read(\"" + fullModulePath + "\", false);\r\n";
  }

  private String readModule() {

    if(Helper.isNullOrEmpty(this.baseLineString)){
      return "Module m = read(\"" + fullModulePath + "\", false);\r\n";
    }
    else {

      return "Module readModule (){\r\n"
              + "  Module m;\r\n"
              + "  for m in database do{\r\n"
              + "    if ( fullName (m) == \"" + fullModulePath + "\"){\r\n"
              + "      Baseline b = baselineInfo(m);\r\n"
              + "      string s_baseline = \"\";\r\n"
              + "      if( b != null){\r\n"
              + "        int i_major = major ( b );\r\n"
              + "        int i_minor = minor ( b );\r\n"
              + "        string s_suffix = suffix( b )\r\n"
              + "        s_baseline = i_major \".\" i_minor \"\" s_suffix;\r\n"
              + "      }\r\n"
              + "      if( \"" + this.baseLineString + "\" == s_baseline){\r\n"
              + "        current = m;\r\n"
              + "        return m;\r\n"
              + "      }\r\n"
              + "    }\r\n"
              + "  }\r\n"
              + "  m = read(\"" + fullModulePath + "\", false);\r\n"
              + "  Baseline b = baseline( " + this.baseLineMajor + ", " + this.baseLineMinor + ", \"" + this.baseLineSuffix + "\" );\r\n"
              + "  return load(m, b, true);\r\n"
              + "}\r\n"
              + "\r\n"
              + "Module m = readModule;\r\n";
    }
  }

  //setting current is expensive
  //TODO: try to avoid setting current when reading a module (don't confuse current ->moduleRef<- with current m ->objectRef<-)
  private String readModule (boolean setCurrent){
    if(setCurrent){
      return readModule();
    }
    else{
      if(Helper.isNullOrEmpty(this.baseLineString)){
        return "Module m = read(\"" + fullModulePath + "\", false);\r\n";
      }
      else {

        return "Module readModule (){\r\n"
                + "  Module m;\r\n"
                + "  for m in database do{\r\n"
                + "    if ( fullName (m) == \"" + fullModulePath + "\"){\r\n"
                + "      Baseline b = baselineInfo(m);\r\n"
                + "      string s_baseline = \"\";\r\n"
                + "      if( b != null){\r\n"
                + "        int i_major = major ( b );\r\n"
                + "        int i_minor = minor ( b );\r\n"
                + "        string s_suffix = suffix( b )\r\n"
                + "        s_baseline = i_major \".\" i_minor \"\" s_suffix;\r\n"
                + "      }\r\n"
                + "      if( \"" + this.baseLineString + "\" == s_baseline){\r\n"
                + "        return m;\r\n"
                + "      }\r\n"
                + "    }\r\n"
                + "  }\r\n"
                + "  m = read(\"" + fullModulePath + "\", false);\r\n"
                + "  Baseline b = baseline( " + this.baseLineMajor + ", " + this.baseLineMinor + ", \"" + this.baseLineSuffix + "\" );\r\n"
                + "  return load(m, b, true);\r\n"
                + "}\r\n"
                + "\r\n"
                + "Module m = readModule;\r\n";
      }
    }
  }

  public void setFiltering( Boolean onOff ) {
    String dxl;
    dxl = readModule()
        + "current( m );\r\n"
        + "filtering( " + onOff.toString() + " );\r\n"
        + "refresh( m );\r\n"
    ;
    objDOORS.runStr( dxl );
    return;
  }

  //ViewList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getDanTeExportViewList() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "string s_viewName;\r\n"
        + "for s_viewName in views( m ) do {\n\r\n"
        + "  if ( s_viewName[0:14] == \"99 DanTe Export\" ) {\r\n"
        + "    if ( result != \"\" ) {\r\n"
        + "      result = result \"\\n\";\r\n"
        + "    }\r\n"
        + "    result = result s_viewName;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //ViewList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getFullViewList() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "string s_viewName;\r\n"
        + "for s_viewName in views( m ) do {\n\r\n"
        + "    if ( result != \"\" ) {\r\n"
        + "      result = result \"\\n\";\r\n"
        + "    }\r\n"
        + "    result = result s_viewName;\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //ViewList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getDanTeExportReviewViewList() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "string s_viewName;\r\n"
        + "for s_viewName in views( m ) do {\n\r\n"
        + "  if ( s_viewName[0:17] == \"99 DanTe Review BR\" ) {\r\n"
        + "    if ( result != \"\" ) {\r\n"
        + "      result = result \"\\n\";\r\n"
        + "    }\r\n"
        + "    result = result s_viewName;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //BaselineList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getDanTeExportBaselineList() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "Baseline baseline;\r\n"
        + "string baselineString  = \"\";\r\n"
        + "for baseline in m do {\n\r\n"
          + "baselineString = major( baseline ) \".\" minor( baseline ) suffix( baseline ) \"\";\r\n"
          + "if ( result != \"\" ) {\r\n"
            + "result = result \"\n\";\r\n"
          + "}\r\n"
          + "result = result baselineString;\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getObjectIdentifier( int absNum ) {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object o = object(" + absNum + ");\r\n"
        + "if ( null != o ) {\r\n"
        + "  result = identifier( o );\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getObjectNumber( int absNum ) {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object o = object(" + absNum + ");\r\n"
        + "if ( null != o ) {\r\n"
        + "  result = number( o );\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //ViewList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getColumnAttributesInView() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "Column c;\r\n"
        + "for c in m do {\n\r\n"
        + "  string s_attrName = attrName ( c );\r\n"
        + "  if ( result != \"\" ) {\r\n"
        + "    result = result \"\\n\";\r\n"
        + "  }\r\n"
        + "  result = result s_attrName;\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //ViewList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public boolean existsColumn( String column ) {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "string column = \"" + escapeIllegalChars( column ) + "\";\r\n"
        + "string result = \"false\";\r\n"
        + "Column c;\r\n"
        + "for c in m do {\n\r\n"
        + "  string s_title = title ( c );\r\n"
        + "  if ( column == s_title ) {\r\n"
        + "    result = \"true\";\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  //ViewList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getColumnText( int absNum, String column ) {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = object(" + absNum + ");\r\n"
        + "string column = \"" + escapeIllegalChars( column ) + "\";\r\n"
        + "string result = \"\";\r\n"
        + "Column c;\r\n"
        + "for c in m do {\n\r\n"
        + "  string s_title = title ( c );\r\n"
        + "  if ( column == s_title ) {\r\n"
        + "    result = text( c, o );\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String translateRTFtoHTML( String s ) {
    String dxl;
    dxl = "string translateRTFtoHTML( string s ) {\r\n"
        + "  string result = \"\", textString;\r\n"
        + "  int i;\r\n"
        + "  RichTextParagraph rp;\r\n"
        + "  RichText rt;\r\n"
        + "  for rp in s do {\r\n"
        + "    string paragraph = \"\";\r\n"
        + "    string chunk = \"\";\r\n"
        + "    if (rp.isBullet) {\r\n"
        + "      paragraph = \" <b>*</b> \" paragraph;\r\n"
        + "    }\r\n"
        + "    for rt in rp do {\r\n"
        + "      textString = rt.text;\r\n"
        + "      chunk = \"\";\r\n"
        + "      for (i=0; i<length(textString); i++) {\r\n"
        + "        if (textString[i:i] == \"&\") {\r\n"
        + "          chunk = chunk \"&amp;\";\r\n"
        + "        } else\r\n"
        + "        if (textString[i:i] == \"<\") {\r\n"
        + "          chunk = chunk \"&lt;\";\r\n"
        + "        } else\r\n"
        + "        if (textString[i:i] == \">\") {\r\n"
        + "          chunk = chunk \"&gt;\";\r\n"
        + "        } else {\r\n"
        + "          chunk = chunk textString[i:i];\r\n"
        + "        }\r\n"
        + "      }\r\n"
        + "      if (rt.bold) {\r\n"
        + "        chunk = \"<b><font color=\\\"#000066\\\" face=\\\"Arial,Helvetica,Verdana\\\">\" chunk \"</font></b>\";\r\n"
        + "      }\r\n"
        + "      if (rt.italic) {\r\n"
        + "       chunk = \"<i>\" chunk \"</i>\";\r\n"
        + "      }\r\n"
        + "      if (rt.underline) {\r\n"
        + "       chunk = \"<u>\" chunk \"</u>\";\r\n"
        + "      }\r\n"
        + "      if (rt.strikethru) {\r\n"
        + "       chunk = \"<s>\" chunk \"</s>\";\r\n"
        + "      }\r\n"
        + "      if (rt.superscript) {\r\n"
        + "       chunk = \"<sup>\" chunk \"</sup>\";\r\n"
        + "      }\r\n"
        + "      if (rt.subscript) {\r\n"
        + "       chunk = \"<sub>\" chunk \"</sub>\";\r\n"
        + "      }\r\n"
        + "      paragraph = paragraph chunk;\r\n"
        + "    }\r\n"
        + "    paragraph = paragraph \"<br>\";\r\n"
        + "    result = result paragraph;\r\n"
        + "  }\r\n"
        + "  return result;\r\n"
        + "}\r\n"
        + "oleSetResult( translateRTFtoHTML( \"" + escapeIllegalChars( s ) + "\" ) );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public Boolean testRtfFormatted( String s ) {
    String dxl;
    dxl = "string testRtfFormatted( string s ) {\r\n"
        + "  string result = \"false\";\r\n"
        + "  RichTextParagraph rp;\r\n"
        + "  RichText rt;\r\n"
        + "  for rp in s do {\r\n"
        + "    if (rp.isBullet) {\r\n"
        + "      result = \"true\";\r\n"
        + "      break;\r\n"
        + "    }\r\n"
        + "    for rt in rp do {\r\n"
        + "      if (rt.bold) {\r\n"
        + "        result = \"true\";\r\n"
        + "        break;\r\n"
        + "      }\r\n"
        + "      if (rt.italic) {\r\n"
        + "        result = \"true\";\r\n"
        + "        break;\r\n"
        + "      }\r\n"
        + "      if (rt.underline) {\r\n"
        + "        result = \"true\";\r\n"
        + "        break;\r\n"
        + "      }\r\n"
        + "      if (rt.strikethru) {\r\n"
        + "        result = \"true\";\r\n"
        + "        break;\r\n"
        + "      }\r\n"
        + "      if (rt.superscript) {\r\n"
        + "        result = \"true\";\r\n"
        + "        break;\r\n"
        + "      }\r\n"
        + "      if (rt.subscript) {\r\n"
        + "        result = \"true\";\r\n"
        + "        break;\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return result;\r\n"
        + "}\r\n"
        + "oleSetResult( testRtfFormatted( \"" + escapeIllegalChars( s ) + "\" ) );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public String exportOlePng( int absNum, String attributeName, String filePath, String fileName, int oleNumber, String objectType ) {
    String dxl;
    dxl = readModule()
        + "string rtfString;\r\n"
        + "RichText rtf;\r\n"
        + "EmbeddedOleObject ole;\r\n"
        + "string attributeName = \"" + escapeIllegalChars( attributeName ) + "\";\r\n"
        + "string filePath = \"" + escapeIllegalChars( filePath ) + "\";\r\n"
        + "string fileName = \"" + escapeIllegalChars( fileName ) + "\";\r\n"
        + "int oleNumber = " + oleNumber + ";\r\n"
        + "string objectType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "string result = \"\";\r\n"
        + "Object o = object(" + absNum + ");\r\n"
        + "if ( null != o ) {\r\n"
        + "  rtfString = richTextWithOle( o.attributeName );\r\n"
        + "  for rtf in rtfString do {\r\n"
        + "    if ( rtf.isOle ) {\r\n"
        + "      ole = rtf.getEmbeddedOle;\r\n"
        + "      string oleFileName = fileName \"_\" oleNumber \".png\";\r\n"
        + "      oleNumber += 1;\r\n"
        + "      string errMess = exportPicture( ole, filePath oleFileName, formatPNG );\r\n"
        + "      if ( !null errMess ) {\r\n"
        + "        \r\n"
        + "      }\r\n"
        + "      else {\r\n"
        + "        if ( result != \"\" ) {\r\n"
        + "          result = result \"\\n\";\r\n"
        + "        }\r\n"
        + "        result = result \"OLE-Object im \" objectType \": [image: \" oleFileName \"]<br>\";\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public boolean containsOleObject( int absNum, String attributeName ) {
    String dxl;
    dxl = readModule()
        + "string rtfString;\r\n"
        + "RichText rtf;\r\n"
        + "string attributeName = \"" + escapeIllegalChars( attributeName ) + "\";\r\n"
        + "string result = \"false\";\r\n"
        + "Object o = object(" + absNum + ");\r\n"
        + "if ( null != o ) {\r\n"
        + "  rtfString = richTextWithOle( o.attributeName );\r\n"
        + "  for rtf in rtfString do {\r\n"
        + "    if ( rtf.isOle ) {\r\n"
        + "      result = \"true\";\r\n"
        + "      break;\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public int getTopLevelParentAbsNum( int absNum ) {
    int parent = getAbsoluteNumberFromParent( absNum );
    if ( 0 == parent ) {
      return absNum;
    }
    else {
      return getTopLevelParentAbsNum( parent );
    }
  }

  private String existsAttributeType( String attributeType ) {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "string attributeType = \"" + attributeType + "\";\r\n"
        + "AttrType at = find( m, attributeType );\r\n"
        + "if ( null == at ) {\r\n"
        + "  result = \"Attribute type not found.\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String deleteAttributeType( String attributeType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "string attributeType = \"" + attributeType + "\";\r\n"
        + "AttrType at = find( m, attributeType );\r\n"
        + "if ( null == at ) {\r\n"
        + "  result = \"Could not find attribute type '\" attributeType \"'\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  bool result = delete( at, result );\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  private String createAttributeEnumerationType( String attributeType, String names, String values, String colors ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "string attributeType = \"" + attributeType + "\";\r\n"
        + "string names[] = { " + names + " };\r\n"
        + "int values[] = { " + values + " };\r\n"
        + "int colors[] = { " + colors + " };\r\n"
        + "AttrType at = create( attributeType, names, values, colors, result );\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  // AttrBaseType:
  // Scalar: attrDate, attrInteger, attrReal, attrText, attrString, attrUsername
  // Aggregate: attrEnumeration
  public String createAttributeType( String attributeType, String attrBaseType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "string attributeType = \"" + attributeType + "\";\r\n"
        + "AttrBaseType attrBaseType = " + attrBaseType + ";\r\n"
        + "AttrType at = create( attributeType, attrBaseType, result );\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  // Colormap:
  // 0: White, 1: NewGrey1, 2: NewGrey1, 3: NewGrey1, 4: White, 5: White, 6: NewGrey2, 7: NewGrey3, 8: Grey77, 9: Black
  // 10: Black, 11: Maroon, 12: Black, 13: Red, 14: Red, 15: NewGrey2, 16: Dark_Turquoise, 17: Light_Blue2, 18: Light_Blue, 19: Dark_Turquoise
  // 20: Pink, 21: Blue, 22: Black, 23: White, 24: Brown, 25: Sea_Green, 26: Navy, 27: Red, 28: Yellow, 29: Green
  // 30: Maroon, 31: Orange, 32: Rosy_Brown, 33: Grey82, 34: Grey77, 35: Red, 36: Red, 37: Light_Blue, 38: Red, 39: Magenta
  // 40: Red_Grey, 41: Blue, 42: Cyan, 43: Magenta, 44: White, 45: Orange, 46: Brown, 47: White, 48: Grey82, 49: Grey77
  // 50: Grey66, 51: Grey55, 52: Grey44, 53: Grey33, 54: Grey22, 55: Grey11, 56: Black, 57: Grey66, 58: White, 59: Black
  // 60: Grey77, 61: NewGrey1
  // Missing: Purple, Lime_Green, Peru, Firebrick, Thistle, NewGrey2-4.
  // t_Object Type: Black 22, Brown 24, Sea_Green 25, Navy 26
  private String createAttributeTypeT_ObjectType() {
    String attributeTypeName = "t_Object Type";
    String names = "\"heading\", \"information\", \"requirement\", \"predefinition\"";
    String values = "0, 0, 0, 0";
    String colors = "22, 24, 25, 26";
    String result = existsAttributeType( attributeTypeName );
    if ( !"".equals( result ) ) {
      result = createAttributeEnumerationType( attributeTypeName, names, values, colors );
    }
    return result;
//    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
//      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
//    }
//    String dxl;
//    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
//        + "AttrType at = find( m, \"t_Object Type\" );\r\n"
//        + "string errmess = \"\";\r\n"
//        + "if ( null == at ) {\r\n"
//        + "  print( \"Object Type not found.\\r\\n\" );\r\n"
//        + "}\r\n"
//        + "else {\r\n"
//        + "  bool result = delete( at, errmess );\r\n"
//        + "  if ( null == at ) {\r\n"
//        + "    print( \"Delete failed: '\" errmess \"'.\" );\r\n"
//        + "  }\r\n"
//        + "}\r\n"
//        + "string names[] = { \"a\", \"b\", \"c\", \"d\", \"e\", \"f\", \"g\", \"h\", \"i\", \"j\" };\r\n"
////        + "string names[] = { \"heading\", \"information\", \"requirement\", \"predefinition\" };\r\n"
//        + "int values[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };\r\n"
////        + "int values[] = { 0, 0, 0, 0 };\r\n"
//        + "int colors[] = { 60, 61, -1, -1, -1, -1, -1, -1, -1, -1 };\r\n"
////        + "int colors[] = { 22, 24, 25, 26 };\r\n"
//        + "at = create( \"t_Object Type\", names, values, colors, errmess );\r\n"
//        + "if ( null == at ) {\r\n"
//        + "  print( \"Create failed: '\" errmess \"'.\\r\\n\" );\r\n"
//        + "}\r\n"
//        + "\r\n"
//        + "\r\n"
//        + "\r\n"
//        + "oleSetResult( errmess );\r\n"
//    ;
//    objDOORS.runStr( dxl );
//    return objDOORS.Result();
  }

  private String createAttributeTypeFO_Object_Type() {
    String attributeTypeName = "FO_Object_Type";
    String names =
        "\"Heading\", "
            + "\"Description\", "
            + "\"Vehicle function\", "
            + "\"Function contribution\", "
            + "\"Subfunction\", "
            + "\"Persistent condition\", "
            + "\"Refinement\", "
            + "\"Precondition\", "
            + "\"Trigger\", "
            + "\"End condition\", "
            + "\"Function transition\", "
            + "\"Instance\", "
            + "\"-\""
        ;
    String values = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13";
    String colors = "-1, -1, 26, 22, 26, 21, 22, 25, 25, 26, 11, 16, -1";
    String result = existsAttributeType( attributeTypeName );
    if ( !"".equals( result ) ) {
      result = createAttributeEnumerationType( attributeTypeName, names, values, colors );
    }
    return result;
  }

  private String createAttributeTypeT_ASIL() {
    String attributeTypeName = "T_ASIL";
    String names =
        "\"QM\", "
            + "\"QM(A)\", "
            + "\"QM(B)\", "
            + "\"QM(C)\", "
            + "\"QM(D)\", "
            + "\"ASIL A\", "
            + "\"ASIL A(A)\", "
            + "\"ASIL A(B)\", "
            + "\"ASIL A(C)\", "
            + "\"ASIL A(D)\", "
            + "\"ASIL B\", "
            + "\"ASIL B(B)\", "
            + "\"ASIL B(C)\", "
            + "\"ASIL B(D)\", "
            + "\"ASIL C\", "
            + "\"ASIL C(C)\", "
            + "\"ASIL C(D)\", "
            + "\"ASIL D\", "
            + "\"ASIL D(D)\", "
            + "\"ASIL X\", "
            + "\"k.A.\", "
            + "\"-\""
        ;
    String values = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22";
    String colors = "-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1";
    String result = existsAttributeType( attributeTypeName );
    if ( !"".equals( result ) ) {
      result = createAttributeEnumerationType( attributeTypeName, names, values, colors );
    }
    return result;
  }

  public String createAttribute( String where, String attributeName, String attributeType, String defaultValue, String descriptionValue ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "string attributeName = \"" + attributeName + "\";\r\n"
        + "string attributeType = \"" + attributeType + "\";\r\n"
        + "string defaultValue = \"" + defaultValue + "\";\r\n"
        + "string descriptionValue = \"" + descriptionValue + "\";\r\n"
        + "AttrDef ad = create " + where + " type attributeType description descriptionValue attribute attributeName default defaultValue;\r\n"
        + "if ( null == ad ) {\r\n"
        + "  result = \"Couldn't create attribute '\" attributeName \"'.\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getURL( int absNum ) {
    if (absNum == 0) {
      return getModuleURL();
    }
    else {
      return getObjectURL( absNum );
    }
  }

  private String getObjectURL( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "string result;\r\n"
        + "if (o != null) {\r\n"
        + "  result = getURL( o );\r\n"
        + "}\r\n"
        + "else {\n"
        + "  result = \"\";\r\n"
        + "}\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  private String getModuleURL(){
    String dxl;
    dxl = readModule()
        + "string result;\r\n"
        + "if (m != null) {\r\n"
        + "  result = getURL( m );\r\n"
        + "}\r\n"
        + "else {\n"
        + "  result = \"\";\r\n"
        + "}\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getCurrentURL() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "string result;\r\n"
        + "if (o != null) {\r\n"
        + "  result = getURL( o );\r\n"
        + "}\r\n"
        + "else {\n"
        + "  result = \"\";\r\n"
        + "}\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getLegacyURL(String url ) {
    String dxl;
    dxl = "string legacyUrl;\r\n"
        + "string result = getLegacyURL( \"" + url + "\", legacyUrl );\r\n"
        + "if ( result == null ) {\r\n"
        + "  result = legacyUrl;\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  result = \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String decodeURL( String legacyUrl ) {
    String dxl;
    dxl = "string dbHost;\r\n"
        + "int dbPort;\r\n"
        + "string dbName;\r\n"
        + "string dbID;\r\n"
        + "Item i;\r\n"
        + "ModuleVersion modVer;\r\n"
        + "int absNo;\r\n"
        + "string typeString = \"\";\r\n"
        + "string fullNameString = \"\";\r\n"
        + "string descriptionString = \"\";\r\n"
        + "string result = decodeURL( \"" + legacyUrl + "\", dbHost, dbPort, dbName, dbID, i, modVer, absNo );\r\n"
        + "if ( result == null ) {\r\n"
        + "  if ( i != null ) {\r\n"
        + "    typeString = type ( i );\r\n"
        + "    fullNameString = fullName( i );\r\n"
        + "    descriptionString = description( i );\r\n"
        + "  }\r\n"
        + "  result = dbHost \"\\r\\n\" dbPort \"\\r\\n\" dbName \"\\r\\n\" dbID \"\\r\\n\" absNo \"\\r\\n\" typeString \"\\r\\n\" fullNameString \"\\r\\n\" descriptionString \"\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  result = \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public boolean existsObject( int absoluteNumber ) {
    String dxl;
    dxl = readModule()
        + "string result = \"false\";\r\n"
        + "Object o = object( " + absoluteNumber + " );\r\n"
        + "if ( null != o ) {\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  @Deprecated
  public boolean isObjectDeletedNew( int absoluteNumber ) {
    String dxl;
    dxl = readModule()
        + "string result = \"false\";\r\n"
        + "Object o = object( " + absoluteNumber + " );\r\n"
        + "if ( isDeleted( o ) ) {\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean existsObjectEvenWhenDeleted( int absoluteNumber ) {
    String dxl;
    dxl = readModule()
        + "string result = \"false\";\r\n"
        + "Object o;\r\n"
        + "for o in entire( m ) do {\r\n"
        + "  if ( o.\"Absolute Number\" \"\" == \"" + absoluteNumber + "\" ) {\r\n"
        + "    result = \"true\";\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean isObjectDeleted( int absoluteNumber ) {
    if(absoluteNumber == 0){
      return false;
    }
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object o;\r\n"
        + "for o in entire( m ) do {\r\n"
        + "  if ( o.\"Absolute Number\" \"\" == \"" + absoluteNumber + "\" ) {\r\n"
        + "    if ( isDeleted( o ) ) {\r\n"
        + "      result = \"true\";\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      result = \"false\";\r\n"
        + "    }\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean isObjectVisibleAndNotDeleted( int absoluteNumber ) {
    String dxl;
    dxl = readModule()
        + "string result = \"false\";\r\n"
        + "Object o;\r\n"
        + "for o in entire( m ) do {\r\n"
        + "  if ( o.\"Absolute Number\" \"\" == \"" + absoluteNumber + "\" ) {\r\n"
        + "    if ( isVisible( o ) && !isDeleted( o ) ) {\r\n"
        + "      result = \"true\";\r\n"
        + "    }\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  //TODO: Testen

  /**
   * Will try to undelete Object referenced by absoluteNumber.
   * Fails, if parent is also deleted.
   * In this case Use unDeleteObjectAndMoveIfNecessary instead.
   * @param absoluteNumber
   * @return
   */
  public boolean unDeleteObject( int absoluteNumber ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "Object o;\r\n"
        + "for o in entire( m ) do {\r\n"
        + "  if ( o.\"Absolute Number\" \"\" == \"" + absoluteNumber + "\" ) {\r\n"
        + "    if ( null == undelete( o ) ) {\r\n"
        + "      result = \"true\";\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      result = \"false\";\r\n"
        + "    }\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  /**
   * Will try to undelete Object referenced by absoluteNumber.
   * if parent is also deleted, referenced object will also be moved to
   * its last existing parent.
   *
   * ATTENTION: Doors does not allow to undelete an object, while parent stays deleted.
   * So we have to undelete the upper most deleted parent, move the queried object, and
   * delete the previously undeleted tree again.
   *
   * Undeleting an element causes its children to get undeleted, too.
   *
   * @param absNum object to undelete
   * @return absNum of parent or -1 if undeleteOperation failed.
   */
  public int unDeleteObjectAndMoveIfNecessary( Integer absNum ) {
    int parentAbsNum = getAbsoluteNumberFromParentEvenWhenDeleted( absNum );
    Logger.info( "absNum " + absNum + " has parent " + parentAbsNum );
    if (!isObjectDeleted( absNum )) {
      //object to undelete is not deleted, nothing to do.
      Logger.info( "unable to undelete absnum " + absNum + ": is not deleted..." );
      return parentAbsNum;
    }
    if (parentAbsNum < 0) {
      //no valid parent absolute Number
      Logger.info("unable to undelete absnum " + absNum + ": parent absnum is not valid...");
      return -1;
    }
    else if (parentAbsNum == 0) {
      //topLevel Requirement, can always be undeleted
      Logger.info( "trying t undelete topLevel Absnum: " + absNum);
      if (unDeleteObject( absNum )) {
        return parentAbsNum;
      }
      return -1;
    }
    else if (isObjectDeleted( parentAbsNum )) {
      Logger.info( "Parent of absnum to undelete (" + absNum + ") is also deleted. ParentAbsnum=" + parentAbsNum );
      //parent does not exist, so we have to undelete upper most deleted parent, first
      int lastDeletedParent = getLastDeletedParent( parentAbsNum );
      Logger.info( "LastDeletedParent for absnum " + parentAbsNum + " is " + lastDeletedParent );
      if (lastDeletedParent > 0) {
        if (unDeleteObject( lastDeletedParent )) {
          //then we have to move the relevant child, to last existing parent
          Logger.info ("Undeleted lastDeletedParent");
          int nextExistingParent = getAbsoluteNumberFromParent( lastDeletedParent );
          String message = moveReqInDOORS( absNum, nextExistingParent, true );
          if (!message.contains( "failed" )) {
            //if move succeeded, delete lastDeletedParent again
            deleteObject( lastDeletedParent );
            return nextExistingParent;
          }
        }
      }
      return -1;
    }
    else {
      //parent exists, so we can undelete
      if (unDeleteObject( absNum )) {
        Logger.info( "undeleted absnum: " + absNum );
        return parentAbsNum;
      }
      Logger.info( "undeleting absnum " + absNum + " failed" );
      return -1;
    }
  }

  private int getLastDeletedParent( int childAbsNum ) {
    int lastDeletedParent = childAbsNum;
    int parentAbsNum = getAbsoluteNumberFromParentEvenWhenDeleted( childAbsNum );
    while (parentAbsNum > 0) {
      if (isObjectDeleted( parentAbsNum )) {
        lastDeletedParent = parentAbsNum;
        parentAbsNum = getAbsoluteNumberFromParentEvenWhenDeleted( parentAbsNum );
      }
      else {
        return lastDeletedParent;
      }
    }
    //deleted top-level requirement
    return lastDeletedParent;
  }

  public int getAbsoluteNumberFromParentEvenWhenDeleted( Integer childAbsNum ) {
    String dxl;
    dxl = readModule()
        + "string result = \"0\";\r\n"
        + "Object o;\r\n"
        + "for o in entire( m ) do {\r\n"
        + "  if ( o.\"Absolute Number\" \"\" == \"" + childAbsNum + "\" ) {\r\n"
        + "    Object p = parent o;\r\n"
        + "    if (null != p) {\r\n"
        + "      int an = p.\"Absolute Number\";\r\n"
        + "      result = an \"\";\r\n"
        + "    }\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }


  /* To be used later...
  public int getAbsoluteNumberOfCurrent() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "string result;\r\n"
        + "int absNum = 0;\r\n"
        + "if ( null != o ) {\r\n"
        + "  absNum = o.\"Absolute Number\";\r\n"
        + "}\r\n"
        + "result = absNum \"\";\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public boolean oleActivateCurrent() {
    final int absoluteNumberOfCurrent = getAbsoluteNumberOfCurrent();
    return oleActivate( absoluteNumberOfCurrent );
  }

  public boolean oleActivate( int absoluteNumber ) {
    String dxl;
    dxl = readModule()
        + "Object o = object( " + absoluteNumber + ", m );\r\n"
        + "string result = \"false\";\r\n"
        + "if ( null != o ) {\r\n"
        + "  if ( oleIsObject( o ) ) {\r\n"
        + "    if ( oleActivate( o ) ) {\r\n"
        + "      result = \"true\";\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

*/
  @Deprecated
  public void gotoReqInDOORS( int absoluteNumber ) {
    String dxl;
    dxl = readModule()
        + "Object o = gotoObject( " + absoluteNumber + ", m );\r\n"
    ;
    objDOORS.runStr( dxl );
  }

  public void updateCurrentRowHeadingInDOORS( String text, String objectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "    o.\"Object Type\" = objType;\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EAText = \"" + escapeIllegalChars( text ) + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  Object o = current\r\n"
        + "  if (o.\"Object Heading\" != EAText) {\r\n"
        + "    o.\"Object Heading\" = EAText\r\n"
        + "    o.\"EALink\" = \"\"\r\n"
        + objTypeStr
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
  }

  public void updateCurrentRowTextInDOORS( String text, String objectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "  o.\"Object Type\" = objType;\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EAText = \"" + escapeIllegalChars( text ) + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  Object o = current\r\n"
        + "  o.\"Object Text\" = EAText\r\n"
        + "  o.\"EALink\" = \"\"\r\n"
        + objTypeStr
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
  }

  public void updateCurrentRowRichTextInDOORS( String text, String objectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "  o.\"Object Type\" = objType;\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EAText = \"" + escapeIllegalChars( text ) + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  Object o = current\r\n"
        + "  o.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
        + "  o.\"EALink\" = \"\"\r\n"
        + objTypeStr
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
  }

  public int countChildrenCurrent() {
    String dxl;
    dxl = readModule()
        + "Object oc = current m;\r\n"
        + "Object o;\r\n"
        + "int count = 0;\r\n"
        + "for o in oc do {\r\n"
        + "  count += 1;\r\n"
        + "}\r\n"
        + "oleSetResult( count \"\" );\r\n"
    ;
    objDOORS.runStr(dxl);
    return Integer.parseInt( objDOORS.Result() );
  }

  public int countNotDeletedChildren( int absNum) {
    String dxl;
    dxl = readModule()
        + "Object oc = object(" + absNum + ", m);\r\n"
        + "Object o;\r\n"
        + "int count = 0;\r\n"
        + "if (oc != null) {\r\n"
        + "  for o in oc do {\r\n"
        + "    if(!isDeleted( o )){\r\n"
        + "      count += 1;\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( count \"\" );\r\n"
    ;
    objDOORS.runStr(dxl);
    return Integer.parseInt( objDOORS.Result() );
  }

  public int countChildrenCurrentRecursive() {
    String dxl;
    dxl = "int countChildrenCurrentRecursive( Object oc ) {\r\n"
        + "  int count = 0;\r\n"
        + "  Object o;\r\n"
        + "  for o in oc do {\r\n"
        + "    count += 1;\r\n"
        + "    count += countChildrenCurrentRecursive( o );\r\n"
        + "  }\r\n"
        + "  return count;\r\n"
        + "}\r\n"
        + readModule()
        + "Object oc = current m;\r\n"
        + "int count = countChildrenCurrentRecursive( oc );\r\n"
        + "oleSetResult( count \"\" );\r\n"
    ;
    objDOORS.runStr(dxl);
    return Integer.parseInt( objDOORS.Result() );
  }

  public int countVisibleObjects(){
    String dxl;
    dxl = readModule()
        + "Object o1 = first current Module\n\n"
        + "Object o2 = last current Module\n\n"
        + "int count=1\n\n"
        + "while (o1 != o2) {\n\n"
        + "  count++\n\n"
        + "  o1 = next o1\n\n"
        + "}"
        + "oleSetResult( count \"\" );\r\n"
    ;
    objDOORS.runStr(dxl);
    return Integer.parseInt( objDOORS.Result() );
  }

  public String getChildrenCurrent() {
    String dxl;
    dxl = readModule()
        + "Object oc = current m;\r\n"
        + "Object o;\r\n"
        + "string result = \"\";\r\n"
        + "for o in oc do {\r\n"
        + "  result = result o.\"Absolute Number\" \" \";\r\n"
        + "  string heading = o.\"Object Heading\";\r\n"
        + "  if ( heading == \"\" ) {\r\n"
        + "    result = result \"Empty Heading\\r\\n\";\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    result = result o.\"Object Heading\" \"\\r\\n\";\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public List<String> getAllObjectAttributeNames(){
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "string attrName;\r\n"
        + "for attrName in m do {\r\n"
        +   "result = result attrName;\r\n"
        +   "result = result \"" + SPLIT_STRING + "\";\r\n"
        + "}\r\n"
        + "oleSetResult(result);\r\n"
        ;
    objDOORS.runStr( dxl );
    String stringResult = objDOORS.Result();
    String[] attributeNames = stringResult.split( SPLIT_STRING );
    List<String> attributeNameList = new ArrayList<>( attributeNames.length );
    for (String attributeName : attributeNames) {
      if(!Helper.isNullOrEmpty( attributeName )){
        attributeNameList.add( attributeName );
      }
    }
    return attributeNameList;
  }

  public boolean isMultiValueType (String attributeName){
    String dxl;
    dxl = readModule()
        + "AttrDef ad = find(m, \"" + attributeName + "\");\r\n"
        + "if (ad != null ) { \r\n"
        + "  bool multi = ad.multi;\r\n"
        + "  oleSetResult( multi \"\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"false\" );\r\n"
        + "}\r\n"
        ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean isEnumType(String attributeName) {
    String dxl;
    dxl = readModule()
        + "AttrDef ad = find(m, \"" + attributeName + "\");\r\n"
        + "if (ad != null) {\r\n"
        + "  AttrType at = ad.type;\r\n"
        + "  string strType = stringOf( at.type );\r\n"
        + "  if ( strType == \"Enumeration\" ) {\r\n"
        + "    oleSetResult ( \"true\" );\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    oleSetResult ( \"false\" );\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult ( \"false\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public String[] getEnumValues (String enumAttributeName) {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "AttrDef ad = find(m, \"" + enumAttributeName + "\");\r\n"
        + "if (ad != null) {\r\n"
        + "  AttrType at = ad.type;\r\n"
        + "  string strType = stringOf( at.type );\r\n"
        + "  if ( strType == \"Enumeration\" ) {\r\n"
        + "    for (i = 0; i < at.size; i++) {\r\n"
        + "      if ( result != \"\" ) {\r\n"
        + "        result = result \",\"\r\n"
        + "      }\r\n"
        + "      result = result at.strings[i] \"\";\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult ( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    String result = objDOORS.Result();
    if (!Helper.isNullOrEmpty( result ) && result.contains( "," )) {
      return result.split( "," );
    }
    else {
      return null;
    }
  }

  public boolean existsAttribute( String attrName ) {
    String dxl;
    dxl = readModule()
        + "string result;\r\n"
        + "if( exists( attribute( \"" + attrName + "\" ) ) ) {\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  result = \"false\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean existsAttributeInCurrent( String attrName ) {
    String dxl;
    dxl = readCurrentModule()
        + "string result;\r\n"
        + "if( exists( attribute( \"" + attrName + "\" ) ) ) {\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  result = \"false\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean close() {
    String dxl;
    dxl = readModule()
        + "bool closed = close( m, false );\r\n"
        + "if ( closed ) {\r\n"
        + "  oleSetResult( \"true\" );\r\n"
        + "} else {\r\n"
        + "  oleSetResult( \"false\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public void save() {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "save( m );\r\n"
    ;
    objDOORS.runStr( dxl );
    return;
  }

  public void createBaseline( String suffix, String annotation ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "save (m); \r\n"
        + "string suffix = \"" + escapeIllegalChars( suffix ) + "\"; \r\n"
        + "string annotation = \"" + escapeIllegalChars( annotation ) + "\"; \r\n"
        + "Baseline b = nextMinor( suffix );\r\n"
        + "create( m, b, annotation );\r\n"
    ;
    objDOORS.runStr( dxl );
    return;
  }

  //BaselineList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getMostRecentBaselineMajor() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "current( m );\r\n"
        + "Baseline b = getMostRecentBaseline( m );\r\n"
        + "if ( null != b ) {\r\n"
        + "  string result = major( b ) \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  //BaselineList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getMostRecentBaselineMinor() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "current( m );\r\n"
        + "Baseline b = getMostRecentBaseline( m );\r\n"
        + "if ( null != b ) {\r\n"
        + "  string result = minor( b ) \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  //BaselineList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getMostRecentBaselineSuffix() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "current( m );\r\n"
        + "Baseline b = getMostRecentBaseline( m );\r\n"
        + "if ( null != b ) {\r\n"
        + "  string result = suffix( b ) \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  //BaselineList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getMostRecentBaselineAnnotation() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"\";\r\n"
        + "current( m );\r\n"
        + "Baseline b = getMostRecentBaseline( m );\r\n"
        + "if ( null != b ) {\r\n"
        + "  string result = annotation( b ) \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public boolean loadBaseline( String baseline, boolean show ) {
    String dxl;
    dxl = "bool show = " + Boolean.toString( show ) + ";\r\n"
        + "Module m = read( \"" + fullModulePath + "\", show );\r\n"
        + "string result = \"false\";\r\n"
        + "Baseline b;\r\n"
        + "string baseline = \"" + escapeIllegalChars( baseline ) + "\";\r\n"
        + "if ( \"\" != baseline ) {\r\n"
        + "  for b in m do {\r\n"
        + "    int i_major = major( b );\r\n"
        + "    int i_minor = minor( b );\r\n"
        + "    string s_suffix = suffix( b );\r\n"
        + "    string s_baseline = i_major \".\" i_minor \"\" s_suffix;\r\n"
        + "    if ( baseline == s_baseline ) {\r\n"
        + "      Module newM = load( m, b, show );\r\n"
        + "      if ( null != newM ) {\r\n"
        + "        result = \"true\";\r\n"
        + "        break;\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
        + "\r\n"
    ;
    objDOORS.runStr( dxl );
    boolean isValidBaseline = Boolean.parseBoolean( objDOORS.Result() );
    if ( isValidBaseline ) {
      useBaseline( baseline );
    }
    return isValidBaseline;
  }

  //BaselineList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  private String getMajorFromBaselineString( String baseline ) {
    String dxl;
    dxl = "Module m = read( \"" + fullModulePath + "\", false );\r\n"
        + "string result = \"false\";\r\n"
        + "Baseline b;\r\n"
        + "string baseline = \"" + escapeIllegalChars( baseline ) + "\";\r\n"
        + "if ( \"\" != baseline ) {\r\n"
        + "  for b in m do {\r\n"
        + "    int i_major = major( b );\r\n"
        + "    int i_minor = minor( b );\r\n"
        + "    string s_suffix = suffix( b );\r\n"
        + "    string s_baseline = i_major \".\" i_minor \"\" s_suffix;\r\n"
        + "    if ( baseline == s_baseline ) {\r\n"
        + "      result = i_major \"\";\r\n"
        + "      break;\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
        + "\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //BaselineList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  private String getMinorFromBaselineString( String baseline ) {
    String dxl;
    dxl = "Module m = read( \"" + fullModulePath + "\", false );\r\n"
        + "string result = \"false\";\r\n"
        + "Baseline b;\r\n"
        + "string baseline = \"" + escapeIllegalChars( baseline ) + "\";\r\n"
        + "if ( \"\" != baseline ) {\r\n"
        + "  for b in m do {\r\n"
        + "    int i_major = major( b );\r\n"
        + "    int i_minor = minor( b );\r\n"
        + "    string s_suffix = suffix( b );\r\n"
        + "    string s_baseline = i_major \".\" i_minor \"\" s_suffix;\r\n"
        + "    if ( baseline == s_baseline ) {\r\n"
        + "      result = i_minor \"\";\r\n"
        + "      break;\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
        + "\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //BaselineList unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  private String getSuffixFromBaselineString( String baseline ) {
    String dxl;
    dxl = "Module m = read( \"" + fullModulePath + "\", false );\r\n"
        + "string result = \"false\";\r\n"
        + "Baseline b;\r\n"
        + "string baseline = \"" + escapeIllegalChars( baseline ) + "\";\r\n"
        + "if ( \"\" != baseline ) {\r\n"
        + "  for b in m do {\r\n"
        + "    int i_major = major( b );\r\n"
        + "    int i_minor = minor( b );\r\n"
        + "    string s_suffix = suffix( b );\r\n"
        + "    string s_baseline = i_major \".\" i_minor \"\" s_suffix;\r\n"
        + "    if ( baseline == s_baseline ) {\r\n"
        + "      result = s_suffix;\r\n"
        + "      break;\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
        + "\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public void showDeletedObjects (boolean show) {
    String dxl;
    dxl = readModule()
        + "current = m;\r\n"
        + "showDeletedObjects(" + show + "); \r\n"
        + "refresh( m )"
    ;
    objDOORS.runStr( dxl );
  }

  public boolean loadView( String viewName ) {
    String dxl;
    dxl = readModule()
        + "string viewName = \"" + escapeIllegalChars( viewName ) + "\";\r\n"
        + "string result = \"true\";\r\n"
        + "bool isLoaded = load( m, view viewName );\r\n"
        + "if ( !isLoaded ) {\r\n"
        + "  result = \"false\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean applyApprovedFilter( String BR ) {
    String dxl;
    dxl = readModule()
        + "current( m );\r\n"
        + "string BR = \"" + escapeIllegalChars( BR ) + "\";\r\n"
        + "string workFlowStatusBR = \"TS WorkflowStatus\" BR;\r\n"
        + "string result = \"true\";\r\n"
        + "Filter f1 = current;\r\n"
        + "Filter f2;\r\n"
        + "if ( !null f1 ) {\r\n"
        + "  filtering off;\r\n"
        + "  f2 = f1 && ((( attribute workFlowStatusBR == \"Approved\" ) && ( attribute \"Object Type\" == \"test case\" )) || ( attribute \"Object Number\" < \"5\" ));\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "   f2 = ((( attribute workFlowStatusBR == \"Approved\" ) && ( attribute \"Object Type\" == \"test case\" )) || ( attribute \"Object Number\" < \"5\" ));\r\n"
        + "}\r\n"
        + "set( m, f2);\r\n"
        + "ancestors true;\r\n"
        + "Object o;\r\n"
        + "for o in m do {\r\n"
        + "  if ( isVisible( o ) && !isDeleted( o ) ) {\r\n"
        + "    if (\r\n"
        + "      o.\"Object Type\" \"\" == \"init step\" ||\r\n"
        + "      o.\"Object Type\" \"\" == \"run step\" ||\r\n"
        + "      o.\"Object Type\" \"\" == \"shutdown step\"\r\n"
        + "     ) {\r\n"
        + "      descendants( true );\r\n"
        + "      break;\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "filtering on;\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean gotoFirstInDOORSModule() {
    String dxl;
    dxl = readModule()
        + "Object o2 = first m;\r\n"
        + "string result;\r\n"
        + "if (null o2) {\r\n"
        + "  result = \"false\";\r\n"
        + "}\r\n" + "else {\r\n"
        + "  int an = o2.\"Absolute Number\";\r\n"
        + "  Object obj = gotoObject( an, m );\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  //TODO: Testen
  public boolean gotoLastInDOORSModule() {
    String dxl;
    dxl = readModule()
        + "Object o2 = last m;\r\n"
        + "string result;\r\n"
        + "if (null o2) {\r\n"
        + "  result = \"false\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  int an = o2.\"Absolute Number\";\r\n"
        + "  Object obj = gotoObject( an, m );\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean moveDownInDOORS() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "Object o2 = next o;\r\n"
        + "string result;\r\n"
        + "if (null o2) {\r\n"
        + "  result = \"false\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  int an = o2.\"Absolute Number\";\r\n"
        + "  Object obj = gotoObject( an, m );\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public String getCurrentRowHeadingFromDOORS() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "string result = o.\"Object Heading\";\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getCurrentRowTextFromDOORS() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "string result = o.\"Object Text\";\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public int getCurrentAbsoluteNumberFromDOORS() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "if (o == null) {\r\n"
        + "  oleSetResult( \"0\" );\r\n"
        + "} else {\r\n"
        + "  string result = o.\"Absolute Number\";\r\n"
        + "  oleSetResult( result )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public int getAbsoluteNumberOfFirstObject() {
    String dxl;
    dxl = readModule()
        + "string result;\r\n"
        + "Object o = m[1];\r\n"
        + "if (o == null) {\r\n"
        + "  result = \"0\";\r\n"
        + "} else {\r\n"
        + "  result = o.\"Absolute Number\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public int getNextAbsoluteNumber( int absNum ) {
    String dxl;
    dxl = readModule()
        + "string result;\r\n"
        + "Object o = object(" + absNum + ");\r\n"
        + "if (o == null) {\r\n"
        + "  result = \"0\";\r\n"
        + "} else {\r\n"
        + "  o = next(o);\r\n"
        + "  if (o == null) {\r\n"
        + "    result = \"0\";\r\n"
        + "  } else {\r\n"
        + "    result = o.\"Absolute Number\";\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public String getAttributeFromDOORS( String attribute ) {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "string result;\r\n"
        + "if ( exists attribute \"" + attribute + "\" ) {\r\n"
        + "  result = o.\"" + attribute + "\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  result = \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getAttributeFromDOORS( int absNum, String attribute ) {
    String dxl;
    dxl = readModule(false)
        + "Object o = object(" + absNum + ", m);\r\n"
        + "string result;\r\n"
        + "AttrDef ad = (find (m, \"" + attribute + "\"));\r\n"
        + "if (( ad != null ) && (o != null)) {\r\n"
        + "  result = o.\"" + attribute + "\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  result = \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getSeveralAttributesFromDOORS(int absNum, List<String> attributeNames){
    String dxl;
    dxl = readModule(false)
        + "Object o = object(" + absNum + ", m);\r\n"
        + "string result = \"\";\r\n"
        + "string value;\r\n"
    ;
    for (String attributeName : attributeNames) {
      dxl = dxl
        + "result = result \"" + SPLIT_STRING + "\";\r\n"
        + "value = o.\"" + attributeName + "\";\r\n"
        + "result = result value;\r\n"
      ;
    }
    dxl = dxl
            + "oleSetResult( result) \r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String getRichTextFromDOORS( int absNum, String attribute ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "string result = \"\";\r\n"
        + "if (o != null) {\r\n"
        + "  if ( exists attribute \"" + attribute + "\" ) {\r\n"
        + "    result = richText( o.\"" + attribute + "\" );\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public void setRichTextInDOORS( int absNum, String attribute, String richText ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "string EAText = \"" + escapeIllegalChars(richText) + "\";\r\n"
        + "if (o != null) {\r\n"
        + "  if ( exists attribute \"" + attribute + "\" ) {\r\n"
        + "    string tmp = richText( o.\"Object Text\");\r\n"
        + "    string txt = richText( removeUnlistedRichText( EAText ) );\r\n"
        + "    if ( tmp != txt ) {\r\n"
        + "      o.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n";
    ;
    objDOORS.runStr( dxl );
  }

  public boolean setAttributeInDOORS( String attributeName, String attributeValue ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current m;\r\n"
        + "string EAText = \"" + escapeIllegalChars( attributeValue ) + "\";\r\n"
        + "if ( exists attribute \"" + attributeName + "\" ) {\r\n"
        + "  o.\"" + attributeName + "\" = EAText;\r\n"
        + "  oleSetResult( \"true\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"false\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean setAttributeInDOORS( int absNum, String attributeName, String attributeValue ) {
    attributeValue = escapeIllegalChars( attributeValue );
    if (!Helper.isNullOrEmpty( attributeValue )) {
      if (attributeValue.contains( "\n" ) && isMultiValueType( attributeName )) {
        //multi-value attributes require \n to be escaped, each other attribute works with \n (and \\n as well)
        attributeValue = attributeValue.replace( "\n", "\\n" );
      }
    }
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = object(" + absNum + ");\r\n"
        + "string EAText = \"" + attributeValue + "\";\r\n"
        + "if (o != null) {\r\n"
        + "  if ( exists attribute \"" + attributeName + "\"  && canWrite o.\"" + attributeName + "\" ) {\r\n"
        + "    o.\"" + attributeName + "\" = EAText;\r\n"
        + "    oleSetResult( \"true\" );\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    oleSetResult( \"false\" );\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"false\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public int gotoAbsoluteNumberNT( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = gotoObject("+ absNum +", m);\r\n"
        + "if ( null o ) {\r\n"
        + "  oleSetResult( \"0\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"" + absNum + "\" )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public String getEALinkFromPreviousObjectNT(  ) {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "Object o2 = previous o;\r\n"
        + "string result;\r\n"
        + "if (null o2) {\r\n"
        + "  result = \"\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  result = o2.\"EALink\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public int getAbsNumFromPreviousObjectViaEALink(  ) {
    String dxl;
    dxl = readModule()
        + "Object oCurrent = current m;\r\n"
        + "int result;\r\n"
        + "string eaLink = \"\";\r\n"
        + "while ( eaLink == \"\" ) {\r\n"
        + "  Object oPrevious = previous oCurrent;\r\n"
        + "  if (null oPrevious) {\r\n"
        + "    result = 0;\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    result = oPrevious.\"Absolute Number\";\r\n"
        + "    eaLink = oPrevious.\"EALink\" \"\";\r\n"
        + "    oCurrent = oPrevious;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result \"\")\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  /**
   * ATTENTION: different from getAbsoluteNumberFromPreviousObjectInDoors!!!
   * This method returns the absolute Number of the previous object "in a depth first tree search"
   * @return
   */
  public int getAbsoluteNumberFromPreviousObjectInDOORS(  ) {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "Object o2 = previous o;\r\n"
        + "string result;\r\n"
        + "if (null o2) {\r\n"
        + "  result = \"0\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  int an = o2.\"Absolute Number\";\r\n"
        + "  result = an \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  /**
   * ATTENTION: Different from getAbsoluteNumberFromPreviousObjectInDoors!!
   * This method returns the absolute Number of the previous object ON SAME LEVEL (in same parent) as object to given absolute Number
   * @param absNum
   * @return
   */
  public int getAbsoluteNumberFromPreviousSiblingInDOORS( int absNum  ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "Object o2 = previous(sibling(o));\r\n"
        + "string result;\r\n"
        + "if (null o2) {\r\n"
        + "  result = \"0\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  int an = o2.\"Absolute Number\";\r\n"
        + "  result = an \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public int getAbsoluteNumberFromParent() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "Object o2 = parent o;\r\n"
        + "string result;\r\n"
        + "if (null o2) {\r\n"
        + "  result = \"0\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  int an = o2.\"Absolute Number\";\r\n"
        + "  result = an \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public int getAbsoluteNumberFromParent( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "string result;\r\n"
        + "if (o != null) {\r\n"
        + "  Object o2 = parent o;\r\n"
        + "  if (null o2) {\r\n"
        + "    result = \"0\";\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    int an = o2.\"Absolute Number\";\r\n"
        + "    result = an \"\";\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  result = \"0\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public String getAllSourceIds() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "string result = \"\";\r\n"
        + "int absnum;\r\n"
        + "string sourceid;\r\n"
        + "for o in entire( m ) do {\r\n"
        + "  absnum = o.\"Absolute Number\";\r\n"
        + "  sourceid = o.\"SourceID\" ;\r\n"
        + "  result = result sourceid \"@\" absnum \"" + SPLIT_STRING + "\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )"
    ;
    objDOORS.runStr( dxl );
   return objDOORS.Result();
  }

  @Deprecated
  public String createBaselineInDOORS( String name ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "bool se = isShare( m );\r\n"
        + "bool ed = isEdit( m );\r\n"
        + "save( m );\r\n"
        + "create( m, nextMinor, \"" + name + "\");\r\n"
        + "if (se) {\r\n"
        + "  share( name m );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  if (ed) {\r\n"
        + "    edit( m );\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public void refreshCurrent() {
    String dxl;
    dxl = readModule()
        + "bringToFront( m );\r\n"
        + "refresh( m );\r\n"
    ;
    objDOORS.runStr( dxl );
  }

  public int getAbsoluteNumberFromGuidNT( String fullGuid ) {
    String guid = guidNoBrackets( fullGuid );
    String dxl;
    dxl = readModule()
        + "string guid = \"" + guid + "\";\r\n"
        + "int offset = null\r\n"
        + "int length = null\r\n"
        + "oleSetResult( \"0\" )\r\n"
        + "Object o = current m;\r\n"
        + "for o in m do {\r\n"
        + "  if ( findPlainText( o.\"EALink\" \"\", guid, offset, length, true, false ) ) {\r\n"
        + "    int an = o.\"Absolute Number\"\r\n"
        + "    oleSetResult( an \"\" )\r\n"
        + "    break\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public int getAbsoluteNumberFromGuidNT( String fullGuid, boolean searchDeletions ) {
    String forSubstring;
    if (searchDeletions) {
      forSubstring = "for o in entire(m) do {\r\n";
    }
    else {
      forSubstring = "for o in m do {\r\n";
    }
    String guid = guidNoBrackets( fullGuid );
    String dxl;
    dxl = readModule()
        + "string guid = \"" + guid + "\";\r\n"
        + "int offset = null\r\n"
        + "int length = null\r\n"
        + "oleSetResult( \"0\" )\r\n"
        + "Object o = current m;\r\n"
        + forSubstring
        + "  if ( findPlainText( o.\"EALink\" \"\", guid, offset, length, true, false ) ) {\r\n"
        + "    int an = o.\"Absolute Number\"\r\n"
        + "    oleSetResult( an \"\" )\r\n"
        + "    break\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public int getCurrentLevel() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "if (o == null) {\r\n"
        + "  oleSetResult( \"0\" );\r\n"
        + "} else {\r\n"
        + "  string result = level( o ) \"\";\r\n"
        + "  oleSetResult( result );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public int getObjectLevel( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "if (null o) {\r\n"
        + "  oleSetResult( \"0\" );\r\n"
        + "} else {\r\n"
        + "  string result = level( o ) \"\";\r\n"
        + "  oleSetResult( result );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  //Modul-IDs unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getQualifiedUniqueID() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "oleSetResult( qualifiedUniqueID( m ) );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //Modul-IDs unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getUniqueID() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "oleSetResult( uniqueID( m ) );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //Modul-Name unabhängig von einer Baseline, daher wird trotz read nicht auf baseline umgeschalten
  public String getFullName() {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", false);\r\n"
        + "oleSetResult( fullName( m ) );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public boolean moveUpInDOORS() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "Object o2 = previous o;\r\n"
        + "string result;\r\n"
        + "if (null o2) {\r\n"
        + "  result = \"false\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  int an = o2.\"Absolute Number\";\r\n"
        + "  Object obj = gotoObject( an, m );\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public String readDOORSAttribute( String attributeName ) {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "string result = o.\"" + attributeName + "\";\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String readDOORSModuleAttribute( String attributeName ) {
    String dxl;
    dxl = readModule()
        + "string attributeName = \"" + escapeIllegalChars( attributeName ) + "\";\r\n"
        + "string result = m.attributeName;\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  /**
   * Requires to set cursor on element to insert below/after before calling :
   * will cut Requirement of given absNum and insert below/after current element.
   *
   * @param cutAbsNum   of element that has to be moved
   * @param insertBelow if true cut element will be inserted below, else behind (on same level) current element
   * @return success message
   */
  public String moveReqInDOORSNT( int cutAbsNum, String insertBelow ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object oCurrent = current m;\r\n"
        + "int currentAbsNum = oCurrent.\"Absolute Number\";\r\n"
        + "Object o = gotoObject( " + cutAbsNum + ", m );\r\n"
        + "if (o != null) {\r\n"
        + "  if (cut()) {\r\n"
        + "    Object oprev = gotoObject( currentAbsNum, m );\r\n"
        + "    bool insertBelow = " + insertBelow + ";\r\n"
        + "    if (insertBelow) {\r\n"
        + "      if (pasteDown()) {\r\n"
        + "        oleSetResult( \"PasteDown ok\" );\r\n"
        + "      }\r\n"
        + "      else {\r\n"
        + "        oleSetResult( \"PasteDown failed\");\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      if (pasteSame()) {\r\n"
        + "        oleSetResult( \"PastSame ok\" );\r\n"
        + "      }\r\n"
        + "      else {\r\n"
        + "        oleSetResult( \"PasteSame failed\");\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    oleSetResult( \"Cut failed\" );\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"Cut failed\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  /**
   * will cut Requirement of given cutAbsNum and insert below/after element of prevAbsNum.
   *
   * @param cutAbsNum   of element that has to be moved
   * @param prevAbsNum  of element that will be the new predecessor
   * @param insertBelow if true cut element will be inserted below, else behind (on same level) predecessor element
   * @return success message
   */
  public String moveReqInDOORS( int cutAbsNum, int prevAbsNum, boolean insertBelow ) {
    if(prevAbsNum == 0) {
      return moveReqToTopLevelFirstPos( cutAbsNum );
    }
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = gotoObject( " + cutAbsNum + ", m );\r\n"
        + "if (o != null) {\r\n"
        + "  if (cut()) {\r\n"
        + "    Object oprev = gotoObject( " + prevAbsNum + ", m );\r\n"
        + "    if (" + insertBelow + ") {\r\n"
        + "      if (pasteDown()) {\r\n"
        + "        oleSetResult( \"PasteDown ok\" );\r\n"
        + "      }\r\n"
        + "      else {\r\n"
        + "        oleSetResult( \"PasteDown failed\");\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      if (pasteSame()) {\r\n"
        + "        oleSetResult( \"PastSame ok\" );\r\n"
        + "      }\r\n"
        + "      else {\r\n"
        + "        oleSetResult( \"PasteSame failed\");\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    oleSetResult( \"Cut failed\" );\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"Cut failed\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  private String moveReqToTopLevelFirstPos( int cutAbsNum ) {
    int firstAbsNum = getAbsoluteNumberOfFirstObject();
    if (firstAbsNum > 0 && firstAbsNum != cutAbsNum) {
      //we can not paste before first element in doors, so we have to
      //add behind first element first
      String message = moveReqInDOORS( cutAbsNum, firstAbsNum, false );
      if (message.contains( "ok" )) {
        //and move first element behind pasted element, then
        return moveReqInDOORS( firstAbsNum, cutAbsNum, false );
      }
      return message;
    }
    return "Failed to move to top level: no other top level sibling existing";
  }

  public String moveReqInDOORS( int afterDOORSAbsNum, int reqLevel ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current m;\r\n"
        + "if (cut()) {\r\n"
        + "  Object oprev = gotoObject( " + afterDOORSAbsNum + ", m );\r\n"
        + "  int lprev = level( oprev );\r\n"
        + "  if (lprev < " + reqLevel + ") {\r\n"
        + "    if (pasteDown()) {\r\n"
        + "      oleSetResult( \"PasteDown ok\" );\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      oleSetResult( \"PasteDown failed\");\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    if (pasteSame()) {\r\n"
        + "      oleSetResult( \"PastSame ok\" );\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      oleSetResult( \"PasteSame failed\");\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"Cut failed\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String valuesOfTypeOfAttribute( String attributeName ) {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "AttrDef ad = find( m, \"" + attributeName + "\" );\r\n"
        + "AttrType at = find( m, ad.typeName );\r\n"
        + "string result = \"\";\r\n"
        + "int i;\r\n"
        + "for ( i=0; i<at.size; i++ ) {\r\n"
        + "  result = result at.strings[i] \"\\n\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public int numberOfValuesOfTypeOfAttribute( String attributeName ) {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "AttrDef ad = find( m, \"" + attributeName + "\" );\r\n"
        + "AttrType at = find( m, ad.typeName );\r\n"
        + "string result = \"\" at.size \"\";\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public void writeFOObjectType( String foObjectType ) {
    writeDOORSAttribute( "FO_Object_Type", foObjectType );
  }

  public String writeDOORSAttribute( String attributeName, String value ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current m;\r\n"
        + "if (canWrite o.\"" + attributeName + "\"){\r\n"
        + "  o.\"" + attributeName + "\" = \"" + escapeIllegalChars( value ) + "\";\r\n"
        + "  oleSetResult( \"true\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"false\" )\r\n"
        + "}"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String writeDOORSModuleAttribute( String attributeName, String value ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string attributeName = \"" + escapeIllegalChars( attributeName ) + "\";\r\n"
        + "string value = \"" + escapeIllegalChars( value ) + "\";\r\n"
        + "m.attributeName = value;\r\n"
        + "string result = \"\";\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  /**
   * Read current object's attribute "SourceID" (Layout DXL)
   * Triggers Layout DXL by calling gotoObject().
   * Note: Current view must have a column for "SourceID".
   */
  public String readSourceIDCurrent() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "int an = o.\"Absolute Number\";\r\n"
        + "Object obj = gotoObject( an, m );\r\n"
        + "string result = obj.\"SourceID\";\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //TODO: Testen
  public String removeUnlistedRichText( String richText ) {
    String dxl;
    dxl = "string richText = \"" + escapeIllegalChars( richText ) + "\";\r\n"
        + "string result = removeUnlistedRichText( richText );\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public boolean gotoParentInDOORS() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "Object o2 = parent o;\r\n"
        + "string result;\r\n"
        + "if (null o2) {\r\n"
        + "  result = \"false\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  int an = o2.\"Absolute Number\";\r\n"
        + "  Object obj = gotoObject( an, m );\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public int insertObject( boolean below ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String belowString;
    if ( below ) {
      belowString = "below ";
    }
    else {
      belowString = "";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current m;\r\n"
        + "Object newObject = create( " + belowString +"( o ) )\r\n"
        + "string result;\r\n"
        + "if ( null newObject ) {\r\n"
        + "  result = \"0\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  int an = newObject.\"Absolute Number\";\r\n"
        + "  result = an \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr(dxl);
    return Integer.parseInt( objDOORS.Result() );
  }

  public String insertInitialHeadingNT( String text, String eaLinkString, boolean setObjectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (setObjectType) {
      objTypeStr = "newObject.\"Object Type\" = \"heading\"\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkString + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "Object newObject = create(m)\r\n"
        + "newObject.\"Object Heading\" = EAText\r\n"
        + "newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "oleSetResult( \"true\" )\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public int insertTopLevelHeading( String text ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }

    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EAText = \"" + escapeIllegalChars( text ) + "\";\r\n"
        + "string result;\r\n"
        + "Object newObject = create(m)\r\n"
        + "newObject.\"Object Heading\" = EAText\r\n"
        + "int an = newObject.\"Absolute Number\";\r\n"
        + "result = an \"\";\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public int insertTopLevelObject(String text) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EAText = \"" + escapeIllegalChars( text ) + "\";\r\n"
        + "Object newObject = create(m)\r\n"
        + "string result;\r\n"
        + "if ( null newObject ) {\r\n"
        + "  result = \"0\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  newObject.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
        + "  int an = newObject.\"Absolute Number\";\r\n"
        + "  result = an \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public int insertInitialObject(){
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object newObject = create(m)\r\n"
        + "string result;\r\n"
        + "if ( null newObject ) {\r\n"
        + "  result = \"0\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  int an = newObject.\"Absolute Number\";\r\n"
        + "  result = an \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr(dxl);
    return Integer.parseInt(objDOORS.Result());
  }

  public String insertHeadingBelowNT( String text, boolean setObjectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (setObjectType) {
      objTypeStr = "newObject.\"Object Type\" = \"heading\"\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current m;\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "Object newObject = create below o\r\n"
        + "newObject.\"Object Heading\" = EAText\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "oleSetResult( \"true\" )\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public static String guidNoBrackets(String fullGuid) {
    if (fullGuid.charAt(0) != '{') {
      Logger.error("String 'guid' doesn't start with '{'", null);
    }
    if (fullGuid.charAt(fullGuid.length() - 1) != '}') {
      Logger.error("String 'guid' doesn't end with '}'", null);
    }
    return fullGuid.substring(1, fullGuid.length() - 1);
  }

  public String getCurrentRowRichTextFromDOORS( String attrName ) {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "string result = richText(o.\"" + attrName + "\", false);\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl )
    ;
    return objDOORS.Result();
  }

  public void deleteCurrentOutLinksInDOORS() {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current;\r\n"
        + "Link l;\r\n"
        + "for l in (current Object) -> \"*\" do {\r\n"
        + "  delete( l );\r\n"
        + "}\r\n"
        + "flushDeletions;\r\n"
    ;
    objDOORS.runStr( dxl );
  }

  public boolean deleteCurrentInDOORS() {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    if (openInLinkModules()) {
      String dxl;
      dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
          + "Object o = current m;\r\n"
          + "Link l;\r\n"
          + "bool hasLinks = false;\r\n"
          + "for l in ( o ) <- \"*\" do {\r\n"
          + "   hasLinks = true;\r\n"
          + "}\r\n"
          + "if ( hasLinks ) {\r\n"
          + "   oleSetResult( \"false\" );\r\n"
          + "}\r\n"
          + "else {\r\n"
          + "   for l in ( o ) -> \"*\" do {\r\n"
          + "     delete( l );\r\n"
          + "   }\r\n"
          + "   flushDeletions;\r\n"
          + "   softDelete( o );\r\n"
          + "   flushDeletions;\r\n"
          + "   oleSetResult( \"true\" );\r\n"
          + "}\r\n"
      ;
      objDOORS.runStr( dxl );
      return Boolean.parseBoolean( objDOORS.Result() );
    }
    else {
      return false;
    }
  }

  public boolean deleteObject( int absNum ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    if (openInLinkModules( absNum )) {
      String dxl;
      dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
          + "Object o = object(" + absNum + ");\r\n"
          + "Link l;\r\n"
          + "bool hasLinks = false;\r\n"
          + "if (o != null) {\r\n"
          + "  for l in ( o ) <- \"*\" do {\r\n"
          + "     hasLinks = true;\r\n"
          + "  }\r\n"
          + "  if ( hasLinks ) {\r\n"
          + "     oleSetResult( \"false\" );\r\n"
          + "  }\r\n"
          + "  else {\r\n"
          + "    for l in ( o ) -> \"*\" do {\r\n"
          + "      delete( l );\r\n"
          + "    }\r\n"
          + "    flushDeletions;\r\n"
          + "    softDelete( o );\r\n"
          + "    flushDeletions;\r\n"
          + "    oleSetResult( \"true\" );\r\n"
          + "  }\r\n"
          + "}\r\n"
          + "else {\r\n"
          + "  oleSetResult( \"false\" );\r\n"
          + "}\r\n"
      ;
      objDOORS.runStr( dxl );
      return Boolean.parseBoolean( objDOORS.Result() );
    }
    else {
      return false;
    }
  }

  //FIXME: beachtet keinen Filter, Abfrage ob o == null fehlt
  public boolean hasChildrenWithoutEALink() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "Object obj;\r\n"
        + "bool hasChildrenWithoutEalink = false;\r\n"
        + "string eaLink;\r\n"
        + "for obj in o do {\r\n"
        + "  eaLink = obj.\"EALink\";\r\n"
        + "  if ( \"\" == eaLink ) {\r\n"
        + "    hasChildrenWithoutEalink = true;\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( hasChildrenWithoutEalink \"\" );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  /**
   * Creates a string containing all outgoing links from current object.
   * Pattern per link: targetAbsoluteNumber@targetModulePath
   *
   * @return string of outgoing links, e.g. link1|link2|link3
   */
  //TODO: Testen
  public String getOutgoingLinks() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "string result = \"\";\r\n"
        + "Link lnk;\r\n"
        + "for lnk in o -> \"*\" do {\r\n"
        + "  ModName_ mt = target lnk;\r\n" // Eine Modulreferenz bekommen wir auch bei geschlossenem Modul
        + "  string mn = fullName( mt );\r\n" // Daraus den Modulpfad
        + "  Object ot = target lnk;\r\n" // Bei geschlossenem Modul bekommen wir hier null
        + "  if ( null == ot ) {\r\n" // Dann
        + "    Module mr = read( mn, false );\r\n" // Modul öffnen
        + "    ot = target lnk;\r\n" // Objekt nochmals holen
        + "  }\r\n"
        + "  if ( null != ot ) {\r\n" // Wenn immer noch null, haben wir keine Berechtigung, das Modul zu lesen
        + "    string absNum = ot.\"Absolute Number\";\r\n" // In dem Fall nichts tun
        + "    if (result != \"\") {\r\n"
        + "      result = result  \"|\";\r\n"
        + "    }\r\n"
        + "    result = result absNum \"@\" mn;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  //TODO: Testen
  public String getOutgoingLinks( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "string result = \"\";\r\n"
        + "Link lnk;\r\n"
        + "if (o != null) {\r\n"
        + "  for lnk in o -> \"*\" do {\r\n"
        + "    ModName_ mt = target lnk;\r\n" // Eine Modulreferenz bekommen wir auch bei geschlossenem Modul
        + "    string mn = fullName( mt );\r\n" // Daraus den Modulpfad
        + "    Object ot = target lnk;\r\n" // Bei geschlossenem Modul bekommen wir hier null
        + "    if ( null == ot ) {\r\n" // Dann
        + "      Module mr = read( mn, false );\r\n" // Modul öffnen
        + "      ot = target lnk;\r\n" // Objekt nochmals holen
        + "    }\r\n"
        + "    if ( null != ot ) {\r\n" // Wenn immer noch null, haben wir keine Berechtigung, das Modul zu lesen
        + "      string absNum = ot.\"Absolute Number\";\r\n" // In dem Fall nichts tun
        + "      if (result != \"\") {\r\n"
        + "        result = result  \"|\";\r\n"
        + "      }\r\n"
        + "      result = result absNum \"@\" mn;\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getOutgoingLinksAndLinkModule( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "string result = \"\";\r\n"
        + "Link lnk;\r\n"
        + "for lnk in all o -> \"*\" do {\r\n"
        + "  Module linkModule = module( lnk );\r\n"
        + "  string linkModulePath = fullName( linkModule );\r\n"
        + "  ModName_ mt = target lnk;\r\n" // Eine Modulreferenz bekommen wir auch bei geschlossenem Modul
        + "  string mn = fullName( mt );\r\n" // Daraus den Modulpfad
        + "  Object ot = target lnk;\r\n" // Bei geschlossenem Modul bekommen wir hier null
        + "  if ( null == ot ) {\r\n" // Dann
        + "    Module mr = read( mn, false );\r\n" // Modul öffnen
        + "    ot = target lnk;\r\n" // Objekt nochmals holen
        + "  }\r\n"
        + "  if ( null != ot ) {\r\n" // Wenn immer noch null, haben wir keine Berechtigung, das Modul zu lesen
        + "    string absNum = ot.\"Absolute Number\";\r\n" // In dem Fall nichts tun
        + "    if (result != \"\") {\r\n"
        + "      result = result  \"|\";\r\n"
        + "    }\r\n"
        + "    result = result absNum \"@\" mn \"@\" linkModulePath;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  /**
   * Delete link from current to absoluteNumber in module modulepath
   *
   * @param modulePath     path (fullName) of target module
   * @param absoluteNumber absolute number of target object in target module
   * @return true if link was found and deleted, false otherwise
   */
  public boolean deleteLinkInDOORS( String modulePath, int absoluteNumber ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current m;\r\n"
        + "Module ml = read(\"" + modulePath + "\", false);\r\n"
        + "Object ol = object( " + absoluteNumber + ", ml );\r\n"
        + "Link lnk;\r\n"
        + "string result = \"false\";\r\n"
        + "if (o != null) {\r\n"
        + "  for lnk in o -> \"*\" do {\r\n"
        + "    Object ot = target lnk;\r\n"
        + "    if (ot == ol) {\n\r"
        + "      delete( lnk );\n\r"
        + "      result = \"true\";\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  /**
   * Delete link from element of sourceAbsNum in current module to targetAbsNum in targetModule
   *
   * @param sourceAbsNum absolute number of source object in current module
   * @param targetAbsNum absolute number of target object in target module
   * @param targetModule path (fullName) of target module
   * @return true if link was found and deleted, false otherwise
   */
  public boolean deleteLinkInDOORS( int sourceAbsNum, int targetAbsNum, String targetModule ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = object( " + sourceAbsNum + ", m );\r\n"
        + "Module ml = read(\"" + targetModule + "\", false);\r\n"
        + "Object ol = object( " + targetAbsNum + ", ml );\r\n"
        + "Link lnk;\r\n"
        + "string result = \"false\";\r\n"
        + "if (o != null) {\r\n"
        + "  if (ol != null) {\r\n"
        + "    for lnk in o -> \"*\" do {\r\n"
        + "      Object ot = target lnk;\r\n"
        + "      if (ot == ol) {\n\r"
        + "        delete( lnk );\n\r"
        + "        result = \"true\";\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  /**
   * Checks if link from current to absoluteNumber in module modulepath exists
   *
   * @param modulePath     path (fullName) of target module
   * @param absoluteNumber absolute number of target object in target module
   * @return true if link was found, false otherwise
   */
  public boolean existsLinkInDOORS( String modulePath, int absoluteNumber ) {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "Module ml = read(\"" + modulePath + "\", false);\r\n"
        + "Object ol = object( " + absoluteNumber + ", ml );\r\n"
        + "Link lnk;\r\n"
        + "string result = \"false\";\r\n"
        + "if (ol != null) {\r\n"
        + "  for lnk in o -> \"*\" do {\r\n"
        + "    Object ot = target lnk;\r\n"
        + "    if (ot == ol) {\n\r"
        + "      result = \"true\";\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  /**
   * Checks if link from object of sourceAbsNum in current module to targetAbsNum in targetModule exists
   *
   * @param sourceAbsNum absolute number of source object in current module
   * @param targetAbsNum absolute number of target object in target module
   * @param targetModule path (fullName) of target module
   * @return true if link was found, false otherwise
   */
  public boolean existsLinkInDOORS( int sourceAbsNum, int targetAbsNum, String targetModule ) {
    String dxl;
    dxl = readModule()
        + "Object o = object( " + sourceAbsNum + ", m );\r\n"
        + "Module ml = read(\"" + targetModule + "\", false);\r\n"
        + "Object ol = object( " + targetAbsNum + ", ml );\r\n"
        + "Link lnk;\r\n"
        + "string result = \"false\";\r\n"
        + "if (o != null) {\r\n"
        + "  if (ol != null) {\r\n"
        + "    for lnk in o -> \"*\" do {\r\n"
        + "      Object ot = target lnk;\r\n"
        + "      if (ot == ol) {\n\r"
        + "        result = \"true\";\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean createLinkInDOORS( String modulePath, int absoluteNumber ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current m;\r\n"
        + "Module ml = read(\"" + modulePath + "\", false);\r\n"
        + "Object ol = object( " + absoluteNumber + ", ml );\r\n"
        + "if (null ol) {\r\n"
        + "  oleSetResult( \"false\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  o -> ol;\r\n"
        + "  oleSetResult( \"true\" )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean createLinkInDOORSTest( String modulePath, int absoluteNumber ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current m;\r\n"
        + "Module ml = read(\"" + modulePath + "\", false);\r\n"
        + "Object ol = object( " + absoluteNumber + ", ml );\r\n"
        + "if (null ol) {\r\n"
        + "  oleSetResult( \"false\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  Link l = o -> ol;\r\n"
        + "  if ( null == l ) {;\r\n"
        + "    oleSetResult( \"false\" );\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    oleSetResult( \"true\" );\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  /**
   * Creates a link from object of sourceAbsNum in current module to targetAbsNum in targetModule
   *
   * @param sourceAbsNum absolute number of source object in current module
   * @param targetAbsNum absolute number of target object in target module
   * @param targetModule path (fullName) of target module
   * @return true if link was created, false otherwise
   */
  public boolean createLink( int sourceAbsNum, int targetAbsNum, String targetModule ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = object( " + sourceAbsNum + ", m );\r\n"
        + "Module ml = read(\"" + targetModule + "\", false);\r\n"
        + "Object ol = object( " + targetAbsNum + ", ml );\r\n"
        + "if (o != null) {\r\n"
        + "  if (null ol) {\r\n"
        + "    oleSetResult( \"false\" );\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    Link l = o -> ol;\r\n"
        + "    if ( null == l ) {;\r\n"
        + "      oleSetResult( \"false\" );\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      oleSetResult( \"true\" );\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

//  public String insertDiagramIntoDOORS( String eaLinkString, String timestamp, String objectType, String diagramName, boolean isBelow ) {
//    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
//      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
//    }
//    String dxl;
//    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
//        + "string EALink = \"" + eaLinkString + "\";\r\n"
//        + "string timeStamp = \"" + timestamp + "\";\r\n"
//        + "string diagramName = \"" + escapeIllegalChars( diagramName ) + "\";\r\n"
//        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
//        + "oleSetResult( \"false\" )\r\n"
//        + "Object o = current m;\r\n"
//        + "AttrDef ad = find(m, \"EALink\")\r\n"
//        + "if ( null ad ) {\r\n"
//        + "  oleSetResult( \"-1\" )\r\n"
//        + "}\r\n"
//        + "else {\r\n"
//        + "  Object newObject;\r\n"
//        + "  if ( " + isBelow + " ) {\r\n"
//        + "    newObject = create below o;\r\n"
//        + "  }\r\n"
//        + "  else {\r\n"
//        + "    newObject = create o;\r\n"
//        + "  }\r\n"
//        + "  newObject.\"Object Text\" = \"Diagram \'\" diagramName \"\', timestamp = \'\" timeStamp \"\'\"\r\n"
//        + "  newObject.\"EALink\" = EALink\r\n"
//        + "  newObject.\"Object Type\" = objType\r\n"
//        + commonDOORSAttributes
//        + "  olePaste(newObject)\r\n"
//        + "  oleSetResult( \"true\" )\r\n"
//        + "}\r\n"
//    ;
//    objDOORS.runStr(dxl);
//    return objDOORS.Result();
//  }

  public String insertDiagramIntoDOORSFromFile(  String eaLinkUrl, String timestamp, String objectType, String diagramName, String fileName ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "  newObject.\"Object Type\" = objType\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"true\";\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string timeStamp = \"" + timestamp + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "string diagramName = \"" + escapeIllegalChars( diagramName ) + "\";\r\n"
        + "string fileName = \"" + escapeIllegalChars( fileName ) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  Object newObject = o;\r\n"
        + "  newObject.\"Object Text\" = \"Diagram \'\" diagramName \"\', timestamp = \'\" timeStamp \"\'\\r\\n\"\r\n"
        + "  newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "  EmbeddedOleObject oleObject;\r\n"
        + "  RichText rtf;\r\n"
        + "  string s;\r\n"
        + "  int width = 0, height = 0;\r\n"
        + "  if ( oleInsert(newObject, fileName, false) ) {\r\n"
        + "    s = richTextWithOle newObject.\"Object Text\";\r\n"
        + "    width = 0; height = 0;\r\n"
        + "    for rtf in s do {\r\n"
        + "      if ( rtf.isOle ) {\r\n"
        + "        oleObject = rtf.getEmbeddedOle;\r\n"
        + "        getOleWidthHeight( oleObject, width, height );\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "    if ( width > 800 ) {\r\n"
        + "      height = 800 * height / width;\r\n"
        + "      width = 800;\r\n"
        + "      oleSetHeightandWidth( newObject.\"Object Text\", height, width, 0 );\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    result = \"false\";\r\n"
        + "  }\r\n"
        + "  oleSetResult( result )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String insertDiagramIntoDOORSFromFile( String eaLinkUrl, String timestamp, String objectType, String diagramName, String fileName, boolean isBelow ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "  newObject.\"Object Type\" = objType\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"true\";\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string timeStamp = \"" + timestamp + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "string diagramName = \"" + escapeIllegalChars( diagramName ) + "\";\r\n"
        + "string fileName = \"" + escapeIllegalChars( fileName ) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  Object newObject;\r\n"
        + "  if ( " + isBelow + " ) {\r\n"
        + "    newObject = create below o;\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    newObject = create o;\r\n"
        + "  }\r\n"
        + "  newObject.\"Object Text\" = \"Diagram \'\" diagramName \"\', timestamp = \'\" timeStamp \"\'\\r\\n\"\r\n"
        + "  newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "  EmbeddedOleObject oleObject;\r\n"
        + "  RichText rtf;\r\n"
        + "  string s;\r\n"
        + "  int width = 0, height = 0;\r\n"
        + "  if ( oleInsert(newObject, fileName, false) ) {\r\n"
        + "    s = richTextWithOle newObject.\"Object Text\";\r\n"
        + "    width = 0; height = 0;\r\n"
        + "    for rtf in s do {\r\n"
        + "      if ( rtf.isOle ) {\r\n"
        + "        oleObject = rtf.getEmbeddedOle;\r\n"
        + "        getOleWidthHeight( oleObject, width, height );\r\n"
        + "      }\r\n"
        + "    }\r\n"
//        + "    newObject.\"comment\" = width \"/\" height \"\";\r\n"
        + "    if ( width > 800 ) {\r\n"
        + "      height = 800 * height / width;\r\n"
        + "      width = 800;\r\n"
        + "      oleSetHeightandWidth( newObject.\"Object Text\", height, width, 0 );\r\n"
//        + "      newObject.\"comment\" = width \"/\" height \"\";\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    result = \"false\";\r\n"
        + "  }\r\n"
        + "  oleSetResult( result )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

//  public String updateDiagramInDOORS( String eaLinkString, String fullGuid, String timestamp, String objectType, String diagramName ) {
//    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
//      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
//    }
//    String guid = EALink.guidNoBrackets( fullGuid );
//    String dxl;
//    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
//        + "string EALink = \"" + eaLinkString + "\";\r\n"
//        + "string guid = \"" + guid + "\";\r\n"
//        + "string timeStamp = \"" + timestamp + "\";\r\n"
//        + "string diagramName = \"" + escapeIllegalChars( diagramName ) + "\";\r\n"
//        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
//        + "int offset = null\r\n"
//        + "int length = null\r\n"
//        + "oleSetResult( \"false\" )\r\n"
//        + "Object o = current m;\r\n"
//        + "AttrDef ad = find(m, \"EALink\")\r\n"
//        + "if ( null ad ) {\r\n"
//        + "  oleSetResult( \"-1\" )\r\n"
//        + "}\r\n"
//        + "else {\r\n"
//        + "  for o in entire(m) do {\r\n"
//        + "    if ( findPlainText( o.\"EALink\" \"\", guid, offset, length, true, false ) ) {\r\n"
//        + "      if ( !isVisible(o) ) {\r\n"
//        + "        oleSetResult( \"invisible\" )\r\n"
//        + "      }\r\n"
//        + "      else {\r\n"
//        + "        int an = o.\"Absolute Number\"\r\n"
//        + "        gotoObject( an, m )\r\n"
//        + "        o = current\r\n"
//        + "        o.\"Object Text\" = \"Diagram \'\" diagramName \"\', timestamp = \'\" timeStamp \"\'\"\r\n"
//        + "        o.\"EALink\" = EALink\r\n"
//        + "        o.\"Object Type\" = objType\r\n"
//        + "        olePaste(o)\r\n"
//        + "        oleSetResult( \"true\" )\r\n"
//        + "        break\r\n"
//        + "      }\r\n"
//        + "    }\r\n"
//        + "  }\r\n"
//        + "}\r\n"
//    ;
//    objDOORS.runStr(dxl);
//    return objDOORS.Result();
//  }

  public String updateDiagramInDOORSFromFile( String eaLinkUrl, String timestamp, String objectType, String diagramName, String fileName ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "  o.\"Object Type\" = objType\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"true\";\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string timeStamp = \"" + timestamp + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "string diagramName = \"" + escapeIllegalChars( diagramName ) + "\";\r\n"
        + "string fileName = \"" + escapeIllegalChars( fileName ) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  o.\"Object Text\" = \"Diagram \'\" diagramName \"\', timestamp = \'\" timeStamp \"\'\\r\\n\"\r\n"
        + "  o.\"EALink\" = EALink\r\n"
        + objTypeStr
        + "  EmbeddedOleObject oleObject;\r\n"
        + "  RichText rtf;\r\n"
        + "  string s;\r\n"
        + "  int width = 0, height = 0;\r\n"
        + "  if ( oleInsert( o, fileName, false ) ) {\r\n"
        + "    s = richTextWithOle o.\"Object Text\";\r\n"
        + "    width = 0; height = 0;\r\n"
        + "    for rtf in s do {\r\n"
        + "      if ( rtf.isOle ) {\r\n"
        + "        oleObject = rtf.getEmbeddedOle;\r\n"
        + "        getOleWidthHeight( oleObject, width, height );\r\n"
        + "      }\r\n"
        + "    }\r\n"
//        + "    o.\"comment\" = width \"/\" height \"\";\r\n"
        + "    if ( width > 800 ) {\r\n"
        + "      height = 800 * height / width;\r\n"
        + "      width = 800;\r\n"
        + "      oleSetHeightandWidth( o.\"Object Text\", height, width, 0 );\r\n"
//        + "      o.\"comment\" = width \"/\" height \"\";\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    result = \"false\";\r\n"
        + "  }\r\n"
        + "  oleSetResult( result )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String setDiagramInDOORSFromFile( int absNum, String diagramName, String timestamp, String fileName ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string result = \"true\";\r\n"
        + "string timeStamp = \"" + timestamp + "\";\r\n"
        + "string diagramName = \"" + escapeIllegalChars( diagramName ) + "\";\r\n"
        + "string fileName = \"" + escapeIllegalChars( fileName ) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = object(" + absNum + ");\r\n"
        + "if ( null o ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  o.\"Object Text\" = \"Diagram \'\" diagramName \"\', timestamp = \'\" timeStamp \"\'\\r\\n\"\r\n"
        + "  EmbeddedOleObject oleObject;\r\n"
        + "  RichText rtf;\r\n"
        + "  string s;\r\n"
        + "  int width = 0, height = 0;\r\n"
        + "  if ( oleInsert( o, fileName, false ) ) {\r\n"
        + "    s = richTextWithOle o.\"Object Text\";\r\n"
        + "    width = 0; height = 0;\r\n"
        + "    for rtf in s do {\r\n"
        + "      if ( rtf.isOle ) {\r\n"
        + "        oleObject = rtf.getEmbeddedOle;\r\n"
        + "        getOleWidthHeight( oleObject, width, height );\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "    if ( width > 800 ) {\r\n"
        + "      height = 800 * height / width;\r\n"
        + "      width = 800;\r\n"
        + "      oleSetHeightandWidth( o.\"Object Text\", height, width, 0 );\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    result = \"false\";\r\n"
        + "  }\r\n"
        + "  oleSetResult( result )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  private String getChildrenOfModule() {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object o;\r\n"
        + "for o in top( m ) do {\r\n"
        + "  if ( result != \"\" ) {\r\n"
        + "    result = result \",\"\r\n"
        + "  }\r\n"
        + "  result = result o.\"Absolute Number\" \"\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  private String getChildrenOfModuleWithoutTables() {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object o;\r\n"
        + "for o in top( m ) do {\r\n"
        + "  if ( !table( o ) ) {\r\n"
        + "    if ( !row( o ) ) {\r\n"
        + "      if ( !cell( o ) ) {\r\n"
        + "        if ( result != \"\" ) {\r\n"
        + "          result = result \",\"\r\n"
        + "        }\r\n"
        + "        result = result o.\"Absolute Number\" \"\";\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  private String getChildrenOfObject( int absNum ) {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object parent = object( " +absNum + " );\r\n"
        + "Object o;\r\n"
        + "if (parent != null){\r\n"
        + "  for o in parent do {\r\n"
        + "    if ( result != \"\" ) {\r\n"
        + "      result = result \",\"\r\n"
        + "    }\r\n"
        + "    result = result o.\"Absolute Number\" \"\";\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  private String getChildrenOfObjectWithoutTables( int absNum ) {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object parent = object( " +absNum + " );\r\n"
        + "Object o;\r\n"
        + "if (parent != null) {\r\n"
        + "  for o in parent do {\r\n"
        + "    if ( !table( o ) ) {\r\n"
        + "      if ( !row( o ) ) {\r\n"
        + "        if ( !cell( o ) ) {\r\n"
        + "          if ( result != \"\" ) {\r\n"
        + "            result = result \",\"\r\n"
        + "          }\r\n"
        + "          result = result o.\"Absolute Number\" \"\";\r\n"
        + "        }\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  private String getAllVisibleAndNotDeletedChildrenOfModule() {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object o;\r\n"
        + "for o in all( m ) do {\r\n"
        + "  if ( null != o ) {\r\n"
        + "    if ( isVisible( o ) && !isDeleted( o ) ) {\r\n"
        + "      if ( result != \"\" ) {\r\n"
        + "        result = result \",\"\r\n"
        + "      }\r\n"
        + "      result = result o.\"Absolute Number\" \"\";\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  private String getVisibleAndNotDeletedChildrenOfModule() {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object o;\r\n"
        + "for o in top( m ) do {\r\n"
        + "  if ( isVisible( o ) && !isDeleted( o ) ) {\r\n"
        + "    if ( result != \"\" ) {\r\n"
        + "      result = result \",\"\r\n"
        + "    }\r\n"
        + "    result = result o.\"Absolute Number\" \"\";\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  private String getVisibleAndNotDeletedChildrenOfObject( int absNum ) {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object parent = object( " +absNum + " );\r\n"
        + "Object o;\r\n"
        + "if (parent != null) {\r\n"
        + "  for o in parent do {\r\n"
        + "    if ( isVisible( o ) && !isDeleted( o ) ) {\r\n"
        + "      if ( result != \"\" ) {\r\n"
        + "        result = result \",\"\r\n"
        + "      }\r\n"
        + "      result = result o.\"Absolute Number\" \"\";\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String getVisibleAndNotDeletedChildrenOfAbsNum( int absNum ) {
    String resultString;
    if ( 0 == absNum ) {
      resultString = getVisibleAndNotDeletedChildrenOfModule();
    }
    else {
      resultString = getVisibleAndNotDeletedChildrenOfObject( absNum );
    }
    return resultString;
  }

  public String getAllVisibleAndNotDeletedChildrenOfAbsNum( int absNum ) {
    String resultString = getVisibleAndNotDeletedChildrenOfAbsNum( absNum );
    if ( !"".equals( resultString ) ) {
      String[] strings = resultString.split( "," );
      resultString = "";
      for (String string : strings) {
        if ( !"".equals( resultString ) ) {
          resultString += ",";
        }
        resultString += string;
        String children = getAllVisibleAndNotDeletedChildrenOfAbsNum( Integer.parseInt( string ) );
        if ( !"".equals( children ) ) {
          resultString += ",";
        }
        resultString += children;
      }
    }
    return resultString;
  }

  public List<Integer> getAllVisibleAndNotDeletedChildrenOfAbsNumList( int absNum ) {
    List<Integer> absNumList = new ArrayList<>();
    String resultString = getVisibleAndNotDeletedChildrenOfAbsNum( absNum );
    if ( !"".equals( resultString ) ) {
      List<Integer> foundNumsList = toIntList( Arrays.asList(resultString.split(",")) );
      absNumList.addAll(foundNumsList);
      for (Integer childNum : foundNumsList) {
        absNumList.addAll( getAllVisibleAndNotDeletedChildrenOfAbsNumList( childNum ));
      }
    }
    return absNumList;
  }

  public List<Integer> getAllVisibleAndNotDeletedChildrenOfAbsNumNestedList( int absNum ) {
    List<Integer> absNumList = new ArrayList<>();
    String resultString = getVisibleAndNotDeletedChildrenOfAbsNum( absNum );
    if ( !"".equals( resultString ) ) {
      List<Integer> foundNumsList = toIntList( Arrays.asList(resultString.split(",")) );
      for (Integer childNum : foundNumsList) {
        absNumList.add( childNum );
        absNumList.addAll( getAllVisibleAndNotDeletedChildrenOfAbsNumNestedList( childNum ));
      }
    }
    return absNumList;
  }


  public List<Integer> getVisibleAndNotDeletedChildrenOfAbsNumList( int absNum ) {
    List<Integer> absNumList = new ArrayList<>();
    String resultString = getVisibleAndNotDeletedChildrenOfAbsNum( absNum );
    if ( !"".equals( resultString ) ) {
      List<Integer> foundNumsList = toIntList( Arrays.asList(resultString.split(",")) );
      absNumList.addAll(foundNumsList);
    }
    return absNumList;
  }

  public List<Integer> toIntList( List<String> stringList ) {
    return stringList.stream().map(Integer::parseInt).collect( Collectors.toList());
  }

  public String getChildrenOfAbsNum( int absNum ) {
    String resultString;
    if ( 0 == absNum ) {
      resultString = getChildrenOfModule();
    }
    else {
      resultString = getChildrenOfObject( absNum );
    }
    return resultString;
  }

  public List<Integer> getChildrenAbsNumsOfAbsNum( int parentAbsNum ) {
    List<Integer> childList = new ArrayList<>();
    String result = getChildrenOfAbsNum( parentAbsNum );
    if (!Helper.isNullOrEmpty( result )) {
      String[] childArray = result.split( "," );
      for (String childString : childArray) {
        int absNum = Integer.parseInt( childString );
        childList.add( absNum );
      }
    }
    return childList;
  }

  public List<Integer> getFirstLevelChildren() {
    List<Integer> childList = new ArrayList<>();
    String result = getChildrenOfModule();
    if (!Helper.isNullOrEmpty( result )) {
      String[] childArray = result.split( "," );
      for (String childString : childArray) {
        int absNum = Integer.parseInt( childString );
        childList.add( absNum );
      }
    }
    return childList;
  }

  public String getChildrenOfAbsNumWithoutTables( int absNum ) {
    String resultString;
    if ( 0 == absNum ) {
      resultString = getChildrenOfModuleWithoutTables();
    }
    else {
      resultString = getChildrenOfObjectWithoutTables( absNum );
    }
    return resultString;
  }

  public String getAllChildrenOfAbsNum( int absNum ) {
    String resultString = getChildrenOfAbsNum( absNum );
    if ( !"".equals( resultString ) ) {
      String[] strings = resultString.split( "," );
      resultString = "";
      for (String string : strings) {
        if ( !"".equals( resultString ) ) {
          resultString += ",";
        }
        resultString += string;
        String children = getAllChildrenOfAbsNum( Integer.parseInt( string ) );
        if ( !"".equals( children ) ) {
          resultString += ",";
        }
        resultString += children;
      }
    }
    return resultString;
  }

  public String getAllChildrenOfAbsNumWithoutTables( int absNum ) {
    String resultString = getChildrenOfAbsNumWithoutTables( absNum );
    if ( !"".equals( resultString ) ) {
      String[] strings = resultString.split( "," );
      resultString = "";
      for (String string : strings) {
        if ( !"".equals( resultString ) ) {
          resultString += ",";
        }
        resultString += string;
        String children = getAllChildrenOfAbsNumWithoutTables( Integer.parseInt( string ) );
        if ( !"".equals( children ) ) {
          resultString += ",";
        }
        resultString += children;
      }
    }
    return resultString;
  }

  public String getNotDeletedChildrenOfAbsNum(int absNum){
    String resultString;
    if(0 == absNum){
      resultString = getNotDeletedChildrenOfModule();
    }
    else{
      resultString = getNotDeletedChildrenOfObject(absNum);
    }
    return resultString;
  }

  private String getNotDeletedChildrenOfObject( int absNum ) {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object parent = object( " +absNum + " );\r\n"
        + "Object o;\r\n"
        + "if (parent != null){\r\n"
        + "  for o in parent do {\r\n"
        + "    if ( !isDeleted( o ) ) {\r\n"
        + "      if ( result != \"\" ) {\r\n"
        + "        result = result \",\"\r\n"
        + "      }\r\n"
        + "      result = result o.\"Absolute Number\" \"\";\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  private String getNotDeletedChildrenOfModule() {
    String dxl;
    dxl = readModule()
        + "string result = \"\";\r\n"
        + "Object o;\r\n"
        + "for o in top( m ) do {\r\n"
        + "  if ( !isDeleted( o ) ) {\r\n"
        + "    if ( result != \"\" ) {\r\n"
        + "      result = result \",\"\r\n"
        + "    }\r\n"
        + "    result = result o.\"Absolute Number\" \"\";\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  // Moved code from DoorsHelper -------------------------------------------------------------------------

  public boolean isEaLinkMultiple(String fullGuid) {
    String guid = guidNoBrackets( fullGuid );
    String dxl;
    dxl = "bool isEaLinkMultiple(Module m, string guid) {\r\n"
        + "  int count = 0\r\n"
        + "  Object o\r\n"
        + "  int offset = null\r\n"
        + "  int length = null\r\n"
        + "  for o in entire(m) do {\r\n"
        + "    if ( !isDeleted( o ) ) {\r\n"
        + "      if ( findPlainText( o.\"EALink\" \"\", guid, offset, length, true, false ) ) {\r\n"
        + "        count++\r\n"
        + "        if (count > 1) {\r\n"
        + "          return true\r\n"
        + "        }\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return false\r\n"
        + "}\r\n"
        + readModule()
        + "string guid = \"" + guid + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  if ( isEaLinkMultiple( m, guid ) ) {\r\n"
        + "    oleSetResult( \"true\" )\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    oleSetResult( \"false\" )\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public String gotoGuidInDOORS( String fullGuid ) {
    String guid = guidNoBrackets( fullGuid );
    String dxl;
    dxl = readModule()
        + "string guid = \"" + guid + "\";\r\n"
        + "int offset = null\r\n"
        + "int length = null\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  for o in entire(m) do {\r\n"
        + "    if ( findPlainText( o.\"EALink\" \"\", guid, offset, length, true, false ) ) {\r\n"
        + "      if ( !isVisible(o) ) {\r\n"
        + "        oleSetResult( \"invisible\" )\r\n"
        + "      }\r\n"
        + "      else {\r\n"
        + "        int an = o.\"Absolute Number\"\r\n"
        + "        gotoObject( an, m )\r\n"
        + "        o = current\r\n"
        + "        oleSetResult( \"true\" )\r\n"
        + "        break\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String gotoChapterInDOORS( String chapterName ) {
    String dxl;
    dxl = readModule()
        + "string chapter = \"" + chapterName + "\";\r\n"
        + "int offset = null\r\n"
        + "int length = null\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"Object Heading\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  for o in entire(m) do {\r\n"
        + "    if ( findPlainText( o.\"Object Heading\" \"\", chapter, offset, length, true, false ) ) {\r\n"
        + "      if ( !isVisible(o) ) {\r\n"
        + "        oleSetResult( \"invisible\" )\r\n"
        + "      }\r\n"
        + "      else {\r\n"
        + "        int an = o.\"Absolute Number\"\r\n"
        + "        gotoObject( an, m )\r\n"
        + "        o = current\r\n"
        + "        oleSetResult( \"true\" )\r\n"
        + "        break\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String undeleteGuidInDOORS( String fullGuid ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String guid = guidNoBrackets( fullGuid );
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string guid = \"" + guid + "\";\r\n"
        + "int offset = null\r\n"
        + "int length = null\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  string result = \"\";\r\n"
        + "  for o in entire(m) do {\r\n"
        + "    if ( findPlainText( o.\"EALink\" \"\", guid, offset, length, true, false ) ) {\r\n"
        + "      if ( !isVisible(o) ) {\r\n"
        + "        result = undelete( o );\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  oleSetResult( result )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

// Only used in "InsertCurrentDiagramIntoDOORS":

  public boolean eaLinkExists( String fullGuid ) {
    String guid = guidNoBrackets( fullGuid );
    String dxl;
    dxl = "bool eaLinkExists(Module m, string guid) {\r\n"
        + "  Object o\r\n"
        + "  int offset = null\r\n"
        + "  int length = null\r\n"
        + "  for o in entire(m) do {\r\n"
        + "    if ( !isDeleted( o ) ) {\r\n"
        + "      if ( findPlainText( o.\"EALink\" \"\", guid, offset, length, true, false ) ) {\r\n"
        + "        return true\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return false\r\n"
        + "}\r\n"
        + readModule()
        + "string guid = \"" + guid + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  if ( eaLinkExists( m, guid ) ) {\r\n"
        + "    oleSetResult( \"true\" )\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    oleSetResult( \"false\" )\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return Boolean.parseBoolean( objDOORS.Result() );
  }

// Only used in "InsertUpdateStateMachine":

  public String insertDiagramIntoDOORSWithText( String eaLinkString, String timestamp, String objectType, String diagramName, boolean isBelow, String notes ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "  newObject.\"Object Type\" = objType\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkString + "\";\r\n"
        + "string timeStamp = \"" + timestamp + "\";\r\n"
        + "string diagramName = \"" + escapeIllegalChars( diagramName ) + "\";\r\n"
        + "string notes = \"" + escapeIllegalChars( notes ) + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  Object newObject;\r\n"
        + "  if ( " + isBelow + " ) {\r\n"
        + "    newObject = create below o;\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    newObject = create o;\r\n"
        + "  }\r\n"
        + "  newObject.\"Object Text\" = \"Diagram \'\" diagramName \"\', timestamp = \'\" timeStamp \"\'\\r\\n\" notes \"\"\r\n"
        + "  newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "  olePaste(newObject)\r\n"
        + "  oleSetResult( \"true\" )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String updateDiagramInDOORSWithText( String eaLinkString, String timestamp, String objectType, String diagramName, String notes ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "o.\"Object Type\" = objType\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current m;\r\n"
        + "string EALink = \"" + eaLinkString + "\";\r\n"
        + "string timeStamp = \"" + timestamp + "\";\r\n"
        + "string diagramName = \"" + escapeIllegalChars( diagramName ) + "\";\r\n"
        + "string notes = \"" + escapeIllegalChars( notes ) + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "o.\"Object Text\" = \"Diagram \'\" diagramName \"\', timestamp = \'\" timeStamp \"\'\\r\\n\" notes \"\"\r\n"
        + "o.\"EALink\" = EALink\r\n"
        + objTypeStr
        + "olePaste(o)\r\n"
        + "oleSetResult( \"true\" )\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String insertHeadingSameNT( String text, boolean setObjectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (setObjectType) {
      objTypeStr = "newObject.\"Object Type\" = \"heading\"\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "Object o = current m;\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "Object newObject = create o\r\n"
        + "newObject.\"Object Heading\" = EAText\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "oleSetResult( \"true\" )\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public boolean openInLinkModules() {
    String dxl;
    dxl = readModule()
        + "Object o = current m;\r\n"
        + "Module linkMod;\r\n"
        + "ModName_ srcModRef;\r\n"
        + "string result = \"true\";\r\n"
        + "for srcModRef in o<-\"*\" do {\r\n"
        + "  linkMod = read( fullName( srcModRef ), false );\r\n"
        + "  if ( null == linkMod ) {\r\n"
        + "    result = \"false\";\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean openInLinkModules( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "Module linkMod;\r\n"
        + "ModName_ srcModRef;\r\n"
        + "string result = \"true\";\r\n"
        + "if (o != null) {\r\n"
        + "  for srcModRef in o <- \"*\" do {\r\n"
        + "    linkMod = read( fullName( srcModRef ), false );\r\n"
        + "    if ( null == linkMod ) {\r\n"
        + "      result = \"false\";\r\n"
        + "      break;\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean existsIncomingLinkInDOORS( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "Link lnk;\r\n"
        + "string result = \"false\";\r\n"
        + "if (o != null) {\r\n"
        + "  for lnk in o <- \"*\" do {\r\n"
        + "    result = \"true\";\r\n"
        + "    break;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public String getSourceAbsNums( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "Link lnk;\r\n"
        + "string result = \"\";\r\n"
        + "if (o != null) {\r\n"
        + "  for lnk in o <- \"*\" do {\r\n"
        + "    Object os = source lnk;\r\n"
        + "    string sourceAbsNum = os.\"Absolute Number\";\r\n"
        + "    if ( result != \"\" ) {\r\n"
        + "      result = result \",\";\r\n"
        + "    }\r\n"
        + "    result = result sourceAbsNum \"\";\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getIncomingLinks( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "string result = \"\";\r\n"
        + "Link lnk;\r\n"
        + "if (o != null) {\r\n"
        + "  for lnk in o <- \"*\" do {\r\n"
        + "    ModName_ ms = source lnk;\r\n"
        + "    string mn = fullName( ms );\r\n"
        + "    Object os = source lnk;\r\n"
        + "    string absNum = os.\"Absolute Number\";\r\n"
        + "    if ( result != \"\" ) {\r\n"
        + "      result = result \"|\";\r\n"
        + "    }\r\n"
        + "    result = result absNum \"@\" mn;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getIncomingLinksAndLinkModules( int absNum ) {
    String dxl;
    dxl = readModule()
        + "Object o = object(" + absNum + ");\r\n"
        + "string result = \"\";\r\n"
        + "Link lnk;\r\n"
        + "if (o != null) {\r\n"
        + "  for lnk in o <- \"*\" do {\r\n"
        + "    ModName_ ms = source lnk;\r\n"
        + "    string mn = fullName( ms );\r\n"
        + "    Module linkModule = module( lnk );\r\n"
        + "    string linkModulePath = fullName( linkModule );\r\n"
        + "    Object os = source lnk;\r\n"
        + "    string absNum = os.\"Absolute Number\";\r\n"
        + "    if ( result != \"\" ) {\r\n"
        + "      result = result \"|\";\r\n"
        + "    }\r\n"
        + "    result = result absNum \"@\" mn \"@\" linkModulePath;\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String updateRichTextInDOORS( String text, String fullGuid, String eaLinkUrl, String objectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String guid = guidNoBrackets( fullGuid );
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "        string tmp2 = o.\"Object Type\";"
          + "        if (tmp2 != objType) {\r\n"
          + "          o.\"Object Type\" = objType\r\n"
          + "        }\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string guid = \"" + guid + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "int offset = null\r\n"
        + "int length = null\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  for o in entire(m) do {\r\n"
        + "    if ( findPlainText( o.\"EALink\" \"\", guid, offset, length, true, false ) ) {\r\n"
        + "      if ( !isVisible(o) ) {\r\n"
        + "        oleSetResult( \"invisible\" )\r\n"
        + "      }\r\n"
        + "      else {\r\n"
        + "        int an = o.\"Absolute Number\"\r\n"
        + "        gotoObject( an, m )\r\n"
        + "        o = current;\r\n"
        + "        string tmp = richText( o.\"Object Text\");\r\n"
        + "        string txt = richText( removeUnlistedRichText( EAText ) );\r\n"
        + "        if ( tmp != txt ) {\r\n"
        + "          o.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
        + "          o.\"EALink\" = EALink\r\n"
        + "        }\r\n"
        + objTypeStr
        + "        break\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String insertRichTextIntoDOORS(String text, String eaLinkUrl, String objectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "      newObject.\"Object Type\" = objType\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  if ( length(o.\"Object Heading\" \"\") != 0 ) {\r\n"
        + "    Object newObject = create below o\r\n"
        + "    if ( ad.canWrite ) {\r\n"
        + "      newObject.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
        + "      newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "      oleSetResult( \"true\" )\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    if ( length(o.\"Object Text\" \"\") != 0 ) {\r\n"
        + "      Object newObject = create(o)\r\n"
        + "      if ( ad.canWrite ) {\r\n"
        + "        newObject.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
        + "        newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "        oleSetResult( \"true\" )\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      if ( ad.canWrite ) {\r\n"
        + "        Object newObject = o\r\n"
        + "        newObject.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
        + "        newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "        oleSetResult( \"true\" )\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String updateHeadingInDOORS(String text, String fullGuid, String eaLinkUrl, boolean setObjectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String guid = guidNoBrackets( fullGuid );
    String objTypeStr = "";
    if (setObjectType) {
      objTypeStr = "        o.\"Object Type\" = \"heading\"\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string guid = \"" + guid + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "int offset = null\r\n"
        + "int length = null\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  for o in entire(m) do {\r\n"
        + "    if ( findPlainText( o.\"EALink\" \"\", guid, offset, length, true, false ) ) {\r\n"
        + "      if ( !isVisible(o) ) {\r\n"
        + "        oleSetResult( \"invisible\" )\r\n"
        + "      }\r\n"
        + "      else {\r\n"
        + "        int an = o.\"Absolute Number\"\r\n"
        + "        gotoObject( an, m )\r\n"
        + "        o = current\r\n"
        + "        o.\"Object Heading\" = EAText\r\n"
        + "        o.\"EALink\" = EALink\r\n"
        + objTypeStr
        + "        break\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String insertHeadingIntoDOORSBelow( String text, String eaLinkUrl, boolean setObjectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String dxl;
    String objTypeStr = "";
    if (setObjectType) {
      objTypeStr = "      newObject.\"Object Type\" = \"heading\"\r\n";
    }
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  if ( length(o.\"Object Heading\" \"\") == 0 ) {\r\n"
        + "    if ( ad.canWrite ) {\r\n"
        + "      Object newObject = create(o)\r\n"
        + "      newObject.\"Object Heading\" = EAText\r\n"
        + "      newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "      oleSetResult( \"true\" )\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    if ( ad.canWrite ) {\r\n"
        + "      Object newObject = create below o\r\n"
        + "      newObject.\"Object Heading\" = EAText\r\n"
        + "      newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "      oleSetResult( \"true\" )\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

// Unused methods:

// Tries to compute integer value from string.
  public int getAbsNum( String value ) {
    if (value == null) {
      return 0;
    }
    try {
      return Integer.parseInt( value );
    }
    catch (NumberFormatException ex) {
      return 0;
    }
  }

  public String updateTextInDOORS(String text, String fullGuid, String eaLinkUrl, String objectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String guid = guidNoBrackets( fullGuid );
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "        string tmp2 = o.\"Object Type\";"
          + "        if (tmp2 != objType) {\r\n"
          + "          o.\"Object Type\" = objType\r\n"
          + "        }\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string guid = \"" + guid + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "int offset = null\r\n"
        + "int length = null\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  for o in entire(m) do {\r\n"
        + "    if ( findPlainText( o.\"EALink\" \"\", guid, offset, length, true, false ) ) {\r\n"
        + "      if ( !isVisible(o) ) {\r\n"
        + "        oleSetResult( \"invisible\" )\r\n"
        + "      }\r\n"
        + "      else {\r\n"
        + "        int an = o.\"Absolute Number\"\r\n"
        + "        gotoObject( an, m )\r\n"
        + "        o = current;\r\n"
        + "        string tmp = o.\"Object Text\";\r\n"
        + "        if ( tmp != EAText ) {\r\n"
        + "          o.\"Object Text\" = EAText\r\n"
        + "          o.\"EALink\" = EALink\r\n"
        + "        }\r\n"
        + objTypeStr
        + "        break\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String insertRichTextIntoDOORSBelow(String text, String eaLinkUrl, String objectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "    newObject.\"Object Type\" = objType\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  Object newObject = create below o\r\n"
        + "  if ( ad.canWrite ) {\r\n"
        + "    newObject.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
        + "    newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "    oleSetResult( \"true\" )\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String insertRichTextIntoDOORSSame( String text, String eaLinkUrl, String objectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "    newObject.\"Object Type\" = objType\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  Object newObject = create(o)\r\n"
        + "  if ( ad.canWrite ) {\r\n"
        + "    newObject.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
        + "    newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "    oleSetResult( \"true\" )\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public int insertRichTextIntoDOORSSame(int prevAbsNum, String text) {
    if (!this.baseLineString.equals("") || !this.isInEditMode) {
      throw new IllegalArgumentException(READ_ONLY_OR_BASELINE_EXCEPTION);
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
            + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
            + "Object o = object(" + prevAbsNum + ");\r\n"
            + "if (o == null){\r\n"
            + "  oleSetResult( \"-1\" )\r\n"
            + "}\r\n"
            + "else{\r\n"
            + "  Object newObject = create(o)\r\n"
            + "  newObject.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
            + "  string result = newObject.\"Absolute Number\";\r\n"
            + "  oleSetResult( result )\r\n"
            + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return Integer.parseInt(objDOORS.Result());
  }

  public int insertRichTextIntoDOORSBelow(int parentAbsNum, String text) {
    if(parentAbsNum == 0) {
      return insertTopLevelObject( text );
    }
    else {
      if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
        throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
      }
      String dxl;
      dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
          + "string EAText = \"" + escapeIllegalChars( text ) + "\";\r\n"
          + "Object o = object(" + parentAbsNum + ");\r\n"
          + "if (o == null){\r\n"
          + "  oleSetResult( \"-1\" )\r\n"
          + "}\r\n"
          + "else{\r\n"
          + "  Object newObject = create below o\r\n"
          + "  newObject.\"Object Text\" = richText( removeUnlistedRichText(EAText) )\r\n"
          + "  string result = newObject.\"Absolute Number\";\r\n"
          + "  oleSetResult( result )\r\n"
          + "}\r\n"
      ;
      objDOORS.runStr( dxl );
      return Integer.parseInt( objDOORS.Result() );
    }
  }

  public String insertTextIntoDOORS( String text, String eaLinkUrl, String objectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (objectType != null) {
      objTypeStr = "      newObject.\"Object Type\" = objType\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string objType = \"" + escapeIllegalChars( objectType ) + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  if ( length(o.\"Object Heading\" \"\") != 0 ) {\r\n"
        + "    Object newObject = create below o\r\n"
        + "    if ( ad.canWrite ) {\r\n"
        + "      newObject.\"Object Text\" = EAText\r\n"
        + "      newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "      oleSetResult( \"true\" )\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    if ( length(o.\"Object Text\" \"\") != 0 ) {\r\n"
        + "      Object newObject = create(o)\r\n"
        + "      if ( ad.canWrite ) {\r\n"
        + "        newObject.\"Object Text\" = EAText\r\n"
        + "        newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "        oleSetResult( \"true\" )\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      if ( ad.canWrite ) {\r\n"
        + "        Object newObject = o\r\n"
        + "        newObject.\"Object Text\" = EAText\r\n"
        + "        newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "        oleSetResult( \"true\" )\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String insertHeadingIntoDOORS(String text, String eaLinkUrl, boolean setObjectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (setObjectType) {
      objTypeStr = "    newObject.\"Object Type\" = \"heading\"\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  if ( length(o.\"Object Heading\" \"\") == 0 ) {\r\n"
        + "    o = parent o\r\n"
        + "  }\r\n"
        + "  if ( ad.canWrite ) {\r\n"
        + "    Object newObject = create(o)\r\n"
        + "    newObject.\"Object Heading\" = EAText\r\n"
        + "    newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "    oleSetResult( \"true\" )\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String insertHeadingIntoDOORSSame( String text, String eaLinkUrl, boolean setObjectType ) {
    if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
      throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
    }
    String objTypeStr = "";
    if (setObjectType) {
      objTypeStr = "    newObject.\"Object Type\" = \"heading\"\r\n";
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
        + "string EALink = \"" + eaLinkUrl + "\";\r\n"
        + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
        + "oleSetResult( \"false\" )\r\n"
        + "Object o = current m;\r\n"
        + "AttrDef ad = find(m, \"EALink\")\r\n"
        + "if ( null ad ) {\r\n"
        + "  oleSetResult( \"-1\" )\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  if ( ad.canWrite ) {\r\n"
        + "    Object newObject = create(o)\r\n"
        + "    newObject.\"Object Heading\" = EAText\r\n"
        + "    newObject.\"EALink\" = EALink\r\n"
        + objTypeStr
        + commonDOORSAttributes
        + "    oleSetResult( \"true\" )\r\n"
        + "  }\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public int insertHeadingIntoDOORSSame(int prevAbsNum, String text) {
    if (!this.baseLineString.equals("") || !this.isInEditMode) {
      throw new IllegalArgumentException(READ_ONLY_OR_BASELINE_EXCEPTION);
    }
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
            + "string EAText = \"" + escapeIllegalChars(text) + "\";\r\n"
            + "Object o = object(" + prevAbsNum + ");\r\n"
            + "if (o == null){\r\n"
            + "  oleSetResult( \"-1\" )\r\n"
            + "}\r\n"
            + "else{\r\n"
            + "  Object newObject = create(o)\r\n"
            + "  newObject.\"Object Heading\" = EAText\r\n"
            + "  string result = newObject.\"Absolute Number\";\r\n"
            + "  oleSetResult( result )\r\n"
            + "}\r\n"
    ;
    objDOORS.runStr(dxl);
    return Integer.parseInt(objDOORS.Result());
  }

  public int insertHeadingIntoDOORSBelow(int parentAbsNum, String text) {
    if(parentAbsNum == 0 ){
      return insertTopLevelHeading( text );
    }

    else {
      if (!this.baseLineString.equals( "" ) || !this.isInEditMode) {
        throw new IllegalArgumentException( READ_ONLY_OR_BASELINE_EXCEPTION );
      }
      String dxl;
      dxl = "Module m = edit(\"" + fullModulePath + "\", false);\r\n"
          + "string EAText = \"" + escapeIllegalChars( text ) + "\";\r\n"
          + "Object o = object(" + parentAbsNum + ");\r\n"
          + "if (o == null){\r\n"
          + "  oleSetResult( \"-1\" )\r\n"
          + "}\r\n"
          + "else{\r\n"
          + "  Object newObject = create below o;\r\n"
          + "  newObject.\"Object Heading\" = EAText\r\n"
          + "  string result = newObject.\"Absolute Number\";\r\n"
          + "  oleSetResult( result )\r\n"
          + "}\r\n"
      ;
      objDOORS.runStr( dxl );
      return Integer.parseInt( objDOORS.Result() );
    }
  }

  private void cacheSourceIds( List<String> duplicatesList ) {
    cachedSourceIds = new HashMap<>(  );
    String cachedSourceIdsString = getAllSourceIds();
    if(!Helper.isNullOrEmpty( cachedSourceIdsString )){
      String[] objectRows = cachedSourceIdsString.split( DoorsModule.SPLIT_STRING );
      for(String objectRow : objectRows){
        String[] row = objectRow.split( "@" );
        if(row.length == 2) {
          String sourceId = row[0];
          String absNumString = row[1];
          int absNum = getAbsNum( absNumString );
          if(!Helper.isNullOrEmpty( sourceId ) && !sourceId.equals( "new" )) {
            if(cachedSourceIds.containsKey( sourceId )){
              List<Integer> absNums = cachedSourceIds.get( sourceId );
              if(absNums.size() == 1){
                Logger.info( "found duplicate sourceID '"+sourceId+"' in Module during caching." );
                duplicatesList.add( "Found duplicate SourceID '" + sourceId + "' in Module (maybe in deleted objects)." );
              }
              absNums.add(absNum);
            }
            else{
              List<Integer> absNums = new ArrayList<>(  );
              absNums.add( absNum );
              cachedSourceIds.put( sourceId, absNums );
            }
          }
        }
      }
    }
  }

  public int getAbsNumBySourceID( String sourceID, List<String> duplicatesList ) {
    if (cachedSourceIds == null) {
      cacheSourceIds( duplicatesList );
    }

    List<Integer> absNums = cachedSourceIds.get( sourceID );
    if(absNums == null || absNums.size() == 0 ){
      //sourceId does neither exist nor is deleted
      Logger.info( "No absolute number found to sourceID '"+sourceID+"'" );
      return -1;
    }
    if(absNums.size() == 1){
      //sourceId found
      Logger.info( "absolute number found to sourceID '"+sourceID+"'" );
      return absNums.get( 0 );
    }
    //multiple absNums with source id found:
    //if exactly one absNum is NOT DELETED, use this,
    //otherwise it is an fatal error: Module contains duplicate sourceId that can not be interpreted correctly!
    int existingCnt = 0;
    int existingAbsNum = -1;
    for (Integer absNum : absNums) {
      if (!isObjectDeleted( absNum )) {
        existingAbsNum = absNum;
        existingCnt++;
      }
    }
    if(existingCnt == 1){
      Logger.info( "queried duplicate sourceID '"+sourceID+"' used the only one that is not deleted" );
      return existingAbsNum;
    }
    throw new RuntimeException( "Doors Module contains duplicate Source ID: " + sourceID + "\n Export might have already changed your module. \n Please check consistency!" );
  }
}