/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.doors.api;

import de.pidata.doors.api.DoorsModule;

public class DoorsException extends RuntimeException {

  private String guid;
  private int absoluteNumber = 0;
  private String modulePath;
  private String umlType;

  public DoorsException( String guid, String umlType, int absoluteNumber, DoorsModule doorsModule, String message, Throwable cause ){
    super(message, cause);
    this.guid = guid;
    if(absoluteNumber > 0) {
      this.absoluteNumber = absoluteNumber;
    }
    this.modulePath = doorsModule.getFullModulePath();
    this.umlType = umlType;
  }

  public DoorsException( String guid, String umlType, int absoluteNumber, DoorsModule doorsModule, String message ){
    super(message);
    this.guid = guid;
    if(absoluteNumber > 0) {
      this.absoluteNumber = absoluteNumber;
    }
    this.modulePath = doorsModule.getFullModulePath();
    this.umlType = umlType;
  }

  public String getGuid() {
    return guid;
  }

  public int getAbsoluteNumber(){
    return this.absoluteNumber;
  }

  public String getModulePath(){
    return this.modulePath;
  }

  public String getUmlType() {
    return umlType;
  }
}
