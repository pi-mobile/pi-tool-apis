/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.doors.api;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComFailException;
import com.jacob.com.Dispatch;
import de.pidata.log.Logger;

public class Doors {

  private ActiveXComponent doorsActiveX;
  private DoorsExplorer doorsExplorer;

  public Doors() throws InstantiationException {
    try {
      doorsActiveX = new ActiveXComponent( "DOORS.Application" );
      doorsExplorer = new DoorsExplorer( this );
    }
    catch (ComFailException ex ) {
      Logger.error( "Error connecting to DOORS", ex );
      throw new InstantiationException( "DOORS was not found or opened on this machine.\n\n" + ex.getMessage() );
    }
  }

  public Doors(ActiveXComponent activeXComponent){
    this.doorsActiveX = activeXComponent;
    this.doorsExplorer = new DoorsExplorer( this );
  }

  public DoorsExplorer getDoorsExplorer() {
    return doorsExplorer;
  }

  public String runStr( String doorsCommand ) {
    String noTimeoutHeader = "pragma runLim,0;\r\n";
    doorsCommand = noTimeoutHeader + doorsCommand;
    return Dispatch.call( doorsActiveX, "runStr", doorsCommand ).getString();
  }

  public String Result() {
    return Dispatch.call( doorsActiveX, "Result" ).getString();
  }
}
