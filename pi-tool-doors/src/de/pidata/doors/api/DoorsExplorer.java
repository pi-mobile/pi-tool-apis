/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.doors.api;

import de.pidata.string.Helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DoorsExplorer {

  private Doors objDOORS;
  private int countOpenEditModules, countOpenReadonlyModules, countOpenVisibleModules;
//  private String fullModulePath;

  public DoorsExplorer( Doors objDOORS ) {
    this.objDOORS = objDOORS;
    this.countOpenEditModules = countOpenEditModules();
    this.countOpenReadonlyModules = countOpenReadonlyModules();
    this.countOpenVisibleModules = countOpenVisibleModules();
  }

  public Doors getDOORS() {
    return objDOORS;
  }

  public int getCountOpenEditModules() {
    return this.countOpenEditModules;
  }

  public int getCountOpenReadonlyModules() {
    return this.countOpenReadonlyModules;
  }

  public int getCountOpenVisibleModules() {
    return this.countOpenVisibleModules;
  }

  public String getCurrentFolderFullPath() {
    String dxl;
    dxl = "Folder f = current();\r\n"
        + "string result = fullName( f );\r\n"
        + "oleSetResult( result \"\" );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getItemsFromFolder( String folderFullPath ) {
    String dxl;
    dxl = "Folder f = folder( \"" + folderFullPath + "\" );\r\n"
        + "string result = \"\";\r\n"
        + "Item i;\r\n"
        + "for i in f do {\r\n"
        + "  if ( \"\" != result ) {\r\n"
        + "    result = result \"|\";\r\n"
        + "  };\r\n"
        + "  result = result + name( i );\r\n"
        + "}\r\n"
        + "oleSetResult( result \"\" );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public boolean isItemFolder( String itemFullPath ) {
    String dxl;
    dxl = "Item i = item( \"" + itemFullPath + "\" );\r\n"
        + "string result = \"false\";\r\n"
        + "if ( \"Folder\" == type( i ) ) {\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result \"\" );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean isItemModule( String itemFullPath ) {
    String dxl;
    dxl = "Item i = item( \"" + itemFullPath + "\" );\r\n"
        + "string result = \"false\";\r\n"
        + "if ( \"Formal\" == type( i ) ) {\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result \"\" );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public String getTypeOfItem( String itemFullPath ) {
    String dxl;
    dxl = "Item i = item( \"" + itemFullPath + "\" );\r\n"
        + "string result = type( i );\r\n"
        + "oleSetResult( result \"\" );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getDescriptionOfItem( String itemFullPath ) {
    String dxl;
    dxl = "Item i = item( \"" + itemFullPath + "\" );\r\n"
        + "string result = description( i );\r\n"
        + "oleSetResult( result \"\" );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getUrlOfItem( String itemFullPath ) {
    String dxl;
    dxl = "Item i = item( \"" + itemFullPath + "\" );\r\n"
        + "string result = getURL( i );\r\n"
        + "oleSetResult( result \"\" );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public boolean existsModule( String fullModulePath ) {
    String dxl;
    dxl = "string result = \"false\"; \r\n"
        + "string path = \"" + fullModulePath + "\"; \r\n"
        + "if (exists module path ){ \r\n"
        + " result = \"true\"; \r\n"
        + "} \r\n"
        + "oleSetResult( result );"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean openModuleRead( String fullModulePath ) {
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", true);\r\n"
        + "string result;\r\n"
        + "if ( null == m ) {\r\n"
        + "  result = \"false\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean openModuleWrite( String fullModulePath ) {
    String dxl;
    dxl = "Module m = edit(\"" + fullModulePath + "\", true);\r\n"
        + "string result;\r\n"
        + "if ( null == m ) {\r\n"
        + "  result = \"false\";\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  result = \"true\";\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  /**
   * returns true if the user can write the module
   * @param fullModulePath
   * @return
   */
  public boolean isModuleWritable(String fullModulePath){
    String dxl;
    dxl = "Module m = read(\"" + fullModulePath + "\", true);\r\n"
        + "string result;\r\n"
        + "  if (canWrite(m) ) {\r\n"
        + "    result = \"true\";\r\n"
        + "  }\r\n"
        + "  else {\r\n"
        + "    result = \"false\";\r\n"
        + "  }\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public String currentModule() {
    String dxl;
    dxl = "Module m = (current Module);\r\n"
        + "string modulePath = fullName( m );\r\n"
        + "oleSetResult( modulePath \"\")\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  private int countOpenEditModules() {
    String dxl;
    dxl = "int countOpenEditModules() {\r\n"
        + "  int count=0\r\n"
        + "  Module m\r\n"
        + "  for m in database do {\r\n"
        + "    if ( isEdit m ) {\r\n"
        + "      count++\r\n"
        + "    }\r\n"
        + "    if ( isShare m ) { \r\n"
        + "      current = m\r\n"
        + "      Object o = current m\r\n"
        + "      if ( isLockedByUser o ) { \r\n"
        + "        count++\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return count\r\n"
        + "}\r\n"
        + "oleSetResult( countOpenEditModules() \"\")\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  private int countOpenReadonlyModules() {
    String dxl;
    dxl = "int countOpenReadonlyModules() {\r\n"
        + "  int count=0\r\n"
        + "  Module m\r\n"
        + "  for m in database do {\r\n"
        + "    if ( isRead m ) {\r\n"
        + "      if ( isVisible m ) {\r\n"
        + "        count++\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return count\r\n"
        + "}\r\n"
        + "oleSetResult( countOpenReadonlyModules() \"\")\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  private int countOpenVisibleModules() {
    String dxl;
    dxl = "int countOpenVisibleModules() {\r\n"
        + "  int count=0\r\n"
        + "  Module m\r\n"
        + "  for m in database do {\r\n"
        + "    if ( isVisible m ) {\r\n"
        + "      count++\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return count\r\n"
        + "}\r\n"
        + "oleSetResult( countOpenVisibleModules() \"\")\r\n"
    ;
    objDOORS.runStr( dxl );
    return Integer.parseInt( objDOORS.Result() );
  }

  public String listModules() {
    String dxl;
    dxl = "string listModules() {\r\n"
        + "  int count=0\r\n"
        + "  Module m\r\n"
        + "  string result = \"\"\r\n"
        + "  for m in database do {\r\n"
        + "    if ( isRead m ) {\r\n"
        + "      result = result \"R\"\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      result = result \"W\"\r\n"
        + "    }\r\n"
        + "    if ( isVisible m ) {\r\n"
        + "      result = result \"V\"\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      result = result \"-\"\r\n"
        + "    }\r\n"
        + "    result = result fullName(m) \" \"\r\n"
        + "    Baseline b = baselineInfo( m )\r\n"
        + "    if ( b == null ) {\r\n"
        + "      result = result \"current\"\r\n"
        + "    }\r\n"
        + "    else {\r\n"
        + "      result = result major(b) \".\" minor(b) \"\"\r\n"
        + "    }\r\n"
        + "    result = result \" | \"\r\n"
        + "  }\r\n"
        + "  return result\r\n"
        + "}\r\n"
        + "oleSetResult( listModules() \"\" )\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }


  private String firstCurrentEditModule() {
    String dxl;
    dxl = "Module firstCurrentEditModule() {\r\n"
        + "  Module m\r\n"
        + "  for m in database do {\r\n"
        + "    if ( isEdit m ) {\r\n"
        + "      return m\r\n"
        + "    }\r\n"
        + "    if ( isShare m ) { \r\n"
        + "      current = m\r\n"
        + "      Object o = current m\r\n"
        + "      if ( isLockedByUser o ) { \r\n"
        + "        return m\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return null\r\n"
        + "}\r\n"
        + "Module m = firstCurrentEditModule();\r\n"
        + "if ( m == null ) {\r\n"
        + "  oleSetResult( \"\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( fullName( m ) )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public boolean firstCurrentEditModuleIsBaseline() {
    String dxl;
    dxl = "Module firstCurrentEditModule() {\r\n"
        + "  Module m\r\n"
        + "  for m in database do {\r\n"
        + "    if ( isEdit m ) {\r\n"
        + "      return m\r\n"
        + "    }\r\n"
        + "    if ( isShare m ) { \r\n"
        + "      current = m\r\n"
        + "      Object o = current m\r\n"
        + "      if ( isLockedByUser o ) { \r\n"
        + "        return m\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return null\r\n"
        + "}\r\n"
        + "Module m = firstCurrentEditModule();\r\n"
        + "if ( baseline( m ) ) {\r\n"
        + "  oleSetResult( \"true\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"false\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  private String firstCurrentReadonlyModule() {
    String dxl;
    dxl = "Module firstCurrentModuleReadOnly() {\r\n"
        + "  Module m\r\n"
        + "  for m in database do {\r\n"
        + "    if ( isRead m ) {\r\n"
        + "      if ( isVisible m ) {\r\n"
        + "        return m\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return null\r\n"
        + "}\r\n"
        + "Module m = firstCurrentReadonlyModule();\r\n"
        + "if ( m == null ) {\r\n"
        + "  oleSetResult( \"\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( fullName( m ) )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  private boolean firstCurrentReadonlyModuleIsBaseline() {
    String dxl;
    dxl = "Module firstCurrentReadonlyModule() {\r\n"
        + "  Module m\r\n"
        + "  for m in database do {\r\n"
        + "    if ( isRead m ) {\r\n"
        + "      if ( isVisible m ) {\r\n"
        + "        return m\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return null\r\n"
        + "}\r\n"
        + "Module m = firstCurrentReadonlyModule();\r\n"
        + "if ( baseline ( m ) ) {\r\n"
        + "  oleSetResult( \"true\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"false\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  private String firstCurrentVisibleModule() {
    String dxl;
    dxl = "Module firstCurrentVisibleModule() {\r\n"
        + "  Module m\r\n"
        + "  for m in database do {\r\n"
        + "    if ( isVisible m ) {\r\n"
        + "      return m\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return null\r\n"
        + "}\r\n"
        + "Module m = firstCurrentVisibleModule();\r\n"
        + "if ( m == null ) {\r\n"
        + "  oleSetResult( \"\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( fullName( m ) )\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public boolean firstCurrentVisibleModuleIsBaseline() {
    String dxl;
    dxl = "Module firstCurrentVisibleModule() {\r\n"
        + "  Module m\r\n"
        + "  for m in database do {\r\n"
        + "    if ( isVisible m ) {\r\n"
        + "      return m\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "  return null\r\n"
        + "}\r\n"
        + "Module m = firstCurrentVisibleModule();\r\n"
        + "if ( baseline( m ) ) {\r\n"
        + "  oleSetResult( \"true\" );\r\n"
        + "}\r\n"
        + "else {\r\n"
        + "  oleSetResult( \"false\" );\r\n"
        + "}\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }
  public String getBaselineInfo(String fullModulePath) {
    String dxl;
    dxl = "string result = \"\";\r\n"
        + "string fullModulePath = \"" + fullModulePath + "\";\r\n"
        + "Module m;\r\n"
        + "for m in database do {\r\n"
        + "  if ( isVisible( m ) ) {\r\n"
        + "    string modulePath = fullName( m );\r\n"
        + "    if ( modulePath == fullModulePath ) {\r\n"
        + "     if ( baseline( m ) ) {\r\n"
        + "       Baseline b = baselineInfo( m );\r\n"
        + "       if ( null != b ) {\r\n"
        + "         result = major( b ) \".\" minor( b ) \"\" suffix( b ) \"\";\r\n"
        + "       }\r\n"
        + "     }\r\n"
        + "     break;\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"

        + " oleSetResult( result );\r\n"
        ;
    objDOORS.runStr(dxl);
    return objDOORS.Result();
  }

  public String getFullModulePathOnlyOpenEdit() {
    if ( this.countOpenEditModules == 1 ) {
      return firstCurrentEditModule();
    }
    else {
      return null;
    }
  }

  public String getFullModulePathOnlyOpenReadonly() {
    if ( this.countOpenReadonlyModules == 1 ) {
      return firstCurrentReadonlyModule();
    }
    else {
      return null;
    }
  }

  public String getFullModulePathOnlyOpenVisible() {
    if ( this.countOpenVisibleModules == 1 ) {
      return firstCurrentVisibleModule();
    }
    else {
      return null;
    }
  }

  public String getModulesListVisible() {
    String dxl;
    dxl = "string result = \"\";\r\n"
        + "Module m;\r\n"
        + "for m in database do {\r\n"
        + "  if ( isVisible m ) {\r\n"
        + "    if ( result != \"\" ) {\r\n"
        + "      result = result \"\n\";\r\n"
        + "    }\r\n"
        + "    result = result fullName( m );\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public String getModulesListEdit() {
    String dxl;
    dxl = "string result = \"\";\r\n"
        + "Module m;\r\n"
        + "for m in database do {\r\n"
        + "  if ( isVisible m ) {\r\n"
        + "    if ( isEdit m ) {\r\n"
        + "      if ( result != \"\" ) {\r\n"
        + "        result = result \"\n\";\r\n"
        + "      }\r\n"
        + "      result = result fullName( m );\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

  public List<String> getVisibleModules(){
    String modules = getModulesListVisible();
    if(!Helper.isNullOrEmpty(modules)) {
      String[] splitString = modules.split( "\n" );
      return Arrays.asList( splitString );
    }
    return new ArrayList<>();
  }

  public boolean isModuleOpenVisible( String fullModulePath ) {
    String dxl;
    dxl = "string result = \"false\";\r\n"
        + "string fullModulePath = \"" + fullModulePath + "\";\r\n"
        + "Module m;\r\n"
        + "for m in database do {\r\n"
        + "  if ( isVisible( m ) ) {\r\n"
        + "    string modulePath = fullName( m );\r\n"
        + "    if ( modulePath == fullModulePath ) {\r\n"
        + "      result = \"true\";\r\n"
        + "      break;\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean isModuleOpenRead( String fullModulePath ) {
    String dxl;
    dxl = "string result = \"false\";\r\n"
        + "string fullModulePath = \"" + fullModulePath + "\";\r\n"
        + "Module m;\r\n"
        + "for m in database do {\r\n"
        + "  if ( isVisible( m ) ) {\r\n"
        + "    if ( isRead( m ) ) {\r\n"
        + "      string modulePath = fullName( m );\r\n"
        + "      if ( modulePath == fullModulePath ) {\r\n"
        + "        result = \"true\";\r\n"
        + "        break;\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public boolean isModuleOpenEdit( String fullModulePath ) {
    String dxl;
    dxl = "string result = \"false\";\r\n"
        + "string fullModulePath = \"" + fullModulePath + "\";\r\n"
        + "Module m;\r\n"
        + "for m in database do {\r\n"
        + "  if ( isVisible( m ) ) {\r\n"
        + "    if ( isEdit( m ) ) {\r\n"
        + "      string modulePath = fullName( m );\r\n"
        + "      if ( modulePath == fullModulePath ) {\r\n"
        + "        result = \"true\";\r\n"
        + "        break;\r\n"
        + "      }\r\n"
        + "    }\r\n"
        + "  }\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return Boolean.parseBoolean( objDOORS.Result() );
  }

  public String getUserName() {
    String dxl;
    dxl = "string result = \"\";\r\n"
        + "User user = find();\r\n"
        + "if ( null != user ) {\r\n"
        + "  result = user.name;\r\n"
        + "}\r\n"
        + "oleSetResult( result );\r\n"
    ;
    objDOORS.runStr( dxl );
    return objDOORS.Result();
  }

}
