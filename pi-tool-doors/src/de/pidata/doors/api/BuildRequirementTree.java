/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.doors.api;

import de.pidata.log.Logger;

/**
 * Created by petrudo on 23.06.2016.
 */
public class BuildRequirementTree {
  public static String doorsPath = "/Scratch/PI-Data/Test";

  public static void main( String[] args ) {
    Doors doors = null;
    try {
      doors = new Doors();
      DoorsExplorer doorsExplorer = doors.getDoorsExplorer();
      String fullModulePath = doorsExplorer.getFullModulePathOnlyOpenEdit();
      DoorsModule doorsModule1 = new DoorsModule( doors, fullModulePath, false );
      String fullName1 = doorsModule1.getFullModulePath();
      if (fullName1 == null) {
        Logger.info( "No open DOORS module in exclusive edit mode found." );
        return;
      } else {
        Logger.info( "Found DOORS module '" + fullName1 + "'." );
      }

      boolean closed = doorsModule1.close();
      if ( closed ) {
        Logger.info( "DOORS module '" + doorsPath + "' successfully closed." );
      } else {
        Logger.info( "DOORS module '" + doorsPath + "' not successfully closed." );
      }

      boolean isOpen = doorsExplorer.openModuleWrite( fullModulePath );
      if ( isOpen ) {
        Logger.info( "DOORS module '" + doorsPath + "' successfully opened." );
      } else {
        Logger.info( "DOORS module '" + doorsPath + "' not successfully opened." );
      }
      DoorsModule doorsModule = new DoorsModule( doors, fullModulePath, false );
      String fullName = doorsModule.getFullModulePath();
      if (fullName == null) {
        Logger.info( "DOORS module '" + doorsPath + "' not found." );
        return;
      } else {
        Logger.info( "Found DOORS module '" + fullName + "'." );
      }

      long startTime = System.currentTimeMillis();
      Logger.info("Start time = '" + startTime + "'.");
      int firstAbsNum = doorsModule.getAbsoluteNumberOfFirstObject();
      Logger.info( "Absolute number of first object is '" + firstAbsNum + "'.");
      do {
        Logger.info( "Absolute number of current object is '" + firstAbsNum + "'.");
        Logger.info("Object Heading: '" + doorsModule.getAttributeFromDOORS(firstAbsNum, DoorsModule.DOORSATTR_OBJECT_HEADING) + "'.");
        Logger.info("Object Text: '" + doorsModule.getAttributeFromDOORS(firstAbsNum, DoorsModule.DOORSATTR_OBJECT_TEXT) + "'.");
      } while (( firstAbsNum = doorsModule.getNextAbsoluteNumber( firstAbsNum )) != 0);
      long endTime = System.currentTimeMillis();
      Logger.info("End time = '" + endTime + "'.");
      Logger.info("Time elapsed = '" + ((endTime-startTime)/1000) + "' seconds.");

      startTime = System.currentTimeMillis();
      Logger.info("Start time = '" + startTime + "'.");
      if (!doorsModule.gotoFirstInDOORSModule()) {
        Logger.error("Module is empty.", null);
        return;
      };
      do {
        Logger.info("Absolute Number: '" + doorsModule.getCurrentAbsoluteNumberFromDOORS() + "'.");
        Logger.info("Object Heading: '" + doorsModule.getCurrentRowHeadingFromDOORS() + "'.");
        Logger.info("Object Text: '" + doorsModule.getCurrentRowTextFromDOORS() + "'.");
      } while (doorsModule.moveDownInDOORS());
  /*
  */
      boolean closed1 = doorsModule.close();
      if (closed1) {
        Logger.info( "DOORS module '" + doorsPath + "' sucessfully closed." );
      } else {
        Logger.info( "DOORS module '" + doorsPath + "' not sucessfully closed." );
      }

      endTime = System.currentTimeMillis();
      Logger.info("End time = '" + endTime + "'.");
      Logger.info("Time elapsed = '" + ((endTime-startTime)/1000) + "' seconds.");
    }
    catch (InstantiationException e) {
      Logger.error( "DOORS not installed.", e );
    }
  }
}
