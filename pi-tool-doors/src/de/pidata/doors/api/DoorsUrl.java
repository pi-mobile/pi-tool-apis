/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.pidata.doors.api;

public class DoorsUrl {
  private String doorsUrlString;
  private String protocol;
  private String domainName;
  private String[] parameters;
  private String guid;
  private String absNumString;
  private int absNum;
  private String urnString;
  private String dbID;
  private String moduleID;

  private final String doorsProtocol = "doors://";
  private final String urnSeparator = "urn=urn:telelogic::";

  private final String protocolSeparator = "://";
  private final String portSeparator = ":";
  private final String parametersSeparator = "/?";
  private final String parametersSeparatorRegEx = "/\\?";
  private final String parameterSeparator = "&";
  private final String parameterPartSeparator = "=";
  private final String urnName = "urn";
  private final String urnPartSeparator = "-";

  public static final String taggedValue = "DOORS-URL";

  public DoorsUrl( String doorsUrlString ) {
    if ( isValidDoorsUrl( doorsUrlString ) ) {
      this.doorsUrlString = doorsUrlString;
      String[] array1 = doorsUrlString.split( protocolSeparator );
      this.protocol = array1[0];
      String rest = array1[1];
      array1 = rest.split( parametersSeparatorRegEx );
      this.domainName = array1[0];
      rest = array1[1];
      this.parameters = rest.split( parameterSeparator );
      for ( String parameter: parameters ) {
        String[] parameterParts = parameter.split( parameterPartSeparator );
        if ( urnName.equals( parameterParts[0] ) ) {
          this.urnString = parameter;
          this.guid = parameterParts[1];
          array1 = guid.split( urnPartSeparator );
          if(array1.length == 4){
            //URL belongs to a module -> Absolute Number 0
            this.dbID = array1[1];
            this.moduleID = array1[3];
            this.absNumString = "0";
            this.absNum = 0;
          }
          else if(array1.length == 5) {
            //URL belongs to an object
            this.dbID = array1[1];
            this.absNumString = array1[3];
            this.moduleID = array1[4];
            this.absNum = Integer.parseInt( absNumString );
          }
        }
      }
    }
  }

  public String getModuleIdAndAbsNum(){
    return urnPartSeparator + this.absNumString + urnPartSeparator + this.moduleID;
  }

  public String getAbsNumString() {
    return absNumString;
  }

  public int getAbsNum() {
    return absNum;
  }

  public String getUrnString() {
    return urnString;
  }

  public String getModuleID() {
    return moduleID;
  }

  public boolean isValidDoorsUrl( String doorsUrlString ) {
    boolean isValid = true;
    if ( !doorsUrlString.startsWith( doorsProtocol ) ) {
      return false;
    }
    if ( !doorsUrlString.contains( protocolSeparator ) ) {
      return false;
    }
    if ( !doorsUrlString.contains( portSeparator ) ) {
      return false;
    }
    if ( !doorsUrlString.contains( parametersSeparator ) ) {
      return false;
    }
    if ( !doorsUrlString.contains( parameterSeparator ) ) {
      return false;
    }
    if ( !doorsUrlString.contains( urnPartSeparator ) ) {
      return false;
    }
    return isValid;
  }

  private int getAbsNumFromDoorsUrl( String doorsUrl ) {
    String[] array1 = doorsUrl.split( urnSeparator );
    String rest = array1[1];
    String[] array2 = rest.split( urnPartSeparator );
    String absNumString = array2[3];
    return Integer.parseInt( absNumString );
  }

}
