/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.doors.models;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelCollection;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.lang.Integer;
import java.lang.String;
import java.util.Collection;
import java.util.Hashtable;

public class Requirement extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.doors" );

  public static final QName ID_ABSNUM = NAMESPACE.getQName("absNum");
  public static final QName ID_HEADING = NAMESPACE.getQName("heading");
  public static final QName ID_REQUIREMENT = NAMESPACE.getQName("requirement");
  public static final QName ID_TEXT = NAMESPACE.getQName("text");
  public static final QName ID_COLUMNVALUE = NAMESPACE.getQName("columnValue");

  public static final ComplexType TYPE;
  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "requirement" ), Requirement.class.getName(), 0 );
    TYPE = type;
    type.addAttributeType( ID_ABSNUM, IntegerType.getDefInt() );
    type.addAttributeType( ID_HEADING, StringType.getDefString() );
    type.addAttributeType( ID_TEXT, StringType.getDefString() );
    type.addRelation( ID_COLUMNVALUE, ColumnValue.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_REQUIREMENT, Requirement.TYPE, 0, Integer.MAX_VALUE);
  }

  private Collection<ColumnValue> columnValues = new ModelCollection<ColumnValue>( ID_COLUMNVALUE, children );
  private Collection<Requirement> requirements = new ModelCollection<Requirement>( ID_REQUIREMENT, children );

  public Requirement() {
    super( null, TYPE, null, null, null );
  }

  public Requirement(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, TYPE, attributeNames, anyAttribs, childNames);
  }

  protected Requirement(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute absNum.
   *
   * @return The attribute absNum
   */
  public Integer getAbsNum() {
    return (Integer) get( ID_ABSNUM );
  }

  /**
   * Set the attribute absNum.
   *
   * @param absNum new value for attribute absNum
   */
  public void setAbsNum( Integer absNum ) {
    set( ID_ABSNUM, absNum );
  }

  /**
   * Returns the attribute heading.
   *
   * @return The attribute heading
   */
  public String getHeading() {
    return (String) get( ID_HEADING );
  }

  /**
   * Set the attribute heading.
   *
   * @param heading new value for attribute heading
   */
  public void setHeading( String heading ) {
    set( ID_HEADING, heading );
  }

  /**
   * Returns the attribute text.
   *
   * @return The attribute text
   */
  public String getText() {
    return (String) get( ID_TEXT );
  }

  /**
   * Set the attribute text.
   *
   * @param text new value for attribute text
   */
  public void setText( String text ) {
    set( ID_TEXT, text );
  }

  public ColumnValue getColumnValue( Key columnValueID ) {
    return (ColumnValue) get( ID_COLUMNVALUE, columnValueID );
  }

  public void addColumnValue( ColumnValue columnValue ) {
    add( ID_COLUMNVALUE, columnValue );
  }

  public void removeColumnValue( ColumnValue columnValue ) {
    remove( ID_COLUMNVALUE, columnValue );
  }

  public ModelIterator<ColumnValue> columnValueIter() {
    return iterator( ID_COLUMNVALUE, null );
  }

  public int columnValueCount() {
    return childCount( ID_COLUMNVALUE );
  }

  /**
   * Returns the collection columnValue.
   *
   * @return The collection columnValue
   */
  public Collection<ColumnValue> getColumnValues() {
    return columnValues;
  }

  public Requirement getRequirement( Key requirementID ) {
    return (Requirement) get( ID_REQUIREMENT, requirementID );
  }

  public void addRequirement( Requirement requirement ) {
    add( ID_REQUIREMENT, requirement );
  }

  public void removeRequirement( Requirement requirement ) {
    remove( ID_REQUIREMENT, requirement );
  }

  public ModelIterator<Requirement> requirementIter() {
    return iterator( ID_REQUIREMENT, null );
  }

  public int requirementCount() {
    return childCount( ID_REQUIREMENT );
  }

  /**
   * Returns the collection requirement.
   *
   * @return The collection requirement
   */
  public Collection<Requirement> getRequirements() {
    return requirements;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
