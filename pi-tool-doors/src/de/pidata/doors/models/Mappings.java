/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.doors.models;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelCollection;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Collection;
import java.util.Hashtable;

public class Mappings extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.doors" );

  public static final QName ID_MAPTAG = NAMESPACE.getQName("mapTag");
  public static final QName ID_MAPATTR = NAMESPACE.getQName("mapAttr");

  public static final ComplexType TYPE;
  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "mappings" ), Mappings.class.getName(), 0 );
    TYPE = type;
    type.addRelation( ID_MAPTAG, MapTag.TYPE, 0, Integer.MAX_VALUE);
    type.addRelation( ID_MAPATTR, MapTag.TYPE, 0, Integer.MAX_VALUE);
  }

  private Collection<MapTag> mapTags = new ModelCollection<MapTag>( ID_MAPTAG, children );
  private Collection<MapTag> mapAttrs = new ModelCollection<MapTag>( ID_MAPATTR, children );

  public Mappings() {
    super( null, TYPE, null, null, null );
  }

  public Mappings(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, TYPE, attributeNames, anyAttribs, childNames);
  }

  protected Mappings(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  public MapTag getMapTag( Key mapTagID ) {
    return (MapTag) get( ID_MAPTAG, mapTagID );
  }

  public void addMapTag( MapTag mapTag ) {
    add( ID_MAPTAG, mapTag );
  }

  public void removeMapTag( MapTag mapTag ) {
    remove( ID_MAPTAG, mapTag );
  }

  public ModelIterator<MapTag> mapTagIter() {
    return iterator( ID_MAPTAG, null );
  }

  public int mapTagCount() {
    return childCount( ID_MAPTAG );
  }

  /**
   * Returns the collection mapTag.
   *
   * @return The collection mapTag
   */
  public Collection<MapTag> getMapTags() {
    return mapTags;
  }

  public MapTag getMapAttr( Key mapAttrID ) {
    return (MapTag) get( ID_MAPATTR, mapAttrID );
  }

  public void addMapAttr( MapTag mapAttr ) {
    add( ID_MAPATTR, mapAttr );
  }

  public void removeMapAttr( MapTag mapAttr ) {
    remove( ID_MAPATTR, mapAttr );
  }

  public ModelIterator<MapTag> mapAttrIter() {
    return iterator( ID_MAPATTR, null );
  }

  public int mapAttrCount() {
    return childCount( ID_MAPATTR );
  }

  /**
   * Returns the collection mapAttr.
   *
   * @return The collection mapAttr
   */
  public Collection<MapTag> getMapAttrs() {
    return mapAttrs;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
