/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.doors.models;

import de.pidata.models.tree.*;
import de.pidata.models.types.Type;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.util.Hashtable;

public class DoorsModelFactory extends de.pidata.models.tree.AbstractModelFactory {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.doors" );

  public static final QName ID_REQUIREMENT = NAMESPACE.getQName("requirement");
  public static final QName ID_MAPPINGS = NAMESPACE.getQName("mappings");
  public static final QName ID_COLUMNVALUE = NAMESPACE.getQName("columnValue");

  public DoorsModelFactory() {
    super( NAMESPACE, "de.pidata.doors", "null" );
    addType( MapTag.TYPE );
    addType( MapAttr.TYPE );
    addType( ColumnValue.TYPE );
    addType( Requirement.TYPE );
    addType( Mappings.TYPE );
    addRootRelation( ID_COLUMNVALUE, ColumnValue.TYPE, 1, 1);
    addRootRelation( ID_REQUIREMENT, Requirement.TYPE, 1, 1);
    addRootRelation( ID_MAPPINGS, Mappings.TYPE, 1, 1);
  }

  public Model createInstance(Key key, Type typeDef, Object[] attributes, Hashtable anyAttribs, ChildList children) {
    Class modelClass = typeDef.getValueClass();
    if (modelClass == MapTag.class) {
      return new MapTag(key, attributes, anyAttribs, children);
    }
    if (modelClass == MapAttr.class) {
      return new MapAttr(key, attributes, anyAttribs, children);
    }
    if (modelClass == ColumnValue.class) {
      return new ColumnValue(key, attributes, anyAttribs, children);
    }
    if (modelClass == Requirement.class) {
      return new Requirement(key, attributes, anyAttribs, children);
    }
    if (modelClass == Mappings.class) {
      return new Mappings(key, attributes, anyAttribs, children);
    }
    return super.createInstance(key, typeDef, attributes, anyAttribs, children);
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
