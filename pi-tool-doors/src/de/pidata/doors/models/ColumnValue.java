/*
 * This file is part of PI-Mobile (https://gitlab.com/pi-mobile/pi-tool-apis).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.doors.models;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import java.lang.String;
import java.util.Hashtable;

public class ColumnValue extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.doors" );

  public static final QName ID_VALUE = NAMESPACE.getQName("value");
  public static final QName ID_NAME = NAMESPACE.getQName("name");

  public static final ComplexType TYPE;
  static {
    DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "columnValue" ), ColumnValue.class.getName(), 0 );
    TYPE = type;
    type.addAttributeType( ID_NAME, StringType.getDefString() );
    type.addAttributeType( ID_VALUE, StringType.getDefString() );
  }

  public ColumnValue() {
    super( null, TYPE, null, null, null );
  }

  public ColumnValue(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, TYPE, attributeNames, anyAttribs, childNames);
  }

  protected ColumnValue(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute name.
   *
   * @return The attribute name
   */
  public String getName() {
    return (String) get( ID_NAME );
  }

  /**
   * Set the attribute name.
   *
   * @param name new value for attribute name
   */
  public void setName( String name ) {
    set( ID_NAME, name );
  }

  /**
   * Returns the attribute value.
   *
   * @return The attribute value
   */
  public String getValue() {
    return (String) get( ID_VALUE );
  }

  /**
   * Set the attribute value.
   *
   * @param value new value for attribute value
   */
  public void setValue( String value ) {
    set( ID_VALUE, value );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
