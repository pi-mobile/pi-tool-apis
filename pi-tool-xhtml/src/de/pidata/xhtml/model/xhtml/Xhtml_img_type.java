// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.xhtml.model.xhtml;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.models.xml.binder.Space;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class Xhtml_img_type extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/1999/xhtml" );

  public static final QName ID_ALT = NAMESPACE.getQName( "alt" );
  public static final QName ID_CLASS = NAMESPACE.getQName( "class" );
  public static final QName ID_DIR = NAMESPACE.getQName( "dir" );
  public static final QName ID_HEIGHT = NAMESPACE.getQName( "height" );
  public static final QName ID_ID = NAMESPACE.getQName( "id" );
  public static final QName ID_LANG = Namespace.getInstance( "http://www.w3.org/XML/1998/namespace" ).getQName( "lang" );
  public static final QName ID_LONGDESC = NAMESPACE.getQName( "longdesc" );
  public static final QName ID_NAME = NAMESPACE.getQName( "name" );
  public static final QName ID_ONCLICK = NAMESPACE.getQName( "onclick" );
  public static final QName ID_ONDBLCLICK = NAMESPACE.getQName( "ondblclick" );
  public static final QName ID_ONKEYDOWN = NAMESPACE.getQName( "onkeydown" );
  public static final QName ID_ONKEYPRESS = NAMESPACE.getQName( "onkeypress" );
  public static final QName ID_ONKEYUP = NAMESPACE.getQName( "onkeyup" );
  public static final QName ID_ONMOUSEDOWN = NAMESPACE.getQName( "onmousedown" );
  public static final QName ID_ONMOUSEMOVE = NAMESPACE.getQName( "onmousemove" );
  public static final QName ID_ONMOUSEOUT = NAMESPACE.getQName( "onmouseout" );
  public static final QName ID_ONMOUSEOVER = NAMESPACE.getQName( "onmouseover" );
  public static final QName ID_ONMOUSEUP = NAMESPACE.getQName( "onmouseup" );
  public static final QName ID_SPACE = Namespace.getInstance( "http://www.w3.org/XML/1998/namespace" ).getQName( "space" );
  public static final QName ID_SRC = NAMESPACE.getQName( "src" );
  public static final QName ID_STYLE = NAMESPACE.getQName( "style" );
  public static final QName ID_TITLE = NAMESPACE.getQName( "title" );
  public static final QName ID_WIDTH = NAMESPACE.getQName( "width" );

  public Xhtml_img_type() {
    super( null, ModelFactory.XHTML_IMG_TYPE_TYPE, null, null, null );
  }

  public Xhtml_img_type( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, ModelFactory.XHTML_IMG_TYPE_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected Xhtml_img_type( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute space.
   *
   * @return The attribute space
   */
  public Space getSpace() {
    return (Space) get( ID_SPACE );
  }

  /**
   * Set the attribute space.
   *
   * @param space new value for attribute space
   */
  public void setSpace( Space space ) {
    set( ID_SPACE, space );
  }

  /**
   * Returns the attribute id.
   *
   * @return The attribute id
   */
  public QName getId() {
    return (QName) get( ID_ID );
  }

  /**
   * Set the attribute id.
   *
   * @param id new value for attribute id
   */
  public void setId( QName id ) {
    set( ID_ID, id );
  }

  /**
   * Returns the attribute class.
   *
   * @return The attribute class
   */
  public String getClass_() {
    return (String) get( ID_CLASS );
  }

  /**
   * Set the attribute class.
   *
   * @param _class new value for attribute class
   */
  public void setClass( String _class ) {
    set( ID_CLASS, _class );
  }

  /**
   * Returns the attribute title.
   *
   * @return The attribute title
   */
  public String getTitle() {
    return (String) get( ID_TITLE );
  }

  /**
   * Set the attribute title.
   *
   * @param title new value for attribute title
   */
  public void setTitle( String title ) {
    set( ID_TITLE, title );
  }

  /**
   * Returns the attribute lang.
   *
   * @return The attribute lang
   */
  public String getLang() {
    return (String) get( ID_LANG );
  }

  /**
   * Set the attribute lang.
   *
   * @param lang new value for attribute lang
   */
  public void setLang( String lang ) {
    set( ID_LANG, lang );
  }

  /**
   * Returns the attribute dir.
   *
   * @return The attribute dir
   */
  public Dir getDir() {
    return (Dir) get( ID_DIR );
  }

  /**
   * Set the attribute dir.
   *
   * @param dir new value for attribute dir
   */
  public void setDir( Dir dir ) {
    set( ID_DIR, dir );
  }

  /**
   * Returns the attribute style.
   *
   * @return The attribute style
   */
  public String getStyle() {
    return (String) get( ID_STYLE );
  }

  /**
   * Set the attribute style.
   *
   * @param style new value for attribute style
   */
  public void setStyle( String style ) {
    set( ID_STYLE, style );
  }

  /**
   * Returns the attribute onclick.
   *
   * @return The attribute onclick
   */
  public String getOnclick() {
    return (String) get( ID_ONCLICK );
  }

  /**
   * Set the attribute onclick.
   *
   * @param onclick new value for attribute onclick
   */
  public void setOnclick( String onclick ) {
    set( ID_ONCLICK, onclick );
  }

  /**
   * Returns the attribute ondblclick.
   *
   * @return The attribute ondblclick
   */
  public String getOndblclick() {
    return (String) get( ID_ONDBLCLICK );
  }

  /**
   * Set the attribute ondblclick.
   *
   * @param ondblclick new value for attribute ondblclick
   */
  public void setOndblclick( String ondblclick ) {
    set( ID_ONDBLCLICK, ondblclick );
  }

  /**
   * Returns the attribute onmousedown.
   *
   * @return The attribute onmousedown
   */
  public String getOnmousedown() {
    return (String) get( ID_ONMOUSEDOWN );
  }

  /**
   * Set the attribute onmousedown.
   *
   * @param onmousedown new value for attribute onmousedown
   */
  public void setOnmousedown( String onmousedown ) {
    set( ID_ONMOUSEDOWN, onmousedown );
  }

  /**
   * Returns the attribute onmouseup.
   *
   * @return The attribute onmouseup
   */
  public String getOnmouseup() {
    return (String) get( ID_ONMOUSEUP );
  }

  /**
   * Set the attribute onmouseup.
   *
   * @param onmouseup new value for attribute onmouseup
   */
  public void setOnmouseup( String onmouseup ) {
    set( ID_ONMOUSEUP, onmouseup );
  }

  /**
   * Returns the attribute onmouseover.
   *
   * @return The attribute onmouseover
   */
  public String getOnmouseover() {
    return (String) get( ID_ONMOUSEOVER );
  }

  /**
   * Set the attribute onmouseover.
   *
   * @param onmouseover new value for attribute onmouseover
   */
  public void setOnmouseover( String onmouseover ) {
    set( ID_ONMOUSEOVER, onmouseover );
  }

  /**
   * Returns the attribute onmousemove.
   *
   * @return The attribute onmousemove
   */
  public String getOnmousemove() {
    return (String) get( ID_ONMOUSEMOVE );
  }

  /**
   * Set the attribute onmousemove.
   *
   * @param onmousemove new value for attribute onmousemove
   */
  public void setOnmousemove( String onmousemove ) {
    set( ID_ONMOUSEMOVE, onmousemove );
  }

  /**
   * Returns the attribute onmouseout.
   *
   * @return The attribute onmouseout
   */
  public String getOnmouseout() {
    return (String) get( ID_ONMOUSEOUT );
  }

  /**
   * Set the attribute onmouseout.
   *
   * @param onmouseout new value for attribute onmouseout
   */
  public void setOnmouseout( String onmouseout ) {
    set( ID_ONMOUSEOUT, onmouseout );
  }

  /**
   * Returns the attribute onkeypress.
   *
   * @return The attribute onkeypress
   */
  public String getOnkeypress() {
    return (String) get( ID_ONKEYPRESS );
  }

  /**
   * Set the attribute onkeypress.
   *
   * @param onkeypress new value for attribute onkeypress
   */
  public void setOnkeypress( String onkeypress ) {
    set( ID_ONKEYPRESS, onkeypress );
  }

  /**
   * Returns the attribute onkeydown.
   *
   * @return The attribute onkeydown
   */
  public String getOnkeydown() {
    return (String) get( ID_ONKEYDOWN );
  }

  /**
   * Set the attribute onkeydown.
   *
   * @param onkeydown new value for attribute onkeydown
   */
  public void setOnkeydown( String onkeydown ) {
    set( ID_ONKEYDOWN, onkeydown );
  }

  /**
   * Returns the attribute onkeyup.
   *
   * @return The attribute onkeyup
   */
  public String getOnkeyup() {
    return (String) get( ID_ONKEYUP );
  }

  /**
   * Set the attribute onkeyup.
   *
   * @param onkeyup new value for attribute onkeyup
   */
  public void setOnkeyup( String onkeyup ) {
    set( ID_ONKEYUP, onkeyup );
  }

  /**
   * Returns the attribute src.
   *
   * @return The attribute src
   */
  public String getSrc() {
    return (String) get( ID_SRC );
  }

  /**
   * Set the attribute src.
   *
   * @param src new value for attribute src
   */
  public void setSrc( String src ) {
    set( ID_SRC, src );
  }

  /**
   * Returns the attribute alt.
   *
   * @return The attribute alt
   */
  public String getAlt() {
    return (String) get( ID_ALT );
  }

  /**
   * Set the attribute alt.
   *
   * @param alt new value for attribute alt
   */
  public void setAlt( String alt ) {
    set( ID_ALT, alt );
  }

  /**
   * Returns the attribute longdesc.
   *
   * @return The attribute longdesc
   */
  public String getLongdesc() {
    return (String) get( ID_LONGDESC );
  }

  /**
   * Set the attribute longdesc.
   *
   * @param longdesc new value for attribute longdesc
   */
  public void setLongdesc( String longdesc ) {
    set( ID_LONGDESC, longdesc );
  }

  /**
   * Returns the attribute name.
   *
   * @return The attribute name
   */
  public String getName() {
    return (String) get( ID_NAME );
  }

  /**
   * Set the attribute name.
   *
   * @param name new value for attribute name
   */
  public void setName( String name ) {
    set( ID_NAME, name );
  }

  /**
   * Returns the attribute height.
   *
   * @return The attribute height
   */
  public String getHeight() {
    return (String) get( ID_HEIGHT );
  }

  /**
   * Set the attribute height.
   *
   * @param height new value for attribute height
   */
  public void setHeight( String height ) {
    set( ID_HEIGHT, height );
  }

  /**
   * Returns the attribute width.
   *
   * @return The attribute width
   */
  public String getWidth() {
    return (String) get( ID_WIDTH );
  }

  /**
   * Set the attribute width.
   *
   * @param width new value for attribute width
   */
  public void setWidth( String width ) {
    set( ID_WIDTH, width );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
