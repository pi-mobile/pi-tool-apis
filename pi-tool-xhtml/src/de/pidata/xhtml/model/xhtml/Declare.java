// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.xhtml.model.xhtml;


public enum Declare {

  declare( "declare" );

  private String value;

  Declare( String value ) {
    this.value = value;
  }

  /**
   * Returns the String value of the Declare instance.
   *
   * @return The String value of the Declare instance.
   */
  public String getValue() {
    return this.value;
  }

  /**
   * Returns the Declare instance associated with the given String value.
   *
   * @param stringValue The value to associate with an Declare instance
   * @return The Declare instance associated with the given String value.
   */
  public static Declare fromString( String stringValue ) {
    if (stringValue == null) {
      return null;
    }
    for (Declare entry : values()) {
      if (entry.value.equals( stringValue )) {
        return entry;
      }
    }
    return null;
  }

  /**
   * Returns the name of this enum constant as declared in the schema.
   *
   * @return The name of this enum constant as declared in the schema.
   */
  @Override
  public String toString() {
    return value.toString();
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
