// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.xhtml.model.xhtml;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.models.xml.binder.Space;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class Xhtml_heading_type extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://www.w3.org/1999/xhtml" );

  public static final QName ID_A = NAMESPACE.getQName( "a" );
  public static final QName ID_ABBR = NAMESPACE.getQName( "abbr" );
  public static final QName ID_ACRONYM = NAMESPACE.getQName( "acronym" );
  public static final QName ID_B = NAMESPACE.getQName( "b" );
  public static final QName ID_BDO = NAMESPACE.getQName( "bdo" );
  public static final QName ID_BIG = NAMESPACE.getQName( "big" );
  public static final QName ID_BR = NAMESPACE.getQName( "br" );
  public static final QName ID_BUTTON = NAMESPACE.getQName( "button" );
  public static final QName ID_CITE = NAMESPACE.getQName( "cite" );
  public static final QName ID_CLASS = NAMESPACE.getQName( "class" );
  public static final QName ID_CODE = NAMESPACE.getQName( "code" );
  public static final QName ID_DEL = NAMESPACE.getQName( "del" );
  public static final QName ID_DFN = NAMESPACE.getQName( "dfn" );
  public static final QName ID_DIR = NAMESPACE.getQName( "dir" );
  public static final QName ID_EM = NAMESPACE.getQName( "em" );
  public static final QName ID_I = NAMESPACE.getQName( "i" );
  public static final QName ID_ID = NAMESPACE.getQName( "id" );
  public static final QName ID_IMG = NAMESPACE.getQName( "img" );
  public static final QName ID_INPUT = NAMESPACE.getQName( "input" );
  public static final QName ID_INS = NAMESPACE.getQName( "ins" );
  public static final QName ID_KBD = NAMESPACE.getQName( "kbd" );
  public static final QName ID_LABEL = NAMESPACE.getQName( "label" );
  public static final QName ID_LANG = Namespace.getInstance( "http://www.w3.org/XML/1998/namespace" ).getQName( "lang" );
  public static final QName ID_MAP = NAMESPACE.getQName( "map" );
  public static final QName ID_NOSCRIPT = NAMESPACE.getQName( "noscript" );
  public static final QName ID_OBJECT = NAMESPACE.getQName( "object" );
  public static final QName ID_ONCLICK = NAMESPACE.getQName( "onclick" );
  public static final QName ID_ONDBLCLICK = NAMESPACE.getQName( "ondblclick" );
  public static final QName ID_ONKEYDOWN = NAMESPACE.getQName( "onkeydown" );
  public static final QName ID_ONKEYPRESS = NAMESPACE.getQName( "onkeypress" );
  public static final QName ID_ONKEYUP = NAMESPACE.getQName( "onkeyup" );
  public static final QName ID_ONMOUSEDOWN = NAMESPACE.getQName( "onmousedown" );
  public static final QName ID_ONMOUSEMOVE = NAMESPACE.getQName( "onmousemove" );
  public static final QName ID_ONMOUSEOUT = NAMESPACE.getQName( "onmouseout" );
  public static final QName ID_ONMOUSEOVER = NAMESPACE.getQName( "onmouseover" );
  public static final QName ID_ONMOUSEUP = NAMESPACE.getQName( "onmouseup" );
  public static final QName ID_Q = NAMESPACE.getQName( "q" );
  public static final QName ID_RUBY = NAMESPACE.getQName( "ruby" );
  public static final QName ID_SAMP = NAMESPACE.getQName( "samp" );
  public static final QName ID_SCRIPT = NAMESPACE.getQName( "script" );
  public static final QName ID_SELECT = NAMESPACE.getQName( "select" );
  public static final QName ID_SMALL = NAMESPACE.getQName( "small" );
  public static final QName ID_SPACE = Namespace.getInstance( "http://www.w3.org/XML/1998/namespace" ).getQName( "space" );
  public static final QName ID_SPAN = NAMESPACE.getQName( "span" );
  public static final QName ID_STRONG = NAMESPACE.getQName( "strong" );
  public static final QName ID_STYLE = NAMESPACE.getQName( "style" );
  public static final QName ID_SUB = NAMESPACE.getQName( "sub" );
  public static final QName ID_SUP = NAMESPACE.getQName( "sup" );
  public static final QName ID_TEXTAREA = NAMESPACE.getQName( "textarea" );
  public static final QName ID_TITLE = NAMESPACE.getQName( "title" );
  public static final QName ID_TT = NAMESPACE.getQName( "tt" );
  public static final QName ID_U = NAMESPACE.getQName( "u" );
  public static final QName ID_VAR = NAMESPACE.getQName( "var" );

  public Xhtml_heading_type() {
    super( null, ModelFactory.XHTML_HEADING_TYPE_TYPE, null, null, null );
  }

  public Xhtml_heading_type( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, ModelFactory.XHTML_HEADING_TYPE_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected Xhtml_heading_type( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute space.
   *
   * @return The attribute space
   */
  public Space getSpace() {
    return (Space) get( ID_SPACE );
  }

  /**
   * Set the attribute space.
   *
   * @param space new value for attribute space
   */
  public void setSpace( Space space ) {
    set( ID_SPACE, space );
  }

  /**
   * Returns the attribute id.
   *
   * @return The attribute id
   */
  public QName getId() {
    return (QName) get( ID_ID );
  }

  /**
   * Set the attribute id.
   *
   * @param id new value for attribute id
   */
  public void setId( QName id ) {
    set( ID_ID, id );
  }

  /**
   * Returns the attribute class.
   *
   * @return The attribute class
   */
  public String getClass_() {
    return (String) get( ID_CLASS );
  }

  /**
   * Set the attribute class.
   *
   * @param _class new value for attribute class
   */
  public void setClass( String _class ) {
    set( ID_CLASS, _class );
  }

  /**
   * Returns the attribute title.
   *
   * @return The attribute title
   */
  public String getTitle() {
    return (String) get( ID_TITLE );
  }

  /**
   * Set the attribute title.
   *
   * @param title new value for attribute title
   */
  public void setTitle( String title ) {
    set( ID_TITLE, title );
  }

  /**
   * Returns the attribute lang.
   *
   * @return The attribute lang
   */
  public String getLang() {
    return (String) get( ID_LANG );
  }

  /**
   * Set the attribute lang.
   *
   * @param lang new value for attribute lang
   */
  public void setLang( String lang ) {
    set( ID_LANG, lang );
  }

  /**
   * Returns the attribute dir.
   *
   * @return The attribute dir
   */
  public Dir getDir() {
    return (Dir) get( ID_DIR );
  }

  /**
   * Set the attribute dir.
   *
   * @param dir new value for attribute dir
   */
  public void setDir( Dir dir ) {
    set( ID_DIR, dir );
  }

  /**
   * Returns the attribute style.
   *
   * @return The attribute style
   */
  public String getStyle() {
    return (String) get( ID_STYLE );
  }

  /**
   * Set the attribute style.
   *
   * @param style new value for attribute style
   */
  public void setStyle( String style ) {
    set( ID_STYLE, style );
  }

  /**
   * Returns the attribute onclick.
   *
   * @return The attribute onclick
   */
  public String getOnclick() {
    return (String) get( ID_ONCLICK );
  }

  /**
   * Set the attribute onclick.
   *
   * @param onclick new value for attribute onclick
   */
  public void setOnclick( String onclick ) {
    set( ID_ONCLICK, onclick );
  }

  /**
   * Returns the attribute ondblclick.
   *
   * @return The attribute ondblclick
   */
  public String getOndblclick() {
    return (String) get( ID_ONDBLCLICK );
  }

  /**
   * Set the attribute ondblclick.
   *
   * @param ondblclick new value for attribute ondblclick
   */
  public void setOndblclick( String ondblclick ) {
    set( ID_ONDBLCLICK, ondblclick );
  }

  /**
   * Returns the attribute onmousedown.
   *
   * @return The attribute onmousedown
   */
  public String getOnmousedown() {
    return (String) get( ID_ONMOUSEDOWN );
  }

  /**
   * Set the attribute onmousedown.
   *
   * @param onmousedown new value for attribute onmousedown
   */
  public void setOnmousedown( String onmousedown ) {
    set( ID_ONMOUSEDOWN, onmousedown );
  }

  /**
   * Returns the attribute onmouseup.
   *
   * @return The attribute onmouseup
   */
  public String getOnmouseup() {
    return (String) get( ID_ONMOUSEUP );
  }

  /**
   * Set the attribute onmouseup.
   *
   * @param onmouseup new value for attribute onmouseup
   */
  public void setOnmouseup( String onmouseup ) {
    set( ID_ONMOUSEUP, onmouseup );
  }

  /**
   * Returns the attribute onmouseover.
   *
   * @return The attribute onmouseover
   */
  public String getOnmouseover() {
    return (String) get( ID_ONMOUSEOVER );
  }

  /**
   * Set the attribute onmouseover.
   *
   * @param onmouseover new value for attribute onmouseover
   */
  public void setOnmouseover( String onmouseover ) {
    set( ID_ONMOUSEOVER, onmouseover );
  }

  /**
   * Returns the attribute onmousemove.
   *
   * @return The attribute onmousemove
   */
  public String getOnmousemove() {
    return (String) get( ID_ONMOUSEMOVE );
  }

  /**
   * Set the attribute onmousemove.
   *
   * @param onmousemove new value for attribute onmousemove
   */
  public void setOnmousemove( String onmousemove ) {
    set( ID_ONMOUSEMOVE, onmousemove );
  }

  /**
   * Returns the attribute onmouseout.
   *
   * @return The attribute onmouseout
   */
  public String getOnmouseout() {
    return (String) get( ID_ONMOUSEOUT );
  }

  /**
   * Set the attribute onmouseout.
   *
   * @param onmouseout new value for attribute onmouseout
   */
  public void setOnmouseout( String onmouseout ) {
    set( ID_ONMOUSEOUT, onmouseout );
  }

  /**
   * Returns the attribute onkeypress.
   *
   * @return The attribute onkeypress
   */
  public String getOnkeypress() {
    return (String) get( ID_ONKEYPRESS );
  }

  /**
   * Set the attribute onkeypress.
   *
   * @param onkeypress new value for attribute onkeypress
   */
  public void setOnkeypress( String onkeypress ) {
    set( ID_ONKEYPRESS, onkeypress );
  }

  /**
   * Returns the attribute onkeydown.
   *
   * @return The attribute onkeydown
   */
  public String getOnkeydown() {
    return (String) get( ID_ONKEYDOWN );
  }

  /**
   * Set the attribute onkeydown.
   *
   * @param onkeydown new value for attribute onkeydown
   */
  public void setOnkeydown( String onkeydown ) {
    set( ID_ONKEYDOWN, onkeydown );
  }

  /**
   * Returns the attribute onkeyup.
   *
   * @return The attribute onkeyup
   */
  public String getOnkeyup() {
    return (String) get( ID_ONKEYUP );
  }

  /**
   * Set the attribute onkeyup.
   *
   * @param onkeyup new value for attribute onkeyup
   */
  public void setOnkeyup( String onkeyup ) {
    set( ID_ONKEYUP, onkeyup );
  }

  /**
   * Returns the element br.
   *
   * @return The element br
   */
  public Xhtml_br_type getBr() {
    return (Xhtml_br_type) get( ID_BR, null );
  }

  /**
   * Set the element br.
   *
   * @param br new value for element br
   */
  public void setBr( Xhtml_br_type br ) {
    setChild( ID_BR, br );
  }

  /**
   * Returns the element span.
   *
   * @return The element span
   */
  public Xhtml_span_type getSpan() {
    return (Xhtml_span_type) get( ID_SPAN, null );
  }

  /**
   * Set the element span.
   *
   * @param span new value for element span
   */
  public void setSpan( Xhtml_span_type span ) {
    setChild( ID_SPAN, span );
  }

  /**
   * Returns the element em.
   *
   * @return The element em
   */
  public Xhtml_em_type getEm() {
    return (Xhtml_em_type) get( ID_EM, null );
  }

  /**
   * Set the element em.
   *
   * @param em new value for element em
   */
  public void setEm( Xhtml_em_type em ) {
    setChild( ID_EM, em );
  }

  /**
   * Returns the element strong.
   *
   * @return The element strong
   */
  public Xhtml_strong_type getStrong() {
    return (Xhtml_strong_type) get( ID_STRONG, null );
  }

  /**
   * Set the element strong.
   *
   * @param strong new value for element strong
   */
  public void setStrong( Xhtml_strong_type strong ) {
    setChild( ID_STRONG, strong );
  }

  /**
   * Returns the element dfn.
   *
   * @return The element dfn
   */
  public Xhtml_dfn_type getDfn() {
    return (Xhtml_dfn_type) get( ID_DFN, null );
  }

  /**
   * Set the element dfn.
   *
   * @param dfn new value for element dfn
   */
  public void setDfn( Xhtml_dfn_type dfn ) {
    setChild( ID_DFN, dfn );
  }

  /**
   * Returns the element code.
   *
   * @return The element code
   */
  public Xhtml_code_type getCode() {
    return (Xhtml_code_type) get( ID_CODE, null );
  }

  /**
   * Set the element code.
   *
   * @param code new value for element code
   */
  public void setCode( Xhtml_code_type code ) {
    setChild( ID_CODE, code );
  }

  /**
   * Returns the element samp.
   *
   * @return The element samp
   */
  public Xhtml_samp_type getSamp() {
    return (Xhtml_samp_type) get( ID_SAMP, null );
  }

  /**
   * Set the element samp.
   *
   * @param samp new value for element samp
   */
  public void setSamp( Xhtml_samp_type samp ) {
    setChild( ID_SAMP, samp );
  }

  /**
   * Returns the element kbd.
   *
   * @return The element kbd
   */
  public Xhtml_kbd_type getKbd() {
    return (Xhtml_kbd_type) get( ID_KBD, null );
  }

  /**
   * Set the element kbd.
   *
   * @param kbd new value for element kbd
   */
  public void setKbd( Xhtml_kbd_type kbd ) {
    setChild( ID_KBD, kbd );
  }

  /**
   * Returns the element var.
   *
   * @return The element var
   */
  public Xhtml_var_type getVar() {
    return (Xhtml_var_type) get( ID_VAR, null );
  }

  /**
   * Set the element var.
   *
   * @param var new value for element var
   */
  public void setVar( Xhtml_var_type var ) {
    setChild( ID_VAR, var );
  }

  /**
   * Returns the element cite.
   *
   * @return The element cite
   */
  public Xhtml_cite_type getCite() {
    return (Xhtml_cite_type) get( ID_CITE, null );
  }

  /**
   * Set the element cite.
   *
   * @param cite new value for element cite
   */
  public void setCite( Xhtml_cite_type cite ) {
    setChild( ID_CITE, cite );
  }

  /**
   * Returns the element abbr.
   *
   * @return The element abbr
   */
  public Xhtml_abbr_type getAbbr() {
    return (Xhtml_abbr_type) get( ID_ABBR, null );
  }

  /**
   * Set the element abbr.
   *
   * @param abbr new value for element abbr
   */
  public void setAbbr( Xhtml_abbr_type abbr ) {
    setChild( ID_ABBR, abbr );
  }

  /**
   * Returns the element acronym.
   *
   * @return The element acronym
   */
  public Xhtml_acronym_type getAcronym() {
    return (Xhtml_acronym_type) get( ID_ACRONYM, null );
  }

  /**
   * Set the element acronym.
   *
   * @param acronym new value for element acronym
   */
  public void setAcronym( Xhtml_acronym_type acronym ) {
    setChild( ID_ACRONYM, acronym );
  }

  /**
   * Returns the element q.
   *
   * @return The element q
   */
  public Xhtml_q_type getQ() {
    return (Xhtml_q_type) get( ID_Q, null );
  }

  /**
   * Set the element q.
   *
   * @param q new value for element q
   */
  public void setQ( Xhtml_q_type q ) {
    setChild( ID_Q, q );
  }

  /**
   * Returns the element tt.
   *
   * @return The element tt
   */
  public Xhtml_InlPres_type getTt() {
    return (Xhtml_InlPres_type) get( ID_TT, null );
  }

  /**
   * Set the element tt.
   *
   * @param tt new value for element tt
   */
  public void setTt( Xhtml_InlPres_type tt ) {
    setChild( ID_TT, tt );
  }

  /**
   * Returns the element i.
   *
   * @return The element i
   */
  public Xhtml_InlPres_type getI() {
    return (Xhtml_InlPres_type) get( ID_I, null );
  }

  /**
   * Set the element i.
   *
   * @param i new value for element i
   */
  public void setI( Xhtml_InlPres_type i ) {
    setChild( ID_I, i );
  }

  /**
   * Returns the element b.
   *
   * @return The element b
   */
  public Xhtml_InlPres_type getB() {
    return (Xhtml_InlPres_type) get( ID_B, null );
  }

  /**
   * Set the element b.
   *
   * @param b new value for element b
   */
  public void setB( Xhtml_InlPres_type b ) {
    setChild( ID_B, b );
  }

  /**
   * Returns the element u.
   *
   * @return The element u
   */
  public Xhtml_InlPres_type getU() {
    return (Xhtml_InlPres_type) get( ID_U, null );
  }

  /**
   * Set the element u.
   *
   * @param u new value for element u
   */
  public void setU( Xhtml_InlPres_type u ) {
    setChild( ID_U, u );
  }

  /**
   * Returns the element big.
   *
   * @return The element big
   */
  public Xhtml_InlPres_type getBig() {
    return (Xhtml_InlPres_type) get( ID_BIG, null );
  }

  /**
   * Set the element big.
   *
   * @param big new value for element big
   */
  public void setBig( Xhtml_InlPres_type big ) {
    setChild( ID_BIG, big );
  }

  /**
   * Returns the element small.
   *
   * @return The element small
   */
  public Xhtml_InlPres_type getSmall() {
    return (Xhtml_InlPres_type) get( ID_SMALL, null );
  }

  /**
   * Set the element small.
   *
   * @param small new value for element small
   */
  public void setSmall( Xhtml_InlPres_type small ) {
    setChild( ID_SMALL, small );
  }

  /**
   * Returns the element sub.
   *
   * @return The element sub
   */
  public Xhtml_InlPres_type getSub() {
    return (Xhtml_InlPres_type) get( ID_SUB, null );
  }

  /**
   * Set the element sub.
   *
   * @param sub new value for element sub
   */
  public void setSub( Xhtml_InlPres_type sub ) {
    setChild( ID_SUB, sub );
  }

  /**
   * Returns the element sup.
   *
   * @return The element sup
   */
  public Xhtml_InlPres_type getSup() {
    return (Xhtml_InlPres_type) get( ID_SUP, null );
  }

  /**
   * Set the element sup.
   *
   * @param sup new value for element sup
   */
  public void setSup( Xhtml_InlPres_type sup ) {
    setChild( ID_SUP, sup );
  }

  /**
   * Returns the element bdo.
   *
   * @return The element bdo
   */
  public Xhtml_bdo_type getBdo() {
    return (Xhtml_bdo_type) get( ID_BDO, null );
  }

  /**
   * Set the element bdo.
   *
   * @param bdo new value for element bdo
   */
  public void setBdo( Xhtml_bdo_type bdo ) {
    setChild( ID_BDO, bdo );
  }

  /**
   * Returns the element a.
   *
   * @return The element a
   */
  public Xhtml_a_type getA() {
    return (Xhtml_a_type) get( ID_A, null );
  }

  /**
   * Set the element a.
   *
   * @param a new value for element a
   */
  public void setA( Xhtml_a_type a ) {
    setChild( ID_A, a );
  }

  /**
   * Returns the element img.
   *
   * @return The element img
   */
  public Xhtml_img_type getImg() {
    return (Xhtml_img_type) get( ID_IMG, null );
  }

  /**
   * Set the element img.
   *
   * @param img new value for element img
   */
  public void setImg( Xhtml_img_type img ) {
    setChild( ID_IMG, img );
  }

  /**
   * Returns the element map.
   *
   * @return The element map
   */
  public Xhtml_map_type getMap() {
    return (Xhtml_map_type) get( ID_MAP, null );
  }

  /**
   * Set the element map.
   *
   * @param map new value for element map
   */
  public void setMap( Xhtml_map_type map ) {
    setChild( ID_MAP, map );
  }

  /**
   * Returns the element object.
   *
   * @return The element object
   */
  public Xhtml_object_type getObject() {
    return (Xhtml_object_type) get( ID_OBJECT, null );
  }

  /**
   * Set the element object.
   *
   * @param _object new value for element object
   */
  public void setObject( Xhtml_object_type _object ) {
    setChild( ID_OBJECT, _object );
  }

  /**
   * Returns the element input.
   *
   * @return The element input
   */
  public Xhtml_input_type getInput() {
    return (Xhtml_input_type) get( ID_INPUT, null );
  }

  /**
   * Set the element input.
   *
   * @param input new value for element input
   */
  public void setInput( Xhtml_input_type input ) {
    setChild( ID_INPUT, input );
  }

  /**
   * Returns the element select.
   *
   * @return The element select
   */
  public Xhtml_select_type getSelect() {
    return (Xhtml_select_type) get( ID_SELECT, null );
  }

  /**
   * Set the element select.
   *
   * @param select new value for element select
   */
  public void setSelect( Xhtml_select_type select ) {
    setChild( ID_SELECT, select );
  }

  /**
   * Returns the element textarea.
   *
   * @return The element textarea
   */
  public Xhtml_textarea_type getTextarea() {
    return (Xhtml_textarea_type) get( ID_TEXTAREA, null );
  }

  /**
   * Set the element textarea.
   *
   * @param textarea new value for element textarea
   */
  public void setTextarea( Xhtml_textarea_type textarea ) {
    setChild( ID_TEXTAREA, textarea );
  }

  /**
   * Returns the element label.
   *
   * @return The element label
   */
  public Xhtml_label_type getLabel() {
    return (Xhtml_label_type) get( ID_LABEL, null );
  }

  /**
   * Set the element label.
   *
   * @param label new value for element label
   */
  public void setLabel( Xhtml_label_type label ) {
    setChild( ID_LABEL, label );
  }

  /**
   * Returns the element button.
   *
   * @return The element button
   */
  public Xhtml_button_type getButton() {
    return (Xhtml_button_type) get( ID_BUTTON, null );
  }

  /**
   * Set the element button.
   *
   * @param button new value for element button
   */
  public void setButton( Xhtml_button_type button ) {
    setChild( ID_BUTTON, button );
  }

  /**
   * Returns the element ruby.
   *
   * @return The element ruby
   */
  public Xhtml_ruby_type getRuby() {
    return (Xhtml_ruby_type) get( ID_RUBY, null );
  }

  /**
   * Set the element ruby.
   *
   * @param ruby new value for element ruby
   */
  public void setRuby( Xhtml_ruby_type ruby ) {
    setChild( ID_RUBY, ruby );
  }

  /**
   * Returns the element ins.
   *
   * @return The element ins
   */
  public Xhtml_edit_type getIns() {
    return (Xhtml_edit_type) get( ID_INS, null );
  }

  /**
   * Set the element ins.
   *
   * @param ins new value for element ins
   */
  public void setIns( Xhtml_edit_type ins ) {
    setChild( ID_INS, ins );
  }

  /**
   * Returns the element del.
   *
   * @return The element del
   */
  public Xhtml_edit_type getDel() {
    return (Xhtml_edit_type) get( ID_DEL, null );
  }

  /**
   * Set the element del.
   *
   * @param del new value for element del
   */
  public void setDel( Xhtml_edit_type del ) {
    setChild( ID_DEL, del );
  }

  /**
   * Returns the element script.
   *
   * @return The element script
   */
  public Xhtml_script_type getScript() {
    return (Xhtml_script_type) get( ID_SCRIPT, null );
  }

  /**
   * Set the element script.
   *
   * @param script new value for element script
   */
  public void setScript( Xhtml_script_type script ) {
    setChild( ID_SCRIPT, script );
  }

  /**
   * Returns the element noscript.
   *
   * @return The element noscript
   */
  public Xhtml_noscript_type getNoscript() {
    return (Xhtml_noscript_type) get( ID_NOSCRIPT, null );
  }

  /**
   * Set the element noscript.
   *
   * @param noscript new value for element noscript
   */
  public void setNoscript( Xhtml_noscript_type noscript ) {
    setChild( ID_NOSCRIPT, noscript );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
